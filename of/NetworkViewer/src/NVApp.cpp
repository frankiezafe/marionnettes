#include "NVApp.h"

// https://smeenk.com/kinect-field-of-view-comparison/
// horizontal : 58.5
// vertical : 46.6
// float fov_x = sin( 58.5 * 0.5 / 180 * PI ); ~0.510508806
// float fov_y = sin( 46.6 * 0.5 / 180 * PI ); ~0.406661716
static float kinect_near = 1.f;
static float kinect_far = 5.5f;
static float kinect_points[ 90 ] = {
    // cone
    0, 0, 0,
    0.510508806f * kinect_far, kinect_far, 0.406661716f * kinect_far,
    0, 0, 0,
    0.510508806f * kinect_far, kinect_far, -0.406661716f * kinect_far,
    0, 0, 0,
    -0.510508806f * kinect_far, kinect_far, -0.406661716f * kinect_far,
    0, 0, 0,
    -0.510508806f * kinect_far, kinect_far, 0.406661716f * kinect_far,
    // far
    0.510508806f * kinect_far, kinect_far, 0.406661716f * kinect_far,
    0.510508806f * kinect_far, kinect_far, -0.406661716f * kinect_far,
    0.510508806f * kinect_far, kinect_far, -0.406661716f * kinect_far,
    -0.510508806f * kinect_far, kinect_far, -0.406661716f * kinect_far,
    -0.510508806f * kinect_far, kinect_far, -0.406661716f * kinect_far,
    -0.510508806f * kinect_far, kinect_far, 0.406661716f * kinect_far,
    -0.510508806f * kinect_far, kinect_far, 0.406661716f * kinect_far,
    0.510508806f * kinect_far, kinect_far, 0.406661716f * kinect_far,
    // near
    0.510508806f * kinect_near, kinect_near, 0.406661716f * kinect_near,
    0.510508806f * kinect_near, kinect_near, -0.406661716f * kinect_near,
    0.510508806f * kinect_near, kinect_near, -0.406661716f * kinect_near,
    -0.510508806f * kinect_near, kinect_near, -0.406661716f * kinect_near,
    -0.510508806f * kinect_near, kinect_near, -0.406661716f * kinect_near,
    -0.510508806f * kinect_near, kinect_near, 0.406661716f * kinect_near,
    -0.510508806f * kinect_near, kinect_near, 0.406661716f * kinect_near,
    0.510508806f * kinect_near, kinect_near, 0.406661716f * kinect_near,

    // up
    0.510508806f * 0.5f * kinect_near, kinect_near, 0.406661716f * 1.05f * kinect_near,
    -0.510508806f * 0.5f * kinect_near, kinect_near, 0.406661716f * 1.05f * kinect_near,
    -0.510508806f * 0.5f * kinect_near, kinect_near, 0.406661716f * 1.05f * kinect_near,
    0, kinect_near, 0.406661716f * 1.25f * kinect_near,
    0, kinect_near, 0.406661716f * 1.25f * kinect_near,
    0.510508806f * 0.5f * kinect_near, kinect_near, 0.406661716f * 1.05f * kinect_near

};

NVApp::NVApp() :
osc_senders_num(0),
osc_receivers_num(0),
osc_senders(0),
osc_receivers(0),
kinect_num(0),
refresh_ui(false),
kinect_selected(0) {
    //    model_rotor.makeRotate(180, ofVec3f(0, 0, 1));
    //    model_rotor *= ofQuaternion(-20, ofVec3f(1, 0, 0));
    matrix_rotor.makeRotationMatrix(model_rotor);
    scale.set(0.4, 0.4, 0.4);
}

void NVApp::exit() {

    if (osc_senders) {
        delete[] osc_senders;
        osc_senders = 0;
        osc_senders_num = 0;
    }
    if (osc_receivers) {
        delete[] osc_receivers;
        osc_receivers = 0;
        osc_receivers_num = 0;
    }

}

void NVApp::setup() {

    std::vector<std::string> oscout;
    std::vector<std::string> oscin;
    ofFile oscconf(ofToDataPath("configuration.osc"));
    if (oscconf.exists()) {
        ofBuffer buffer(oscconf);
        for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
            if ((*it)[0] == '#') {
                continue;
            }
            string line = *it;
            if (line.length() < 2) {
                continue;
            }
            std::vector<std::string> args = ofSplitString(line, " ");
            if (args.size() >= 2) {
                if (args[0].compare("OUT") == 0 && !args[1].empty()) {
                    if (args[1].find(":") != std::string::npos) {
                        oscout.push_back(args[1]);
                    }
                } else if (args[0].compare("IN") == 0 && !args[1].empty()) {
                    oscin.push_back(args[1]);
                }
            }
        }
    }

    if (!oscout.empty()) {
        osc_senders_num = oscout.size();
        osc_senders = new ofxOscSender[osc_senders_num];
        for (size_t i = 0; i < osc_senders_num; ++i) {
            std::cout << "osc sender : " << oscout[i] << std::endl;
            std::vector<std::string> p = ofSplitString(oscout[i], ":");
            osc_senders[i].setup(p[0], atoi(p[1].c_str()));
        }
    }

    if (!oscin.empty()) {
        osc_receivers_num = oscin.size();
        osc_receivers = new ofxOscReceiver[osc_receivers_num];
        for (size_t i = 0; i < osc_receivers_num; ++i) {
            std::cout << "osc receiver : " << oscin[i] << std::endl;
            osc_receivers[i].setup(atoi(oscin[i].c_str()));
        }
    }

    kinect_selector.setup();

}

void NVApp::printOsc(ofxOscMessage& m) {

    std::stringstream ss;
    ss << m.getRemoteIp() << ":" <<
            m.getRemotePort() << " >> " <<
            m.getAddress() << std::endl;
    std::stringstream ss_values;
    ofxOscArgType type;
    for (int i = 0; i < m.getNumArgs(); ++i) {
        type = m.getArgType(i);
        ss << "[" << i << ":";
        if (i > 0) {
            ss_values << " ";
        }
        ss_values << "[" << i << "]";
        switch (type) {
            case OFXOSC_TYPE_INT32:
                ss << "i32]";
                ss_values << m.getArgAsInt32(i);
                break;
            case OFXOSC_TYPE_INT64:
                ss << "i64]";
                ss_values << m.getArgAsInt64(i);
                break;
            case OFXOSC_TYPE_FLOAT:
                ss << "f]";
                ss_values << m.getArgAsFloat(i);
                break;
            case OFXOSC_TYPE_DOUBLE:
                ss << "d]";
                ss_values << m.getArgAsDouble(i);
                break;
            case OFXOSC_TYPE_STRING:
                ss << "s]";
                ss_values << m.getArgAsString(i);
                break;
            case OFXOSC_TYPE_SYMBOL:
                ss << "S]";
                ss_values << m.getArgAsSymbol(i);
                break;
            case OFXOSC_TYPE_CHAR:
                ss << "c]";
                ss_values << m.getArgAsChar(i);
                break;
            case OFXOSC_TYPE_MIDI_MESSAGE:
                ss << "m]";
                ss_values << m.getArgAsMidiMessage(i);
                break;
            case OFXOSC_TYPE_TRUE:
                ss << "T]";
                ss_values << "true";
                break;
            case OFXOSC_TYPE_FALSE:
                ss << "F]";
                ss_values << "false";
                break;
            case OFXOSC_TYPE_TRIGGER:
                ss << "I]";
                ss_values << m.getArgAsTrigger(i);
                break;
            case OFXOSC_TYPE_BLOB:
                ss << "b]";
                ss_values << "***";
                break;
            case OFXOSC_TYPE_BUNDLE:
                ss << "B]";
                ss_values << "***";
                break;
            case OFXOSC_TYPE_RGBA_COLOR:
                ss << "r]";
                ss_values << m.getArgAsRgbaColor(i);
                break;
            default:
                ss << "?]";
                ss_values << "?";
                break;
        }
    }

    std::cout << ss.str() << std::endl << ss_values.str() << std::endl;

}

void NVApp::parseOsc() {

    ofxOscMessage m;
    for (size_t i = 0; i < osc_receivers_num; ++i) {
        ofxOscReceiver& rcvr = osc_receivers[i];
        while (rcvr.hasWaitingMessages()) {
            rcvr.getNextMessage(m);
            if (m.getAddress().compare(OSC_SKELETON_POSITIONS) == 0) {
                parseSkeletonPositions(m);
            } else if (m.getAddress().compare(OSC_MATRIX_UPLOAD) == 0) {
                parseCalibrationMatrix(m);
            } else if (m.getAddress().compare(OSC_FILTER_UPLOAD) == 0) {
                parseFiltering(m);
            }
            //printOsc(m);
        }
    }

}

bool NVApp::kinect_hash(ofxOscMessage& m, std::string& dst) {

    std::string ip = m.getRemoteIp();
    uint32_t port = m.getRemotePort();
    
    if ( m.getNumArgs() < 2 ) {
        std::cout << "strange message! " <<
                ip << ":" <<
                port << 
                ", message address: " << m.getAddress() <<
                ", args: " << m.getNumArgs() <<
                std::endl;
        return false;
    }
    
    uint8_t appid = m.getArgAsInt(0);
    uint8_t kinect = m.getArgAsInt(1);

    if ( ip.empty() || port == 0 || appid == 0 || kinect > 1 ) {
        std::cout << "kinect_hash, Unidentified sender! " <<
                ip << ":" <<
                port << 
                ", message address: " << m.getAddress() <<
                ", app id: " << appid <<
                ", kinect id: " << kinect <<
                " =>  no kinect declared" <<
                std::endl;
        return false;
    }
    
    std::stringstream ssk;
    ssk << ip << "." << int(appid) << "." << int( kinect);

    std::string kh = ssk.str();
    if (kinects.find(kh) == kinects.end()) {
        std::cout << "new kinect! " << kh << " on " << m.getAddress() << std::endl;
        std::string ip = m.getRemoteIp();
        kinects[kh].hash = kh;
        kinects[kh].ip = ip;
        kinects[kh].port = KINECT_CONTROL_PORT;
        kinects[kh].appid = appid;
        kinects[kh].kinect = kinect;
        kinects[kh].sender.setup(ip, KINECT_CONTROL_PORT);
        kinects[kh].sender_configured = true;
        refresh_ui = true;
    }

    dst = ssk.str();
    
    return true;

}

void NVApp::parseFiltering(ofxOscMessage& m) {

    uint8_t appid = m.getArgAsInt(0);
    NUI_TRANSFORM_SMOOTH_PARAMETERS f;
    f.fCorrection = m.getArgAsFloat(1);
    f.fJitterRadius = m.getArgAsFloat(2);
    f.fMaxDeviationRadius = m.getArgAsFloat(3);
    f.fPrediction = m.getArgAsFloat(4);
    f.fSmoothing = m.getArgAsFloat(5);
    std::map< std::string, KinectControl >::iterator itk = kinects.begin();
    std::map< std::string, KinectControl >::iterator itke = kinects.end();
    for (; itk != itke; ++itk) {
        if ((*itk).second.appid == appid) {
            (*itk).second.filtering.fCorrection = f.fCorrection;
            (*itk).second.filtering.fJitterRadius = f.fJitterRadius;
            (*itk).second.filtering.fMaxDeviationRadius = f.fMaxDeviationRadius;
            (*itk).second.filtering.fPrediction = f.fPrediction;
            (*itk).second.filtering.fSmoothing = f.fSmoothing;
            (*itk).second.filtering_received = true;
            std::cout << "filtering updated for " << (*itk).second.hash << std::endl;
        }
    }

}

void NVApp::parseCalibrationMatrix(ofxOscMessage& m) {

    uint8_t kinect = m.getArgAsInt(1);

    std::string kh;
    if ( !kinect_hash(m, kh) ) {
        return;
    }
    KinectControl& kc = kinects[kh];
    for (uint8_t i = 0; i < 16; ++i) {
        kc.calibration.getPtr()[i] = m.getArgAsFloat(i + 2);
        refresh_ui = true;
    }
    kc.calib_received = true;
    std::cout << "kinect calibration received " << kh << std::endl;

}

void NVApp::parseSkeletonPositions(ofxOscMessage& m) {

    std::string ip = m.getRemoteIp();
    uint32_t port = m.getRemotePort();
    uint8_t appid = m.getArgAsInt(0);
    uint8_t kinect = m.getArgAsInt(1);
    uint8_t id = m.getArgAsInt(2);

    std::string kh;
    if ( !kinect_hash(m, kh) ) {
        return;
    }

    std::stringstream ss;
    ss << kh << "." << int( id);
    std::string skeleton_hash = ss.str();

    SkeletonDisplay& sk = skeletons[skeleton_hash];
    sk.ip = ip;
//    sk.port = port;
    sk.appid = appid;
    sk.kinect = kinect;
    sk.id = id;
    sk.last_update = raw::now();
    for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {
        sk.absolute_positions[i].set(
                m.getArgAsFloat(3 + (i * 3)),
                m.getArgAsFloat(4 + (i * 3)),
                m.getArgAsFloat(5 + (i * 3))
                );
    }
    ksk[ &kinects[kh] ][kinect] = &skeletons[skeleton_hash];

}

void NVApp::update() {

    parseOsc();

    uint64_t now = raw::now();

    // purging old skeletons
    std::vector< std::string > zombies;
    SKMapIter it = skeletons.begin();
    SKMapIter ite = skeletons.end();
    for (; it != ite; ++it) {
        // more than 1 second that no info arrived
        if (now - it->second.last_update > 1000000) {
            ksk[ &kinects[ it->second.kinect_hash() ] ][ it->second.kinect ] = 0;
            zombies.push_back(it->first);
        }
    }

    if (!zombies.empty()) {
        std::vector< std::string >::iterator itz = zombies.begin();
        std::vector< std::string >::iterator itze = zombies.end();
        for (; itz != itze; ++itz) {
            skeletons.erase((*itz));
        }
    }
    
    std::map< std::string, KinectControl >::iterator itk;
    std::map< std::string, KinectControl >::iterator itke = kinects.end();

    if ( kinect_num != kinects.size() ) {
        kinect_num = kinects.size();
        itk = kinects.begin();
        std::cout << "new kinect registered" << std::endl;
        uint16_t i = 0;
        for( ; itk != itke; ++itk ) {
            std::cout << "[" << i << "] " << 
                    itk->first << " | " << 
                    itk->second.hash << 
                    std::endl;
            ++i;
        }
    }

    if (refresh_ui) {
        ui_selector();
        ui_calibrator();
        refresh_ui = false;
    }
    
    itk = kinects.begin();
    for (; itk != itke; ++itk) {
        KinectControl& kc = itk->second;
        if ( !kc.sender_configured ) {
            continue;
        }
        if (
                (
                !kc.calib_received ||
                !kc.filtering_received
                ) && kc.request_timeout < now) {
            kc.request_timeout = now + 1000000;
            ofxOscMessage m;
            if (!kc.calib_received) {
                std::cout << "requesting calibration matrix from " <<
                        kc.hash << std::endl;
                m.setAddress(OSC_MATRIX_REQUEST);
            } else {
                std::cout << "requesting filtering from " <<
                        kc.hash << std::endl;
                m.setAddress(OSC_FILTER_REQUEST);
            }
            kc.sender.sendMessage(m);
        }
    }

    autocalibration();

}

void NVApp::autocalibration() {

    std::map< std::string, KinectControl >::iterator itk;
    std::map< std::string, KinectControl >::iterator itke = kinects.end();

    KinectControl* ground_truth = 0;
    SkeletonDisplay* ground_truth_sk = 0;
    SkeletonDisplay* calibration_sk = 0;
    uint8_t active;
    std::stringstream ss;

    itk = kinects.begin();
    for (; itk != itke; ++itk) {
        if (itk->second.ground_truth) {
            ground_truth = &itk->second;
        }
    }

    if (ground_truth) {
        // how many skeletons in ground_truth?
        active = 0;
        for (uint8_t i = 0; i < NUI_SKELETON_COUNT; ++i) {
            if (ksk[ ground_truth ][ i ]) {
                ground_truth_sk = ksk[ ground_truth ][ i ];
                ++active;
            }
        }
        if (active != 1) {
            ss << "/!\\ ground_truth '" <<
                    ground_truth->hash <<
                    "' has " << int( active) <<
                    " active skeleton(s), " <<
                    "must be 1 to calibrate" << std::endl;
            ground_truth = 0;
            ground_truth_sk = 0;
        } else {
            ss << "ground_truth '" <<
                    ground_truth->hash <<
                    "' is ready for calibration" <<
                    std::endl;
        }
    }

    // ok, ground_truth has exactly 1 skeleton active
    if (ground_truth) {

        itk = kinects.begin();
        for (; itk != itke; ++itk) {

            KinectControl& kc = itk->second;

            if (!kc.auto_calibrate) {
                continue;
            }
            // how many skeletons in ground_truth?
            active = 0;
            for (uint8_t i = 0; i < NUI_SKELETON_COUNT; ++i) {
                if (ksk[ &itk->second ][ i ]) {
                    calibration_sk = ksk[ &itk->second ][ i ];
                    ++active;
                }
            }
            if (active != 1) {
                ss << "/!\\ kinect '" <<
                        kc.hash <<
                        "' has " << int( active) <<
                        " active skeleton(s), " <<
                        "must be 1 to calibrate" << std::endl;
            } else {

                if (kc.deltas.empty()) {
                    raw_conf.vec3_num = 2;
                    std::stringstream outp;
                    outp << "calibration_" << kc.hash << "_" << raw::now();
                    std::string p = ofToDataPath(outp.str(), true);
                    raw_recoder.write_open(p);
                }

                ofVec3f gtp(ground_truth_sk->absolute_positions[ NUI_SKELETON_POSITION_HIP_CENTER ]);
                ofVec3f clp(calibration_sk->absolute_positions[ NUI_SKELETON_POSITION_HIP_CENTER ]);
                kc.deltas.push_back(std::pair< ofVec3f, ofVec3f >(gtp, clp));

                raw_recoder.write_start_frame(&raw_conf);
                raw_recoder.write_vec3(gtp.x, gtp.y, gtp.z);
                raw_recoder.write_vec3(clp.x, clp.y, clp.z);

                ss << "kinect '" <<
                        ground_truth->hash <<
                        "' is calibrating, " <<
                        kc.deltas.size() << " positions" <<
                        std::endl;

                if (kc.deltas.size() >= 500) {
                    raw_recoder.write_close();
                    compute_calibration(&itk->second, ground_truth);
                    kc.auto_calibrate = false;
                    refresh_ui = true;
                }

            }

        }

    }

    autocalibration_debug = ss.str();

}

void NVApp::compute_calibration(KinectControl* kc, KinectControl* gt) {

    if (kc->deltas.size() < 2) {
        kc->deltas.clear();
        return;
    }

    size_t vnum = kc->deltas.size();
    size_t vvalid = 0;
    Vec3PairListIter it;
    Vec3PairListIter itprev;
    Vec3PairListIter itend;

    // temporary data
    ofVec3f zero(0, 0, 0);
    ofMatrix4x4 calibration_matrix;
    Vec3PairList directions;
    std::vector< ofVec3f > rotated_dir;
    directions.reserve(vnum - 1);
    rotated_dir.reserve(vnum - 1);

    float* weight = new float[vnum];
    float weight_total = 0;

    // verification of data + directions process
    it = kc->deltas.begin();
    itprev = kc->deltas.begin();
    itend = kc->deltas.end();
    ++it;
    size_t i = 1;
    for (; it != itend; ++it, ++i) {
        bool valid = true;
        ofVec3f delta_first = it->first - itprev->first;
        if (delta_first.lengthSquared() < 1.e-5) {
            std::cout << "duplicated first position at " << i << std::endl;
            valid = false;
        }
        ofVec3f delta_second = it->second - itprev->second;
        if (delta_second.lengthSquared() < 1.e-5) {
            std::cout << "duplicated second position at " << i << std::endl;
            valid = false;
        }
        if (valid) {
            ++vvalid;
            weight[ i - 1 ] = it->first.distance(it->second) * (
                    delta_first.length() + delta_second.length()
                    );
            weight_total += weight[ i - 1 ];
            directions.push_back(
                    std::pair<ofVec3f, ofVec3f>(
                    delta_first.normalize(),
                    delta_second.normalize()
                    )
                    );
        } else {
            weight[ i - 1 ] = 0;
            directions.push_back(
                    std::pair<ofVec3f, ofVec3f>(zero, zero)
                    );
        }
        rotated_dir.push_back(zero);
    }

    ofQuaternion calibration_q;
    calibration_matrix.makeIdentityMatrix();
    ofMatrix4x4 calibration_mati;
    ofMatrix4x4 work_mat;

    // decomposition of the orientation in 3 axis:
    // Z first (~yaw) -> angle perpendicular to ground
    // X (~tilt) -> up/down angle
    // Y (~roll) -> if kinect transversal axis is not parallel to ground, the
    // less likely

    // ************ Z ************

    float zangl = 0;
    it = directions.begin();
    itend = directions.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        if (it->first == zero) {
            continue;
        }
        // projection of the direction on XY plane
        ofVec2f first(it->first.x, it->first.y);
        ofVec2f second(it->second.x, it->second.y);
        zangl += second.angle(first) * weight[i] / weight_total;
    }

    std::cout << "average Z angle " << zangl << std::endl;

    calibration_q = ofQuaternion(zangl, ofVec3f(0, 0, 1));
    calibration_matrix.makeRotationMatrix(calibration_q);
    calibration_mati = calibration_matrix.getInverse();

    // applying the Z correction on points & rendering new directions
    it = kc->deltas.begin();
    itprev = kc->deltas.begin();
    itend = kc->deltas.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        work_mat.makeTranslationMatrix(it->second);
        work_mat = work_mat * calibration_matrix;
        if (i != 0 && directions[i - 1].first != zero) {
            work_mat.makeTranslationMatrix(directions[i - 1].second);
            work_mat = calibration_mati * work_mat * calibration_matrix;
            rotated_dir[i - 1] = work_mat.getTranslation();
        }
    }

    // ************ X ************

    float xangl = 0;
    it = directions.begin();
    itend = directions.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        if (it->first == zero) {
            continue;
        }
        // projection of the direction on YZ plane
        ofVec2f first(it->first.y, it->first.z);
        ofVec2f second(rotated_dir[i].y, rotated_dir[i].z);
        xangl += second.angle(first) * weight[i] / weight_total;
    }

    std::cout << "average X angle " << xangl << std::endl;

    ofVec3f right(1, 0, 0);
    right = calibration_q * right;
    calibration_q *= ofQuaternion(xangl, right);
    calibration_matrix.makeRotationMatrix(calibration_q);
    calibration_mati = calibration_matrix.getInverse();

    // applying the X correction on points & rendering new directions
    it = kc->deltas.begin();
    itprev = kc->deltas.begin();
    itend = kc->deltas.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        work_mat.makeTranslationMatrix(it->second);
        work_mat = work_mat * calibration_matrix;
        if (i != 0 && directions[i - 1].first != zero) {
            work_mat.makeTranslationMatrix(directions[i - 1].second);
            work_mat = calibration_mati * work_mat * calibration_matrix;
            rotated_dir[i - 1] = work_mat.getTranslation();
        }
    }

    // ************ Y ************

#ifdef CALIBRATION_PROCESS_ROLL

    float yangl = 0;
    it = directions.begin();
    itend = directions.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        if (it->first == zero) {
            continue;
        }
        // projection of the direction on XZ plane
        ofVec2f first(it->first.x, it->first.z);
        ofVec2f second(rotated_dir[i].x, rotated_dir[i].z);
        yangl += second.angle(first) * weight[i] / weight_total;
    }

    std::cout << "average Y angle " << yangl << std::endl;

    ofVec3f front(0, 1, 0);
    front = calibration_q * front;
    calibration_q *= ofQuaternion(yangl, front);
    calibration_matrix.makeRotationMatrix(calibration_q);
    calibration_mati = calibration_matrix.getInverse();

    // applying the Y correction on points & rendering new directions
    it = kc->deltas.begin();
    itprev = kc->deltas.begin();
    itend = kc->deltas.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        work_mat.makeTranslationMatrix(it->second);
        work_mat = calibration_mati * work_mat * calibration_matrix;
    }

#endif

    // back to positions
    it = kc->deltas.begin();
    itend = kc->deltas.end();

    // rendering the average difference between rotated position and originals
    float lerp = 1.f / vnum;
    ofVec3f median_offset(0, 0, 0);
    for (; it != itend; ++it, ++i) {
        work_mat.makeTranslationMatrix(it->second);
        work_mat = calibration_mati * work_mat * calibration_matrix;
        ofVec3f prtd = work_mat.getTranslation();
        median_offset += (it->first - prtd) * lerp;
    }

    work_mat.makeTranslationMatrix(median_offset);
    calibration_matrix *= work_mat;

    kc->calibration *= calibration_matrix;

    kc->deltas.clear();
    delete[] weight;

    sendCalibration(kc);

}

void NVApp::ui_selector() {

    std::map< std::string, KinectControl >::iterator itk;
    std::map< std::string, KinectControl >::iterator itke = kinects.end();

    itk = kinects.begin();
    for (; itk != itke; ++itk) {
        itk->second.sel_button.removeListener(this, &NVApp::kinectSelected);
    }
    
    kinect_selector.clear();
    kinect_selector.setup();

    itk = kinects.begin();
    for (; itk != itke; ++itk) {
        std::stringstream label;
        if (itk->second.ground_truth) {
            label << "[!] ";
        }
        if (itk->second.auto_calibrate) {
            label << ">> ";
        }
        label << itk->second.hash;
        kinect_selector.add(
                itk->second.sel_button.setup(
                label.str(),
                kinect_selected == &itk->second,
                250
                ));
        itk->second.sel_button.addListener(this, &NVApp::kinectSelected);
    }
    
    kinect_selector.setSize(250, kinect_selector.getHeight());

}

void NVApp::ui_calibrator() {
        
    kinect_pos_x.removeListener(this, &NVApp::kinectCalibrated);
    kinect_pos_y.removeListener(this, &NVApp::kinectCalibrated);
    kinect_pos_z.removeListener(this, &NVApp::kinectCalibrated);
    kinect_rot_x.removeListener(this, &NVApp::kinectCalibrated);
    kinect_rot_y.removeListener(this, &NVApp::kinectCalibrated);
    kinect_rot_z.removeListener(this, &NVApp::kinectCalibrated);
    kinect_ground_truth.removeListener(this, &NVApp::kinectGroundtruth);
    kinect_auto_calibrate.removeListener(this, &NVApp::kinectAutocalibrate);
    kinect_reset_matrix.removeListener(this, &NVApp::kinectResetMatrix);

    filter_fCorrection.removeListener(this, &NVApp::kinectFilter);
    filter_fJitterRadius.removeListener(this, &NVApp::kinectFilter);
    filter_fMaxDeviationRadius.removeListener(this, &NVApp::kinectFilter);
    filter_fPrediction.removeListener(this, &NVApp::kinectFilter);
    filter_fSmoothing.removeListener(this, &NVApp::kinectFilter);
    
    kinect_calibrator.clear();
    
    if (    !kinect_selected || 
            !kinect_selected->calib_received || 
            !kinect_selected->filtering_received) {
        if ( !kinect_selected ) {        
            std::cout << "No kinect selected!" << std::endl;
        } else {
            std::cout << "Kinect " << kinect_selected->hash <<
                    " is not ready!" <<
                    std::endl;
        }
        return;
    }

    ofVec3f pos = kinect_selected->calibration.getTranslation();
    ofVec3f eulers = kinect_selected->calibration.getRotate().getEuler();

    // anti NaN verification:
    if ( pos.x != pos.x ) { pos.set(0,0,0); }
    if ( eulers.x != eulers.x ) { eulers.set(0,0,0); }
    
    kinect_calibrator.setup();

    if (!kinect_selected->calib_received || !kinect_selected->filtering_received) {

        kinect_calibrator.add(kinect_label.setup(
                "kl", "waiting calibration & filtering"));

    } else {

        if (!kinect_selected->auto_calibrate) {
            kinect_calibrator.add(kinect_ground_truth.setup(
                    "ground truth",
                    kinect_selected->ground_truth
                    ));
        }

        if (!kinect_selected->ground_truth) {
            kinect_calibrator.add(kinect_auto_calibrate.setup(
                    "auto calibration",
                    kinect_selected->auto_calibrate
                    ));
        }

        if (
                !kinect_selected->ground_truth &&
                !kinect_selected->auto_calibrate
                ) {

            kinect_calibrator.add(kinect_reset_matrix.setup("reset matrix", false));
            
            kinect_calibrator.add(kinect_pos_x.setup("px", pos.x, -10, 10, 800));
            kinect_calibrator.add(kinect_pos_y.setup("py", pos.y, -10, 10, 800));
            kinect_calibrator.add(kinect_pos_z.setup("pz", pos.z, -10, 10, 800));

            kinect_calibrator.add(kinect_rot_x.setup("rx", eulers.x, -180, 180, 800));
            kinect_calibrator.add(kinect_rot_y.setup("ry", eulers.y, -180, 180, 800));
            kinect_calibrator.add(kinect_rot_z.setup("rz", eulers.z, -180, 180, 800));

            kinect_calibrator.add(filter_fCorrection.setup(
                    "fCorrection", kinect_selected->filtering.fCorrection, 0, 1, 300));
            kinect_calibrator.add(filter_fJitterRadius.setup(
                    "fJitterRadius", kinect_selected->filtering.fJitterRadius, 0, 1, 300));
            kinect_calibrator.add(filter_fMaxDeviationRadius.setup(
                    "fMaxDeviationRadius", kinect_selected->filtering.fMaxDeviationRadius, 0, 1, 300));
            kinect_calibrator.add(filter_fPrediction.setup(
                    "fPrediction", kinect_selected->filtering.fPrediction, 0, 1, 300));
            kinect_calibrator.add(filter_fSmoothing.setup(
                    "fSmoothing", kinect_selected->filtering.fSmoothing, 0, 1, 300));

            kinect_reset_matrix.addListener(this, &NVApp::kinectResetMatrix);
            kinect_pos_x.addListener(this, &NVApp::kinectCalibrated);
            kinect_pos_y.addListener(this, &NVApp::kinectCalibrated);
            kinect_pos_z.addListener(this, &NVApp::kinectCalibrated);
            kinect_rot_x.addListener(this, &NVApp::kinectCalibrated);
            kinect_rot_y.addListener(this, &NVApp::kinectCalibrated);
            kinect_rot_z.addListener(this, &NVApp::kinectCalibrated);

            filter_fCorrection.addListener(this, &NVApp::kinectFilter);
            filter_fJitterRadius.addListener(this, &NVApp::kinectFilter);
            filter_fMaxDeviationRadius.addListener(this, &NVApp::kinectFilter);
            filter_fPrediction.addListener(this, &NVApp::kinectFilter);
            filter_fSmoothing.addListener(this, &NVApp::kinectFilter);

        }

        if (!kinect_selected->auto_calibrate) {
            kinect_ground_truth.addListener(this, &NVApp::kinectGroundtruth);
        }

        if (!kinect_selected->ground_truth) {
            kinect_auto_calibrate.addListener(this, &NVApp::kinectAutocalibrate);
        }

    }

    kinect_calibrator.setSize(800, kinect_calibrator.getHeight());

    kinect_calibrator.setPosition(
            kinect_selector.getPosition().x +
            kinect_selector.getShape().width + 5,
            kinect_selector.getPosition().y
            );

}

void NVApp::kinectSelected(bool& v) {

    if (kinect_selected) {
        kinect_selected->sel_button = false;
        kinect_selected = 0;
    }

    std::map< std::string, KinectControl >::iterator itk = kinects.begin();
    std::map< std::string, KinectControl >::iterator itke = kinects.end();
    for (; itk != itke; ++itk) {
        if (itk->second.sel_button.getParameter().cast<bool>()) {
            kinect_selected = &itk->second;
            std::cout << "kinect selected: " <<
                    kinect_selected->hash << 
                    ", calibrated: " << kinect_selected->calib_received << 
                    ", filtering: " << kinect_selected->filtering_received << 
                    std::endl;
            break;
        }
    }
    
    refresh_ui = true;

}

void NVApp::kinectAutocalibrate(bool& v) {

    if (!kinect_selected) {
        return;
    }
    kinect_selected->auto_calibrate = v;
    if (v) {
        kinect_selected->deltas.clear();
    }
    refresh_ui = true;

}

void NVApp::kinectGroundtruth(bool& v) {

    if (!kinect_selected) {
        return;
    }

    if (!v) {

        kinect_selected->ground_truth = false;

    } else {

        std::map< std::string, KinectControl >::iterator itk = kinects.begin();
        std::map< std::string, KinectControl >::iterator itke = kinects.end();
        for (; itk != itke; ++itk) {
            if (itk->second.ground_truth && kinect_selected != &itk->second) {
                itk->second.ground_truth = false;
            } else if (kinect_selected == &itk->second) {
                itk->second.ground_truth = true;
            }
            itk->second.auto_calibrate = false;
        }

    }

    refresh_ui = true;

}

void NVApp::kinectFilter(float& v) {

    if (!kinect_selected) {
        return;
    }

    kinect_selected->filtering.fCorrection =
            filter_fCorrection.getParameter().cast<float>();
    kinect_selected->filtering.fJitterRadius =
            filter_fJitterRadius.getParameter().cast<float>();
    kinect_selected->filtering.fMaxDeviationRadius =
            filter_fMaxDeviationRadius.getParameter().cast<float>();
    kinect_selected->filtering.fPrediction =
            filter_fPrediction.getParameter().cast<float>();
    kinect_selected->filtering.fSmoothing =
            filter_fSmoothing.getParameter().cast<float>();

    std::map< std::string, KinectControl >::iterator itk = kinects.begin();
    std::map< std::string, KinectControl >::iterator itke = kinects.end();
    for (; itk != itke; ++itk) {
        if (
                (*itk).second.appid == kinect_selected->appid &&
                kinect_selected != &(*itk).second
                ) {
            // second kinect of the same application
            (*itk).second.filtering.fCorrection = 
                    kinect_selected->filtering.fCorrection;
            (*itk).second.filtering.fJitterRadius = 
                    kinect_selected->filtering.fJitterRadius;
            (*itk).second.filtering.fMaxDeviationRadius = 
                    kinect_selected->filtering.fMaxDeviationRadius;
            (*itk).second.filtering.fPrediction = 
                    kinect_selected->filtering.fPrediction;
            (*itk).second.filtering.fSmoothing = 
                    kinect_selected->filtering.fSmoothing;
        }
    }

    // packing new filtering
    sendFiltering(kinect_selected);

}

void NVApp::kinectCalibrated(float& v) {

    if (!kinect_selected) {
        return;
    }

    ofVec3f pos;
    pos.x = kinect_pos_x.getParameter().cast<float>();
    pos.y = kinect_pos_y.getParameter().cast<float>();
    pos.z = kinect_pos_z.getParameter().cast<float>();

    ofVec3f eulers;
    eulers.x = kinect_rot_x.getParameter().cast<float>();
    eulers.y = kinect_rot_y.getParameter().cast<float>();
    eulers.z = kinect_rot_z.getParameter().cast<float>();
    ofMatrix4x4 rotm(ofQuaternion(
            eulers.x, ofVec3f(1, 0, 0),
            eulers.y, ofVec3f(0, 1, 0),
            eulers.z, ofVec3f(0, 0, 1)
            ));

    kinect_selected->calibration.makeIdentityMatrix();
    kinect_selected->calibration *= rotm;
    kinect_selected->calibration.translate(pos);

    // packing new matrix
    sendCalibration(kinect_selected);

}

void NVApp::kinectResetMatrix(bool& v) {

    if (!kinect_selected) {
        return;
    }
    kinect_selected->calibration.makeIdentityMatrix();
    sendCalibration(kinect_selected);
    refresh_ui = true;

}

void NVApp::sendFiltering(KinectControl* skd) {

    if (!skd) {
        return;
    }
    ofxOscMessage m;
    m.setAddress(OSC_FILTER_UPDATE);
    m.addFloatArg(skd->filtering.fCorrection);
    m.addFloatArg(skd->filtering.fJitterRadius);
    m.addFloatArg(skd->filtering.fMaxDeviationRadius);
    m.addFloatArg(skd->filtering.fPrediction);
    m.addFloatArg(skd->filtering.fSmoothing);
    skd->sender.sendMessage(m);

}

void NVApp::sendCalibration(KinectControl* skd) {

    if (!skd) {
        return;
    }
    ofxOscMessage m;
    m.setAddress(OSC_MATRIX_UPDATE);
    m.addInt32Arg(int(kinect_selected->kinect));
    for (uint8_t i = 0; i < 16; ++i) {
        m.addFloatArg(skd->calibration.getPtr()[i]);
    }
    skd->sender.sendMessage(m);

}

void NVApp::grid(bool x, bool y, bool z) const {
    glColor4f(0.4, 0.4, 0.4, 1);
    glLineWidth(0.5);
    glBegin(GL_LINES);
    for (float u = -10; u <= 10; u += 0.1) {
        //        if (u == int(u)) continue;
        for (float v = -10; v <= 10; v += 0.1) {
            //            if (v == int(v)) continue;
            if (x) {
                //			glColor4f(1, 0, 0, 0.5);
                glVertex3f(0, v, -u);
                glVertex3f(0, v, u);
                glVertex3f(0, -u, v);
                glVertex3f(0, u, v);
            }
            if (y) {
                //			glColor4f(0, 1, 0, 0.5);
                glVertex3f(v, 0, -u);
                glVertex3f(v, 0, u);
                glVertex3f(-u, 0, v);
                glVertex3f(u, 0, v);
            }
            if (z) {
                //			glColor4f(0, 0, 1, 0.5);
                glVertex3f(v, -u, 0);
                glVertex3f(v, u, 0);
                glVertex3f(-u, v, 0);
                glVertex3f(u, v, 0);
            }
        }
    }
    glEnd();
    glColor4f(0.85, 0.85, 0.85, 1);
    glLineWidth(1);
    glBegin(GL_LINES);
    for (float u = -10; u <= 10; u += 1) {
        for (float v = -10; v <= 10; v += 1) {
            if (x) {
                //			glColor4f(1, 0, 0, 0.5);
                glVertex3f(0, v, -u);
                glVertex3f(0, v, u);
                glVertex3f(0, -u, v);
                glVertex3f(0, u, v);
            }
            if (y) {
                //			glColor4f(0, 1, 0, 0.5);
                glVertex3f(v, 0, -u);
                glVertex3f(v, 0, u);
                glVertex3f(-u, 0, v);
                glVertex3f(u, 0, v);
            }
            if (z) {
                //			glColor4f(0, 0, 1, 0.5);
                glVertex3f(v, -u, 0);
                glVertex3f(v, u, 0);
                glVertex3f(-u, v, 0);
                glVertex3f(u, v, 0);
            }
        }
    }
    glEnd();
}

void NVApp::kinect() const {

    glBegin(GL_LINES);
    for (uint8_t i = 0; i < 90; i += 3) {
        glVertex3f(
                kinect_points[i],
                kinect_points[i + 1],
                kinect_points[i + 2]
                );
    }
    glEnd();

}

void NVApp::axis(float size) const {

    glLineWidth(3);
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(size, 0, 0);
    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, size, 0);
    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, size);
    glEnd();
    glLineWidth(1);

}

void NVApp::draw() {

    ofSetupScreenPerspective(ofGetWidth(), ofGetHeight(), 30, 0, 10000);

    ofBackground(50, 50, 50);
    glEnable(GL_DEPTH_TEST);

    ofPushMatrix();
    ofTranslate(ofGetWidth()*0.5, ofGetHeight()*0.5, 0);
    //	ofScale(-1, 1, 1);
    ofVec3f sc = scale * ofGetHeight() * ofVec3f(-1, 1, 1);
    ofScale(sc);
    ofMultMatrix(matrix_rotor);
    ofTranslate(translate);
    axis(0.5);
    grid(false, false, true);
    SKMapIter it = skeletons.begin();
    SKMapIter ite = skeletons.end();
    for (; it != ite; ++it) {
        it->second.lines();
        ofPushMatrix();
        ofTranslate(
                it->second.absolute_positions[ NUI_SKELETON_POSITION_HEAD ].x,
                it->second.absolute_positions[ NUI_SKELETON_POSITION_HEAD ].y,
                it->second.absolute_positions[ NUI_SKELETON_POSITION_HEAD ].z
                );
        ofSetColor(255, 255, 255);
        ofDrawBitmapString(it->first, 0, 0);
        ofPopMatrix();
    }
    std::map< std::string, KinectControl >::iterator itk = kinects.begin();
    std::map< std::string, KinectControl >::iterator itke = kinects.end();
    for (; itk != itke; ++itk) {
        KinectControl& kc = itk->second;
        ofPushMatrix();
        ofMultMatrix(kc.calibration);
        if (kinect_selected == &itk->second) {
            if (itk->second.auto_calibrate) {
                glColor4f(0.9, 0.2, 0.0, 1);
            } else if (itk->second.ground_truth) {
                glColor4f(0.0, 0.9, 0.2, 1);
            } else {
                glColor4f(1, 0.7, 0.1, 1);
            }
        } else if (itk->second.auto_calibrate) {
            glColor4f(0.9, 0.2, 0.0, 0.6);
        } else if (itk->second.ground_truth) {
            glColor4f(0.0, 0.9, 0.2, 0.6);
        } else {
            glColor4f(1, 0.7, 0.1, 0.3);
        }
        kinect();
        ofSetColor(255, 255, 255);
        ofDrawBitmapString(itk->first, 0, 0);
        ofPopMatrix();
        if (!kc.deltas.empty()) {
            Vec3PairListIter itp;
            Vec3PairListIter itpe = kc.deltas.end();
            glPointSize(4);
            ofSetColor(255, 255, 255);
            glBegin(GL_POINTS);
            itp = kc.deltas.begin();
            for (; itp != itpe; ++itp) {
                glVertex3f(itp->first.x, itp->first.y, itp->first.z);
            }
            glEnd();
            ofSetColor(255, 0, 255);
            glBegin(GL_POINTS);
            itp = kc.deltas.begin();
            for (; itp != itpe; ++itp) {
                glVertex3f(itp->second.x, itp->second.y, itp->second.z);
            }
            glEnd();
            glBegin(GL_LINES);
            itp = kc.deltas.begin();
            for (; itp != itpe; ++itp) {
                glColor4f(1, 1, 1, 0.7);
                glVertex3f(itp->first.x, itp->first.y, itp->first.z);
                glColor4f(1, 0, 1, 0.7);
                glVertex3f(itp->second.x, itp->second.y, itp->second.z);
            }
            glEnd();
            glPointSize(1);
        }
    }
    ofPopMatrix();

    glDisable(GL_DEPTH_TEST);
    kinect_selector.draw();
    if (kinect_selected) {
        kinect_calibrator.draw();
    }

    if (!autocalibration_debug.empty()) {
        ofSetColor(255, 255, 255);
        ofDrawBitmapString(autocalibration_debug, 30, ofGetHeight() - 80);
    }

}

void NVApp::keyPressed(int key) {

}

void NVApp::keyReleased(int key) {

}

void NVApp::mouseMoved(int x, int y) {

}

void NVApp::mouseDragged(int x, int y, int button) {

    ofVec2f mp(x, y);
    ofVec2f diff = (mp - prev_mouse) / ofGetWidth();
    //    model_rotor *= ofQuaternion(-diff.y * 200, ofVec3f(1, 0, 0));
    //    model_rotor *= ofQuaternion(diff.x * 200, ofVec3f(0, 1, 0));
    if (button == 0) {
        scale += diff.y;
        if (scale.x < 0.0001) {
            scale.set(0.0001, 0.0001, 0.0001);
        }
    } else if (button == 1) {

        ofMatrix4x4 mr(model_rotor);
        ofMatrix4x4 mri = mr.getInverse();
        ofMatrix4x4 t;
        t.makeTranslationMatrix(ofVec3f(1, 0, 0));
        t = mr * t * mri;
        ofVec3f side = t.getTranslation();
        ofVec3f front = ofQuaternion(90, ofVec3f(0, 0, 1)) * side;
        translate +=
                -diff.x * 10 * side +
                diff.y * 10 * front;

    } else if (button == 2) {

        ofVec3f up = model_rotor * ofVec3f(0, 0, 1);
        ofVec3f right = ofVec3f(1, 0, 0);
        model_rotor *= ofQuaternion(diff.x * 200, up);
        model_rotor *= ofQuaternion(diff.y * 200, right);
        matrix_rotor.makeRotationMatrix(model_rotor);

    }
    prev_mouse = mp;

}

void NVApp::mousePressed(int x, int y, int button) {

    prev_mouse.set(x, y);

}

void NVApp::mouseReleased(int x, int y, int button) {

}

void NVApp::mouseEntered(int x, int y) {

}

void NVApp::mouseExited(int x, int y) {

}

void NVApp::windowResized(int w, int h) {

}

void NVApp::gotMessage(ofMessage msg) {

}

void NVApp::dragEvent(ofDragInfo dragInfo) {

}