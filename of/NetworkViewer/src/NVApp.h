#pragma once

#include "ofMain.h"
#include "ofxOscReceiver.h"
#include "ofxOscSender.h"
#include "ofxGui.h"
#include "SkeletonCommon.h"
#include "raw.h"

//#define CALIBRATION_PROCESS_ROLL

class SkeletonDisplay : public SkeletonBase {
public:

    std::string ip;
    //uint32_t port;
    uint8_t appid;
    uint8_t kinect;
    uint8_t id;
    uint64_t last_update;

    SkeletonDisplay() :
    SkeletonBase(),
//    port(0),
    kinect(0),
    id(0),
    last_update(0) {
    }

    std::string kinect_hash() {
        std::stringstream ssk;
        ssk <<
                ip << "." <<
                int(appid) << "." <<
                int(kinect);
        return ssk.str();
    }

    std::string skeleton_hash() {
        std::stringstream ssk;
        ssk <<
                ip << "." <<
//                port << ":" <<
                int(appid) << "." <<
                int(kinect) << "." <<
                int(id);
        return ssk.str();
    }

};

typedef std::vector< std::pair< ofVec3f, ofVec3f > > Vec3PairList;
typedef std::vector< std::pair< ofVec3f, ofVec3f > >::iterator Vec3PairListIter;

class KinectControl {
public:
    
    std::string hash;
    std::string ip;
    uint32_t port;
    uint8_t appid;
    uint8_t kinect;
    bool sender_configured;
    ofxOscSender sender;
    ofxToggle sel_button;
    bool selected;
    
    uint64_t request_timeout;
    bool calib_received;
    bool filtering_received;
    ofMatrix4x4 calibration;
    NUI_TRANSFORM_SMOOTH_PARAMETERS filtering;
    bool ground_truth;
    bool auto_calibrate;
    Vec3PairList deltas;

    KinectControl() :
    port(0),
    sender_configured(false),
    selected(false),
    request_timeout(0),
    calib_received(false),
    filtering_received(false),
    ground_truth(false),
    auto_calibrate(false) {
        calibration.makeIdentityMatrix();
        filtering.fCorrection = 0;
        filtering.fJitterRadius = 0;
        filtering.fMaxDeviationRadius = 0;
        filtering.fPrediction = 0;
        filtering.fSmoothing = 0;
    }
};

typedef std::map< std::string, SkeletonDisplay > SKMap;
typedef std::map< std::string, SkeletonDisplay >::iterator SKMapIter;
typedef std::map< KinectControl*, SkeletonDisplay*[NUI_SKELETON_COUNT] > KSKMap;
typedef std::map< KinectControl*, SkeletonDisplay*[NUI_SKELETON_COUNT] >::iterator KSKMapIter;

class NVApp : public ofBaseApp {
public:

    NVApp();

    void setup();
    void update();
    void draw();
    void exit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    // UI listeners
    void kinectSelected(bool& v);
    void kinectGroundtruth(bool& v);
    void kinectAutocalibrate(bool& v);
    void kinectCalibrated(float& v);
    void kinectResetMatrix(bool& v);
    void kinectFilter(float& v);

private:

    size_t osc_senders_num;
    size_t osc_receivers_num;
    ofxOscSender* osc_senders;
    ofxOscReceiver* osc_receivers;

    void parseOsc();
    void parseSkeletonPositions(ofxOscMessage& m);
    void parseFiltering(ofxOscMessage& m);
    void parseCalibrationMatrix(ofxOscMessage& m);
    void printOsc(ofxOscMessage& m);
    void sendCalibration( KinectControl* skd );
    void sendFiltering( KinectControl* skd );

    std::map< std::string, KinectControl > kinects;
    uint16_t kinect_num;
    SKMap skeletons;
    
    KSKMap ksk;
    
    void autocalibration();
    void compute_calibration( KinectControl* kc, KinectControl* gt );
    
    frame_configuration raw_conf;
    raw raw_recoder;
    
    ofVec2f prev_mouse;
    ofQuaternion model_rotor;
    ofMatrix4x4 matrix_rotor;
    ofVec3f translate;
    ofVec3f scale;

    void axis(float size) const;
    void grid(bool x, bool y, bool z) const;
    void kinect() const;
    bool kinect_hash(ofxOscMessage& m, std::string& dst);

    // UI
    bool refresh_ui;
    ofxPanel kinect_selector;
    KinectControl* kinect_selected;

    ofxPanel kinect_calibrator;
    ofxLabel kinect_label;
    ofxToggle kinect_ground_truth;
    ofxToggle kinect_auto_calibrate;
    ofxToggle kinect_reset_matrix;
    ofxSlider<float> kinect_pos_x;
    ofxSlider<float> kinect_pos_y;
    ofxSlider<float> kinect_pos_z;
    ofxSlider<float> kinect_rot_x;
    ofxSlider<float> kinect_rot_y;
    ofxSlider<float> kinect_rot_z;
    
    ofxSlider<float> filter_fCorrection;
    ofxSlider<float> filter_fJitterRadius;
    ofxSlider<float> filter_fMaxDeviationRadius;
    ofxSlider<float> filter_fPrediction;
    ofxSlider<float> filter_fSmoothing;

    void ui_selector();
    void ui_calibrator();
    
    std::string autocalibration_debug;

};
