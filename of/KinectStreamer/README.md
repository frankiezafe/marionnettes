# Kinect skeleton streamer

## Structure

* **src/**: contains c++ classes to be compiled
* **bin/resources/**: contains a blender project showing how to export a valid *avatar.model* file
* **bin/data/**: contains all files required for application configuration

## Application

The main class is **App** (and not ofApp). It contains the classes for:

* skeletons management (SkeletonBase, SkeletonModel & SkeletonDelta) - see source for documentation;
* all the processing and display of the skeletons, see setup(), loadConfiguration() and update() for important processes;
* the OSC senders.

The rest of the source code is an adaptation of [ofxKinectNui](https://github.com/sadmb/ofxKinectNui), refactored to work with [openframeworks 0.9.8](http://openframeworks.cc/download/). Rotations retrieval has been added, as well as the option to configure skeleton filtering (see Configuration scripts - bin/data/filtering for more info).

The application depends on [ofxOsc](http://openframeworks.cc/documentation/ofxOsc/) (standard addon) and the [Kinect SDK 1.8](https://www.microsoft.com/en-us/download/details.aspx?id=40278).

The ofxKinectNui addon has been prepared for handling 2 kinects. This feature has not been tested. 

### Before compilation

Download and install the [Kinect SDK 1.8](https://www.microsoft.com/en-us/download/details.aspx?id=40278) and make sure your kinect is working properly by launching Depth Basics-D2D in the Kinect for Windows Developer Toolkit v1.8.0 for instance.

### Visual Studio

#### Kinect SDK

To compile the project, after creating or importing this one via the **projectGenerator.exe**, you will have to modify the configuration of your visual studio project. Everything is done in the project's properties panel (right-click on project > Properties):

*  **C/C++**, general panel: add **$(KINECTSDK10_DIR)\inc** to the **Additional Include Directories**
*  **Linker**, general panel: add **$(KINECTSDK10_DIR)\lib\x86** to the **Additional Library Directories**
*  **Linker**, input panel: add **Kinect10.lib** to the **Additional Dependencies**

This enables the usage of the Kinect SDK in the code.

#### Adding common

The folder **of/common** contains the important classes to process skeletons. As they are used in several projects, the **SkeletonCommon** code is externalised.

To link it to the KinecStreamer, add the source code to the project by adding a new filter called *commom*. Add the 2 files (SkeletonCommon.h & SkeletonCommon.cpp) with right click > Add > Existing Item... .

Once done, go to project > Properties and:
 
* **C/C++**, general panel: add **src\..\..\common** to the **Additional Include Directories**

SkeletonCommon.h becomes accessible to the classes of the current project.

## OSC output

The application broadcast one OSC message per frame and per seen skeleton. See [OSC specifications](http://opensoundcontrol.org/spec-1_0) for more details.

Structure:

* address: **/skrot**;
* arg[0]: int32, kinect ID, always 0 if only one kinect connected;
* arg[1]: int32, skeleton ID, from 0 to 5 by default.

After these 2 arguments, the rest of the messages follows this logic:

* arg[i]: string, name of the target bone, see *bin/data/configuration.osc* here below for more details;
* arg[i+1]: float, *w* component of the rotation delta of the bone;
* arg[i+2]: float, *x* component of the rotation delta of the bone;
* arg[i+3]: float, *y* component of the rotation delta of the bone;
* arg[i+4]: float, *z* component of the rotation delta of the bone;
* the next bone information starts at i+5, with the name of the target bone.

*remark*: OSC messages are composed in the same order as the mapping, see *bin/data/configuration.osc*.

## Configuration scripts

Generally, all scripts uses '#' char to mark a comment, and skips emty lines. The spliting occurs on space char. Make sure you don't have leading or trailing spaces in your lines.

The majority of configuration scripts are reloaded when SPACE key is pressed, excepted configuration.osc.

### bin/data/filtering

This file is a major improvement on the [ofxKinectNui](https://github.com/sadmb/ofxKinectNui). It enables the control of the Kinect SDK builtin filtering algorithms. A complete explanation of the paramters can be found here: [NUI_TRANSFORM_SMOOTH_PARAMETERS Structure](https://msdn.microsoft.com/en-us/library/nuiskeleton.nui_transform_smooth_parameters.aspx).

The file already contains the standard sets of parmeters proposed on the microsoft website.

syntax: one line is one configuration

* ``[PARAM]``: only 5 allowed: fSmoothing, fCorrection, fPrediction, fJitterRadius & fMaxDeviationRadius
* ``[float]``: value of the parameter

see [filtering.md](filtering) for default values given by microsoft

### bin/data/configuration.osc

Contains the OSC configuration, loaded **once** at application startup -> requires restart for modification to applied.

syntax: one line is one configuration

* ``[IN | OUT]``: set an OSC receiver (ofxOscReceiver) or an OSC sender (ofxOscSender)
* ``[127.0.0.1][:8000]``: IP address and (if *OUT*) port

### bin/data/avatar.model

Contains the positions and matrices of the blender's armature, see **bin/resources/avatar-export.py** and **bin/resources/avatar-export.md** for generation procedure.

syntax: one line is one configuration

* ``[BONE NAME``]: name of the bone, see *Statics - bone names*  here below for valid names
* ``[POSITION]``: 3 floats representing the world position of the bone's tail
* ``[MATRIX 4x4]``: 16 floats representing the world orientation of the bone

### bin/data/matrix4x4

Contains alignment matrices for all the skeletons type:

* ``KINECT_POS`` & ``KINECT_ROT``: SkeletonBase;
* ``MODEL_POS`` & ``MODEL_ROT``: SkeletonModel;
* ``OUTPUT_POS`` & ``OUTPUT_ROT``: SkeletonDelta.

These matrices are applied at various places depending on the skeleton type, see *ofMatrix4x4 rot_mat* & *ofMatrix4x4 pos_mat* in the code.

syntax: **5** lines is one configuration

* ``>[TYPE]``: line **1**, defines the target matrix, see *Statics - matrix names*  here below for valid names
* ``[float float float float]``: 4 floats representing the first row of the matrix
* ``[float float float float]``: 4 floats representing the second row of the matrix
* ``[float float float float]``: 4 floats representing the third row of the matrix
* ``[float float float float]``: 4 floats representing the last row of the matrix

*remark*: playing with this will lead you in a world of pain...

### bin/data/avatar.mapping

Contains the relation to be done between the target avatar (the one receiving the OSC messages) and the bones computed in SkeletonDelta.

1 lines is one configuration

**simple**

* description: retrieval of the bone's rotation
* syntax: ``[target bone]`` ``[kinect bone]``
* example: ``neck SHOULDER_CENTER``

**specifying weight**

* description: using a weighted orientation, usually smaller than 1
* syntax: ``[target bone]`` ``[parent bone]`` ``[weight]``
* example: ``neck SHOULDER_CENTER 0.8``

**multiple bone**

* description: you can accumulate bone rotation in the output
* syntax: ``[target bone]`` ``[parent bone A]`` ``[weight A]`` ``[parent bone B]`` ``[weight B]`` ...
* example: ``neck SHOULDER_CENTER 0.8 HEAD 0.2 FOOT_LEFT 0.5``

## Statics

### matrix names

* ``KINECT_POS``
* ``KINECT_ROT``
* ``MODEL_POS``
* ``MODEL_ROT``
* ``OUTPUT_POS``
* ``OUTPUT_ROT``

### bone names

* ``HIP_CENTER``
* ``SPINE``
* ``SHOULDER_CENTER``
* ``HEAD``
* ``SHOULDER_LEFT``
* ``ELBOW_LEFT``
* ``WRIST_LEFT``
* ``HAND_LEFT``
* ``SHOULDER_RIGHT``
* ``ELBOW_RIGHT``
* ``WRIST_RIGHT``
* ``HAND_RIGHT``
* ``HIP_LEFT``
* ``KNEE_LEFT``
* ``ANKLE_LEFT``
* ``FOOT_LEFT``
* ``HIP_RIGHT``
* ``KNEE_RIGHT``
* ``ANKLE_RIGHT``
* ``FOOT_RIGHT``

## License

### OSC.py

the file is located in bin/resources/K2A/

License: GNU Lesser General Public License

Official repository: https://pypi.python.org/pypi/pyOSC

### ofxKinectNui

[original repository](https://github.com/sadmb/ofxKinectNui):

* built by sadmb, mail sadam@sadmb.com

MIT license - https://en.wikipedia.org/wiki/MIT_License

#### Changelog:

13/11/2017

**ofxBase3DVideo**

* bug fix on getPixels() method

**ofxKinectNui** 

* implementation of missing abstract method from ofBaseVideo
* bug fix on texture management ( getPixels() & getPixelFormat() methods )
* method to configure filtering - setSmoothing(NUI_TRANSFORM_SMOOTH_PARAMETERS* params)
* methods to get absolute joints location & orientation - getSkeletonAbsolutePoints() & getSkeletonOrientation()
* modification of update() method to retrieve orientations

**ofxKinectNuiPlayer**

* implementation of missing abstract method from ofBaseVideo

**ofKinectApp**

* all standard openframeworks methods (setup(), update(), draw(), etc.) have been prefixed with an '_' and have to be called manually.

### Other

App.h, App.cpp, .blend files, configuration scripts & python scripts (excepted OSC.py) built by [frankiezafe](http://frankiezafe.org)

MIT license - https://en.wikipedia.org/wiki/MIT_License