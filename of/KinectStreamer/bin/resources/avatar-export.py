import bpy
from mathutils import Matrix

############## CONFIGURATION ##############

export_path = bpy.path.abspath("//") + "../data/avatar.model"
arma_name = "model-arma"
obj = bpy.data.objects["model"]
pose = bpy.data.objects["model"].pose

############## FUNCTIONS ##############

def absoluteRotMatrix( armature_name, bone_name ):
	bone = bpy.data.armatures[armature_name].bones[bone_name]
	boneChain = bone.parent_recursive
	boneChain.insert(0, bone)
	m = Matrix().to_3x3()
	for boneRotation in boneChain:
		tmp = Matrix((boneRotation.x_axis, boneRotation.y_axis, boneRotation.z_axis)).transposed()
		m.rotate( tmp )
	return m.to_4x4()

############## EXECUTION ##############

print( "exporting to " + export_path )

file = open( export_path,"w")
for b in pose.bones:
	print( "bone: " + b.name )
	pos = obj.matrix_world * b.tail
	line = b.name
	line += " " + str( pos.x )
	line += " " + str( pos.y ) 
	line += " " + str( pos.z )
	m = obj.matrix_world * absoluteRotMatrix( arma_name, b.name )
	for y in range(0,4):
		for x in range(0,4):
			line += " " + str( m[x][y] )
	# pose matrices are not required anymore by the application
	# code might be useful, so left in comment
	#m = b.matrix
	#for y in range(0,4):
	#	for x in range(0,4):
	#		line += " " + str( m[x][y] )
	file.write( line + "\n" )
	file.flush()

print( "export done" )