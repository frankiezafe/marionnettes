from K2A.K2A import k2a_init, k2a_update, K2A_buffersize

osc_config = {
    0: ('127.0.0.1', 23000, K2A_buffersize ),
    1: ('192.168.3.68', 23000, K2A_buffersize )}

avatar_list = [ "avatar" ]

k2a_init( "root", osc_config, avatar_list )

k2a_update()