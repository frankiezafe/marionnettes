#pragma once

#include "ofKinectApp.h"
#include "ofxOscReceiver.h"
#include "ofxOscSender.h"
#include "SkeletonCommon.h"

// this will create one port by skeleton, based on OUT osc senders
// defined in the configuration.osc
// this is useful for blender and OSC reception in python, how is 
// particularly slow, due to try/except method to connect to socket
// see blender\scripts\K2A\ThreadOsc.py (OscWorker) in this repo
// for the implemntation of the reception
#define USE_PARALLEL_PORTS 1

class App : public ofKinectApp
{

public:

	App();

	void setup();
	void update();
	void grid( bool x = true, bool y = true, bool z = true );
	void draw();
	void exit();

	virtual void keyPressed(int key);
	virtual void mouseMoved(int x, int y);
	virtual void mouseDragged(int x, int y, int button);
	virtual void mousePressed(int x, int y, int button);
	virtual void mouseReleased(int x, int y, int button);
	virtual void windowResized(int w, int h);

	static inline ofQuaternion vec4toQuaternion(const Vector4& src) {
		return ofQuaternion(src.x, src.y, src.z, src.w);
	}

protected:

	uint8_t appUID;
	ofMatrix4x4 kinect0_calibration;
	ofMatrix4x4 kinect1_calibration;

	SkeletonModel skmodel;

	/* Arrays of Skeleton containing kinect and output skeletons */
	SkeletonBase* skkinect;
	SkeletonDelta* skoutput;

	uint16_t skq_num;
	std::vector<MappingKey> mapping;

	size_t osc_senders_num;
	size_t osc_receivers_num;
	ofxOscSender* osc_senders;
	ofxOscReceiver* osc_receivers;

#ifdef USE_PARALLEL_PORTS
	std::vector< ofxOscSender* > osc_parallel_senders;
#endif

	void parseOsc();
	void sendMatrices();
	void updateMatrice(ofxOscMessage& m);
	void sendFilter();
	void updateFilter(ofxOscMessage& m);

	void addMap(std::string line);
	uint8_t stringToBoneIndex(std::string b);

	bool loadConfiguration_request;
	void loadConfiguration();

	float last_elapsed;

	ofVec2f prev_mouse;
	ofQuaternion model_rotor;
	ofMatrix4x4 matrix_rotor;
	ofVec3f translate;
	ofVec3f scale;

	NUI_TRANSFORM_SMOOTH_PARAMETERS smoothparams;

	void axis(float size) const;

	bool loadKinectMatrix(std::string path, ofMatrix4x4* dst);

};

