/******************************************************************/
/**
 * @file	ofApp.h
 * @brief	Example for ofxKinectNui addon
 * @note
 * @todo
 * @bug	
 *
 * @author	sadmb
 * @date	Oct. 28, 2011
 */
/******************************************************************/
#pragma once

#include "ofMain.h"
#include "ofxKinectNui.h"
#include "ofxKinectNuiPlayer.h"
#include "ofxKinectNuiRecorder.h"

class ofxKinectNuiDrawTexture;
class ofxKinectNuiDrawSkeleton;

// uncomment this to read from two kinects simultaneously
// #define USE_TWO_KINECTS

class ofKinectApp : public ofBaseApp {

	public:

		ofKinectApp();

		/**
		 * @brief	example for adjusting video images to depth images
		 * @note	inspired by akira's video http://vimeo.com/17146552
		 */
		virtual void drawCalibratedTexture();
		//virtual void drawCircle3f(int n, int radius, ofVec3f cur);

		virtual void keyPressed(int key);
		virtual void mouseMoved(int x, int y );
		virtual void mouseDragged(int x, int y, int button);
		virtual void mousePressed(int x, int y, int button);
		virtual void mouseReleased(int x, int y, int button);
		virtual void windowResized(int w, int h);

		virtual void kinectPlugged();
		virtual void kinectUnplugged();
		virtual void startRecording();
		virtual void stopRecording();
		virtual void startPlayback();
		virtual void stopPlayback();

		ofxKinectNui kinect;

#ifdef USE_TWO_KINECTS
		ofxKinectNui kinect2;
#endif
		ofxKinectNuiPlayer kinectPlayer;
		ofxKinectNuiRecorder kinectRecorder;

		ofxBase3DVideo* kinectSource;

		ofTexture calibratedTexture;

		bool bRecord;
		bool bPlayback;
		bool bDrawVideo;
		bool bDrawDepthLabel;
		bool bDrawSkeleton;
		bool bDrawCalibratedTexture;
		bool bPlugged;
		bool bUnplugged;
		
		unsigned short nearClipping;
		unsigned short farClipping;
		int angle;
		
		int mRotationX, mRotationY;

		// Please declare these texture pointer and initialize when you want to draw them
		ofxKinectNuiDrawTexture*	videoDraw_;
		ofxKinectNuiDrawTexture*	depthDraw_;
		ofxKinectNuiDrawTexture*	labelDraw_;
		ofxKinectNuiDrawSkeleton*	skeletonDraw_;

#ifdef USE_TWO_KINECTS
		ofxKinectNuiDrawTexture*	videoDraw2_;
		ofxKinectNuiDrawTexture*	depthDraw2_;
		ofxKinectNuiDrawTexture*	labelDraw2_;
		ofxKinectNuiDrawSkeleton*	skeletonDraw2_;
#endif

		inline void setSmoothing(NUI_TRANSFORM_SMOOTH_PARAMETERS* params) {
			kinect.setSmoothing(params);
#ifdef USE_TWO_KINECTS
			kinect2.setSmoothing(params);
#endif
		}

protected:

	virtual void _setup();
	virtual void _update();
	virtual void _draw();
	virtual void _exit();

};
