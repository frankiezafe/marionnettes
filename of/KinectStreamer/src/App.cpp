#include "App.h"

//////////////////////////
// ********* APP *********
//////////////////////////

App::App() :
	ofKinectApp(),
	appUID(0),
	skkinect(0),
	skoutput(0),
	osc_senders_num(0),
	osc_receivers_num(0),
	osc_senders(0),
	osc_receivers(0),
	loadConfiguration_request(false),
	last_elapsed(0)
{

	// prepare data structures for skeletons analysis
	skq_num = kinect::nui::SkeletonFrame::SKELETON_COUNT;
#ifdef USE_TWO_KINECTS
	skq_num *= 2;
#endif

	kinect0_calibration.makeIdentityMatrix();
	kinect1_calibration.makeIdentityMatrix();

	skkinect = new SkeletonBase[skq_num];
	skoutput = new SkeletonDelta[skq_num];

	smoothparams.fSmoothing = 0.5;
	smoothparams.fCorrection = 0.5;
	smoothparams.fPrediction = 0.5;
	smoothparams.fJitterRadius = 0.5;
	smoothparams.fMaxDeviationRadius = 0.5;

	matrix_rotor.makeRotationMatrix(model_rotor);
	scale.set(0.4, 0.4, 0.4);

}

bool App::loadKinectMatrix(std::string path, ofMatrix4x4* dst) {
	ofFile kconf(ofToDataPath(path));
	if (kconf.exists()) {
		ofBuffer buffer(kconf);
		uint32_t lnum = 0;
		ofMatrix4x4* target_mat = dst;
		ofMatrix4x4 tmp_mat;
		for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
			if ((*it)[0] == '#') {
				continue;
			}
			string line = *it;
			if (line.length() < 2) {
				continue;
			}
			std::vector<std::string> args = ofSplitString(line, " ");
			if (args.size() >= 4) {
				tmp_mat._mat[lnum].set(
					atof(args[0].c_str()),
					atof(args[1].c_str()),
					atof(args[2].c_str()),
					atof(args[3].c_str())
				);
				++lnum;
				if (lnum == 4) {
					// matrix fully loaded
					(*target_mat) = tmp_mat;
					std::cout << path << " matrix successfully loaded" << std::endl;
					return true;
				}
			}
		}
	}
	return false;
}

void App::setup() {

	loadConfiguration();

	// getting application ID
	ofFile appidconf(ofToDataPath("appid"));
	if (appidconf.exists()) {
		ofBuffer buffer(appidconf);
		for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
			if ((*it)[0] == '#') {
				continue;
			}
			string line = *it;
			if (line.length() < 1) {
				continue;
			}
			appUID = atoi(line.c_str());
			std::cout << "application UID : " << int( appUID ) << std::endl;
		}
	}
	else {
		std::cout << "no application UID config! : " << int(appUID) << std::endl;
	}

	// getting kinects calibration ID
	if (!loadKinectMatrix( "kinect0", &kinect0_calibration)) {
		std::cout << "no calibration for kinect 0!" << std::endl;
	} 
	else {
		ofMatrix4x4 kci = kinect0_calibration.getInverse();
		for (uint16_t i = 0; i < kinect::nui::SkeletonFrame::SKELETON_COUNT; ++i) {
			skoutput[i].calibration_mat = kinect0_calibration;
			skoutput[i].calibration_mati = kci;
		}
	}
#ifdef USE_TWO_KINECTS
	if (!loadKinectMatrix("kinect1", &kinect1_calibration)) {
		std::cout << "no calibration for kinect 1!" << std::endl;
	}
	else {
		ofMatrix4x4 kci = kinect1_calibration.getInverse();
		for (uint16_t i = kinect::nui::SkeletonFrame::SKELETON_COUNT; i < skq_num; ++i) {
			skoutput[i].calibration_mat = kinect1_calibration;
			skoutput[i].calibration_mati = kci;
		}
	}
#endif

	// getting OSC in & out configuration
	std::vector<std::string> oscout;
	std::vector<std::string> oscin;
	ofFile oscconf(ofToDataPath("configuration.osc"));
	if (oscconf.exists()) {
		ofBuffer buffer(oscconf);
		for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
			if ((*it)[0] == '#') {
				continue;
			}
			string line = *it;
			if (line.length() < 2) {
				continue;
			}
			std::vector<std::string> args = ofSplitString(line, " ");
			if (args.size() >= 2) {
				if (args[0].compare("OUT") == 0 && !args[1].empty()) {
					if (args[1].find(":") != std::string::npos) {
						oscout.push_back(args[1]);
					}
				}
				else if (args[0].compare("IN") == 0 && !args[1].empty()) {
					oscin.push_back(args[1]);
				}
			}
		}
	}

	// pushing standard OSC IN
	std::stringstream ss; ss << KINECT_CONTROL_PORT;
	bool found = false;
	for (std::vector<std::string>::iterator it = oscin.begin(); it != oscin.end(); ++it) {
		if ((*it).compare(ss.str()) == 0) {
			found = true;
		}
	}
	if (!found) {
		oscin.push_back(ss.str());
	}
	
	if (!oscout.empty()) {
		osc_senders_num = oscout.size();
		osc_senders = new ofxOscSender[osc_senders_num];
		for (size_t i = 0; i < osc_senders_num; ++i) {
			std::cout << "osc sender : " << oscout[i] << std::endl;
			std::vector<std::string> p = ofSplitString(oscout[i], ":");
			std::string ip = p[0];
			uint16_t port = atoi(p[1].c_str());
			osc_senders[i].setup(ip, port);
#ifdef USE_PARALLEL_PORTS
			size_t pp_num = skq_num;
			ofxOscSender* pp = new ofxOscSender[pp_num];
			size_t ppi = 0;
			for (uint8_t ki = 0; ki < skq_num / kinect::nui::SkeletonFrame::SKELETON_COUNT; ++ki) {
				for (uint8_t ski = 0; ski < kinect::nui::SkeletonFrame::SKELETON_COUNT; ++ski) {
					uint16_t pp_port = port + appUID * 100 + ki * 10 + ski + 1;
					std::cout << "osc parallel sender : " << ip << ":" << pp_port << std::endl;
					pp[ppi].setup(ip, pp_port);
					++ppi;
				}
			}
			osc_parallel_senders.push_back(pp);
#endif
		}
	}
	if (!oscin.empty()) {
		osc_receivers_num = oscin.size();
		osc_receivers = new ofxOscReceiver[osc_receivers_num];
		for (size_t i = 0; i < osc_receivers_num; ++i) {
			std::cout << "osc receiver : " << oscin[i] << std::endl;
			osc_receivers[i].setup(atoi(oscin[i].c_str()));
		}
	}

	ofKinectApp::_setup();

}

void App::loadConfiguration() {

	mapping.clear();

	// loading mapping
	ofFile conf_mapping(ofToDataPath("avatar.mapping"));
	if (conf_mapping.exists()) {
		ofBuffer buffer(conf_mapping);
		for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
			if ((*it)[0] == '#') {
				continue;
			}
			string line = *it;
			if (line.length() < 2) {
				continue;
			}
			addMap(line);
		}
		std::vector<MappingKey>::iterator itm = mapping.begin();
		std::vector<MappingKey>::iterator itme = mapping.end();
		for (; itm != itme; ++itm) {
			std::cout << (*itm).print() << std::endl;
		}
	}
	else {
		std::cout << "FAILED TO LOAD avatar.mapping!" << std::endl;
	}

	ofFile conf_filtering(ofToDataPath("filtering"));
	if (conf_filtering.exists()) {
		ofBuffer buffer(conf_filtering);
		for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
			if ((*it)[0] == '#') {
				continue;
			}
			string line = *it;
			if (line.length() < 2) {
				continue;
			}
			std::vector< std::string > words = ofSplitString(line, " ");
			if (words.size() > 1) {
				if (words[0].compare("fSmoothing") == 0) {
					smoothparams.fSmoothing = atof(words[1].c_str());
				} 
				else if (words[0].compare("fCorrection") == 0) {
					smoothparams.fCorrection = atof(words[1].c_str());
				}
				else if (words[0].compare("fPrediction") == 0) {
					smoothparams.fPrediction = atof(words[1].c_str());
				}
				else if (words[0].compare("fJitterRadius") == 0) {
					smoothparams.fJitterRadius = atof(words[1].c_str());
				}
				else if (words[0].compare("fMaxDeviationRadius") == 0) {
					smoothparams.fMaxDeviationRadius = atof(words[1].c_str());
				}
			}
		}
		std::cout << "filtering: " << std::endl <<
			"\tfSmoothing: " << smoothparams.fSmoothing << std::endl <<
			"\tfCorrection: " << smoothparams.fCorrection << std::endl <<
			"\tfPrediction: " << smoothparams.fPrediction << std::endl <<
			"\tfJitterRadius: " << smoothparams.fJitterRadius << std::endl <<
			"\tfMaxDeviationRadius: " << smoothparams.fMaxDeviationRadius << std::endl;
		setSmoothing(&smoothparams);
	}
	else {
		std::cout << "FAILED TO LOAD filtering!" << std::endl;
	}

	ofFile matconf(ofToDataPath("matrix4x4"));
	if (matconf.exists()) {
		ofBuffer buffer(matconf);
		uint32_t lnum = 0;
		ofMatrix4x4* target_mat = 0;
		ofMatrix4x4* target_mati = 0;
		ofVec3f* axis;
		ofMatrix4x4 tmp_mat;
		for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
			if ((*it)[0] == '#') {
				continue;
			}
			string line = *it;
			if (line.length() < 2) {
				continue;
			}
			// flag to select matrix
			if (line.c_str()[0] == '>') {
				if (line.compare(">OUTPUT_POS") == 0) {
					std::cout << "Loading OUTPUT position matrix." << std::endl;
					lnum = 0;
					axis = 0;
					target_mat = &skoutput[0].pos_mat;
					target_mati = &skoutput[0].pos_mati;
					tmp_mat.makeIdentityMatrix();
				}
				else if (line.compare(">OUTPUT_ROT") == 0) {
					std::cout << "Loading OUTPUT rotation matrix." << std::endl;
					lnum = 0;
					axis = 0;
					target_mat = &skoutput[0].rot_mat;
					target_mati = &skoutput[0].rot_mati;
					tmp_mat.makeIdentityMatrix();
				}
				else if (line.compare(">MODEL_POS") == 0) {
					std::cout << "Loading MODEL position matrix." << std::endl;
					lnum = 0;
					axis = 0;
					target_mat = &skmodel.pos_mat;
					target_mati = &skmodel.pos_mati;
					tmp_mat.makeIdentityMatrix();
				}
				else if (line.compare(">MODEL_ROT") == 0) {
					std::cout << "Loading MODEL rotation matrix." << std::endl;
					lnum = 0;
					axis = 0;
					target_mat = &skmodel.rot_mat;
					target_mati = &skmodel.rot_mati;
					tmp_mat.makeIdentityMatrix();
				}
				else if (line.compare(">KINECT_POS") == 0) {
					std::cout << "Loading KINECT position matrix." << std::endl;
					lnum = 0;
					axis = 0;
					target_mat = &skkinect[0].pos_mat;
					target_mati = &skkinect[0].pos_mati;
					tmp_mat.makeIdentityMatrix();
				}
				else if (line.compare(">KINECT_ROT") == 0) {
					std::cout << "Loading KINECT rotation matrix." << std::endl;
					lnum = 0;
					axis = 0;
					target_mat = &skkinect[0].rot_mat;
					target_mati = &skkinect[0].rot_mati;
					tmp_mat.makeIdentityMatrix();
				}
				// set this to load a vector3 from the "matrix4x4" file
				//else if (line.compare(">UP") == 0) {
				//	lnum = 0;
				//	axis = &UP;
				//	target_mat = 0;
				//}
			}
			else if (axis != 0) {
				std::vector<std::string> args = ofSplitString(line, " ");
				if (args.size() >= 3) {
					axis->set(
						atof(args[0].c_str()),
						atof(args[1].c_str()),
						atof(args[2].c_str())
					);
					//if (axis == &UP) {
					//	std::cout << "UP axis successfully loaded." << std::endl;
					//}
					axis = 0;
				}
			
			}
			else if (target_mat != 0) {
				std::vector<std::string> args = ofSplitString(line, " ");
				if (args.size() >= 4) {
					tmp_mat._mat[lnum].set(
						atof(args[0].c_str()),
						atof(args[1].c_str()),
						atof(args[2].c_str()),
						atof(args[3].c_str())
					);
					++lnum;
					if (lnum == 4) {
						// matrix fully loaded
						*target_mat = tmp_mat;
						*target_mati = target_mat->getInverse();
						if (target_mat == &skkinect[0].pos_mat) {
							for (uint8_t i = 1; i < skq_num; ++i) {
								skkinect[i].pos_mat = skkinect[0].pos_mat;
								skkinect[i].check_alignment();
							}
							std::cout << "KINECT position matrix successfully loaded." << std::endl;
						}
						else if (target_mat == &skkinect[0].rot_mat) {
							for (uint8_t i = 1; i < skq_num; ++i) {
								skkinect[i].rot_mat = skkinect[0].rot_mat;
								skkinect[i].check_alignment();
							}
							std::cout << "KINECT rotation matrix successfully loaded." << std::endl;
						}
						else if (target_mat == &skmodel.pos_mat) {
							std::cout << "MODEL position matrix successfully loaded." << std::endl;
							skmodel.loadModel();
						}
						else if (target_mat == &skmodel.rot_mat) {
							std::cout << "MODEL rotation matrix successfully loaded." << std::endl;
							skmodel.loadModel();
						}
						else if (target_mat == &skoutput[0].pos_mat) {
							for (uint8_t i = 1; i < skq_num; ++i) {
								skoutput[i].pos_mat = skoutput[0].pos_mat;
								skoutput[i].check_alignment();
							}
							std::cout << "OUTPUT position matrix successfully loaded." << std::endl;
						}
						else if (target_mat == &skoutput[0].rot_mat) {
							for (uint8_t i = 1; i < skq_num; ++i) {
								skoutput[i].rot_mat = skoutput[0].rot_mat;
								skoutput[i].check_alignment();
							}
							std::cout << "OUTPUT rotation matrix successfully loaded." << std::endl;
						}
						else {
							for (uint8_t m = 0; m < 4; ++m) {
								std::cout << "\t" <<
									target_mat->_mat[m].x << "\t" <<
									target_mat->_mat[m].y << "\t" <<
									target_mat->_mat[m].z << "\t" <<
									target_mat->_mat[m].w << std::endl;
							}
						}
						target_mat = 0;
					}
				}
				else {
					if (target_mat == &skoutput[0].pos_mat)
						std::cout << "Failed to load OUTPUT position matrix!" << std::endl;
					if (target_mat == &skoutput[0].rot_mat)
						std::cout << "Failed to load OUTPUT rotation matrix!" << std::endl;
					else if (target_mat == &skmodel.pos_mat)
						std::cout << "Failed to load MODEL position matrix!" << std::endl;
					if (target_mat == &skmodel.rot_mat)
						std::cout << "Failed to load MODEL rotation matrix!" << std::endl;
					else if (target_mat == &skkinect[0].pos_mat)
						std::cout << "Failed to load KINECT position matrix!" << std::endl;
					else if (target_mat == &skkinect[0].rot_mat)
						std::cout << "Failed to load KINECT rotation matrix!" << std::endl;
					target_mat = 0;
				}
			}
		}
	}

}

uint8_t App::stringToBoneIndex(std::string b) {
	
	for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {
		if (b.compare(SKstatic::bone_key(i)) == 0) {
			return i;
		}
	}
	return NUI_SKELETON_POSITION_COUNT;

}

void App::addMap(std::string line) {

	vector<string> args = ofSplitString(line, " ");
	size_t c = args.size();
	if ( c < 2 || args[0].empty() || ( c > 2 && c % 2 == 0 ) ) {
		std::cout << "Wrong configuration: " << line << std::endl;
		return;
	}
	else if (c == 2) {
		MappingKey mk(args[0], 1);
		uint8_t boneid = stringToBoneIndex(args[1]);
		if (boneid != NUI_SKELETON_POSITION_COUNT) {
			mk.add(0, boneid);
			mapping.push_back(mk);
		}
		else {
			std::cout << "Wrong bone name: " << line << std::endl;
			return;
		}
	}
	else {
		MappingKey mk(args[0], (c-1) / 2);
		for (size_t i = 1, k = 0; i < c; i += 2, ++k) {
			uint8_t boneid = stringToBoneIndex(args[i]);
			if (boneid != NUI_SKELETON_POSITION_COUNT) {
				mk.add(k, boneid, atof(args[i + 1].c_str()));
				mapping.push_back(mk);
			}
			else {
				std::cout << "Wrong bone name: " << line << std::endl;
				return;
			}
		}
		mapping.push_back(mk);
		return;
	}

}

void App::exit() {

	ofKinectApp::_exit();
	if (osc_senders) {
		delete[] osc_senders;
		osc_senders = 0;
	}
	if (osc_receivers) {
		delete[] osc_receivers;
		osc_receivers = 0;
	}	
	if (skkinect) {
		delete[] skkinect;
		skkinect = 0;
	}
	if (skoutput) {
		delete[] skoutput;
		skoutput = 0;
	}

#ifdef USE_PARALLEL_PORTS
	std::vector< ofxOscSender* >::iterator itpp = osc_parallel_senders.begin();
	std::vector< ofxOscSender* >::iterator itppe = osc_parallel_senders.end();
	for (; itpp != itppe; ++itpp) {
		delete[](*itpp);
	}
	osc_parallel_senders.clear();
#endif

}

void App::parseOsc() {

	ofxOscMessage m;
	for (size_t i = 0; i < osc_receivers_num; ++i) {
		ofxOscReceiver& rcvr = osc_receivers[i];
		while (rcvr.hasWaitingMessages()) {
			rcvr.getNextMessage(m);
			if (m.getAddress().compare(OSC_MATRIX_REQUEST) == 0) {
				sendMatrices();
			}
			else if (m.getAddress().compare(OSC_MATRIX_UPDATE) == 0) {
				updateMatrice(m);
			}
			else if (m.getAddress().compare(OSC_FILTER_REQUEST) == 0) {
				sendFilter();
			}
			else if (m.getAddress().compare(OSC_FILTER_UPDATE) == 0) {
				updateFilter(m);
			}
		}
	}

}

void App::sendFilter() {

	ofxOscMessage m;
	m.setAddress(OSC_FILTER_UPLOAD);
	m.addInt32Arg(appUID);
	m.addFloatArg(smoothparams.fCorrection);
	m.addFloatArg(smoothparams.fJitterRadius);
	m.addFloatArg(smoothparams.fMaxDeviationRadius);
	m.addFloatArg(smoothparams.fPrediction);
	m.addFloatArg(smoothparams.fSmoothing);
	for (size_t o = 0; o < osc_senders_num; ++o) {
		osc_senders->sendMessage(m, false);
	}

}

void App::updateFilter(ofxOscMessage& m) {
	
	smoothparams.fCorrection =			m.getArgAsFloat(0);
	smoothparams.fJitterRadius =		m.getArgAsFloat(1);
	smoothparams.fMaxDeviationRadius =	m.getArgAsFloat(2);
	smoothparams.fPrediction =			m.getArgAsFloat(3);
	smoothparams.fSmoothing =			m.getArgAsFloat(4);

	std::string path = ofToDataPath("filtering");
	std::stringstream ss;
	ss << "fCorrection " << smoothparams.fCorrection << std::endl;
	ss << "fJitterRadius " << smoothparams.fJitterRadius << std::endl;
	ss << "fMaxDeviationRadius " << smoothparams.fMaxDeviationRadius << std::endl;
	ss << "fPrediction " << smoothparams.fPrediction << std::endl;
	ss << "fSmoothing " << smoothparams.fSmoothing;
	std::ofstream output;
	output.open(path, ios::out);
	std::cout << "backup of " << path << ", " << output.is_open() << std::endl;
	output.write(ss.str().c_str(), ss.str().size());
	output.close();

	std::cout << "new filter: " << std::endl <<
		ss.str() << std::endl;

}

void App::sendMatrices() {
	
	ofxOscMessage m;
	m.setAddress(OSC_MATRIX_UPLOAD);
	m.addInt32Arg(appUID);
	m.addInt32Arg(0);
	for (uint8_t i = 0; i < 16; ++i) {
		m.addFloatArg(kinect0_calibration.getPtr()[i]);
	}
	for (size_t o = 0; o < osc_senders_num; ++o) {
		osc_senders->sendMessage(m, false);
	}
#ifdef USE_TWO_KINECTS
	m.clear();
	m.setAddress(OSC_MATRIX_UPLOAD);
	m.addInt32Arg(appUID);
	m.addInt32Arg(1);
	for (uint8_t i = 0; i < 16; ++i) {
		m.addFloatArg(kinect1_calibration.getPtr()[i]);
	}
	for (size_t o = 0; o < osc_senders_num; ++o) {
		osc_senders->sendMessage(m, false);
	}
#endif

}

void App::updateMatrice(ofxOscMessage& m) {

	int kid = m.getArgAsInt(0);
	ofMatrix4x4* target = 0;
	std::string path;
	switch (kid) {
		case 0:
			target = &kinect0_calibration;
			path = "kinect0";
			break;
		case 1:
			target = &kinect1_calibration;
			path = "kinect1";
			break;
		default:
			std::cout << "updateMatrice, wrong kinect id! " << int(kid) << std::endl;
			return;
	}
	for (uint8_t i = 0; i < 16; ++i) {
		target->getPtr()[i] = m.getArgAsFloat(i + 1);
	}

	path = ofToDataPath(path);

	std::ofstream output;
	output.open(path, ios::out);
	std::cout << "backup of " << path << ", " << output.is_open() << std::endl;
	std::stringstream ss;
	for (uint8_t i = 0; i < 16; ++i) {
		if (i > 0 && i % 4 == 0) {
			ss << std::endl;
		}
		ss << target->getPtr()[i];
		if (i % 4 != 3) {
			ss << " ";
		}
	}
	std::string ocontent = ss.str();
	output.write(ocontent.c_str(), ocontent.size());
	output.close();

	uint8_t sk_start = kid * kinect::nui::SkeletonFrame::SKELETON_COUNT;
#ifndef USE_TWO_KINECTS
	if (sk_start > 0) {
		return;
	}
#endif
	ofMatrix4x4 kci = (*target).getInverse();
	for (uint16_t i = 0; i < kinect::nui::SkeletonFrame::SKELETON_COUNT; ++i) {
		skoutput[i + sk_start].calibration_mat = (*target);
		skoutput[i + sk_start].calibration_mati = kci;
	}

}

void App::update() {

	parseOsc();

	if (loadConfiguration_request) {
		loadConfiguration();
		loadConfiguration_request = false;
	}

	ofKinectApp::_update();
 
	float to_rad = PI / 180;
	std::vector<MappingKey>::iterator itm;
	std::vector<MappingKey>::iterator itme = mapping.end();
	ofxOscMessage* msg = 0;
	ofxKinectNui* kn = &kinect;

	for (uint16_t i = 0; i < skq_num; ++i) {

		if (i == kinect::nui::SkeletonFrame::SKELETON_COUNT) {
#ifdef USE_TWO_KINECTS
			kn = &kinect2;
#endif		
		}

		uint8_t kid = 0;
		if (i >= kinect::nui::SkeletonFrame::SKELETON_COUNT) {
			kid = 1;
		}
		uint8_t skid = i % kinect::nui::SkeletonFrame::SKELETON_COUNT;
		const ofPoint* pts = kn->getSkeletonAbsolutePoints(skid);
		const NUI_SKELETON_BONE_ORIENTATION* ori = kn->getSkeletonOrientation(skid);
		
		if (!pts || !ori) {
			skoutput[i].updated = false;
			continue;
		}

		ofVec3f origin = pts[NUI_SKELETON_POSITION_HIP_CENTER];
		skkinect[i].set(origin);
		for (uint8_t b = 0; b < NUI_SKELETON_POSITION_COUNT; ++b) {
			ofVec3f rel = ( pts[b] - origin );
			skkinect[i].set( 
				b, rel, 
				vec4toQuaternion( ori[b].absoluteRotation.rotationQuaternion ) 
			);
		}

		// rendering the relative orientations
		skoutput[i].diff(skkinect[i], skmodel);

		// full skeleton
		msg = new ofxOscMessage();
		msg->setAddress(OSC_SKELETON_FRAME);
		msg->addInt32Arg(appUID);
		msg->addInt32Arg(kid);
		msg->addInt32Arg(skid);
		// absolute position
		ofVec3f& hipc = skoutput[i].output_positions[NUI_SKELETON_POSITION_HIP_CENTER];
		msg->addFloatArg(hipc.x);
		msg->addFloatArg(hipc.y);
		msg->addFloatArg(hipc.z);
		itm = mapping.begin();
		while (itm != itme) {
			MappingKey& mk = (*itm);
			ofQuaternion q;
			for (size_t m = 0; m < mk.size(); ++m) {
				q.slerp(mk[m].second, q, skoutput[i].output_rotations[mk[m].first]);
			}
			msg->addStringArg(mk.name());
			msg->addFloatArg(q._v.w);
			msg->addFloatArg(q._v.x);
			msg->addFloatArg(q._v.y);
			msg->addFloatArg(q._v.z);
			++itm;
		}
#ifdef USE_PARALLEL_PORTS
		std::vector< ofxOscSender* >::iterator itpp = osc_parallel_senders.begin();
		std::vector< ofxOscSender* >::iterator itppe = osc_parallel_senders.end();
		for (; itpp != itppe; ++itpp) {
			(*itpp)[i].sendMessage((*msg), false);
		}
#else
		for (size_t o = 0; o < osc_senders_num; ++o) {
			osc_senders->sendMessage((*msg), false);
		}
#endif
		delete msg;

		// raw positions
		msg = new ofxOscMessage();
		msg->setAddress(OSC_SKELETON_POSITIONS);
		msg->addInt32Arg(appUID);
		msg->addInt32Arg(kid);
		msg->addInt32Arg(skid);
		for (uint8_t b = 0; b < NUI_SKELETON_POSITION_COUNT; ++b) {
			ofVec3f p = skoutput[i].output_positions[b];
			msg->addFloatArg(p.x);
			msg->addFloatArg(p.y);
			msg->addFloatArg(p.z);
		}
		for (size_t o = 0; o < osc_senders_num; ++o) {
			osc_senders[o].sendMessage((*msg), false);
		}
		delete msg;

	}

}

void App::axis(float size) const {

	glLineWidth(3);
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(size, 0, 0);
	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, size, 0);
	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, size);
	glEnd();
	glLineWidth(1);
}

void App::grid(bool x, bool y, bool z) {
	glLineWidth(0.5);
	glColor4f(1, 1, 1, 0.5);
	glBegin(GL_LINES);
	for (float v = -0.5; v <= 0.5; v += 0.1) {
		if (x) {
			glColor4f(1, 0, 0, 0.5);
			glVertex3f(0, v, -0.5);
			glVertex3f(0, v, 0.5);
			glVertex3f(0, -0.5, v);
			glVertex3f(0, 0.5, v);
		}
		if (y) {
			glColor4f(0, 1, 0, 0.5);
			glVertex3f(v, 0, -0.5);
			glVertex3f(v, 0, 0.5);
			glVertex3f(-0.5, 0, v);
			glVertex3f(0.5, 0, v);
		}
		if (z) {
			glColor4f(0, 0, 1, 0.5);
			glVertex3f(v, -0.5, 0);
			glVertex3f(v, 0.5, 0);
			glVertex3f(-0.5, v, 0);
			glVertex3f(0.5, v, 0);
		}
	}
	glEnd();
}

void App::draw() {

	ofSetupScreenPerspective(ofGetWidth(), ofGetHeight(), 30, 0, 10000);

	ofBackground(50, 50, 50);

	glDisable(GL_DEPTH_TEST);

	ofKinectApp::_draw();

	glEnable(GL_DEPTH_TEST);

	// visualisation of matrix transformation
	ofPushMatrix();

		ofTranslate(ofGetWidth()*0.5, ofGetHeight()*0.5, 0);
		ofVec3f sc = scale * ofGetHeight() * ofVec3f(-1, 1, 1);
		ofScale(sc);
		ofMultMatrix(matrix_rotor);
		ofTranslate(translate);

		ofPushMatrix();
			axis(0.1);
			ofTranslate(0, 0, 0.6);
			ofPushMatrix();
				grid();
				skmodel.draw();
				ofMultMatrix(skmodel.rot_mati);
				axis(0.1);
			ofPopMatrix();

			ofTranslate(0, 0, -1.2);
			// num row
			int row = skq_num / 3;
			float grid_cells = 1.1;
			float grid_offy = (row > 2) ? grid_cells : 0;
			uint8_t kid = 0;
			for (uint8_t r = 0; r < row; ++r) {
				for (uint8_t c = 0; c < 3; ++c) {
					ofPushMatrix();
					ofTranslate(
						-grid_cells + c * grid_cells, 
						-(grid_offy + grid_cells * 0.5) + r * grid_cells, 0
					);
					glColor4f(1, 1, 1, 1);
					stringstream ss; ss << int(kid);
					grid(false, false, true);
					if (skoutput[kid].updated) {
						skoutput[kid].draw();
					}
					ofMultMatrix(skmodel.origin_mat);
					skkinect[kid].lines();
					ofVec3f& h = skkinect[kid].absolute_positions[NUI_SKELETON_POSITION_HEAD];
					ofTranslate(h.x, h.y, h.z);
					ofDrawBitmapString(ss.str(), 0, 0);
					ofPopMatrix();
					kid++;
				}
			}

		ofPopMatrix();

	ofPopMatrix();

	float deltat = ofGetElapsedTimef() - last_elapsed;
	last_elapsed = ofGetElapsedTimef();

	ofPopMatrix();

	ofSetLineWidth(1);
	ofSetColor(255,255,255);

}

void App::keyPressed(int key) {

	switch (key)
	{
		case ' ':
			loadConfiguration_request = true;
			break;
		default:
			ofKinectApp::keyPressed(key);
			break;
	}

}

void App::mouseMoved(int x, int y) {

	ofKinectApp::mouseMoved(x, y);

}

void App::mouseDragged(int x, int y, int button) {

	ofKinectApp::mouseDragged(x, y, button);

	ofVec2f mp(x, y);
	ofVec2f diff = (mp - prev_mouse) / ofGetWidth();
	//    model_rotor *= ofQuaternion(-diff.y * 200, ofVec3f(1, 0, 0));
	//    model_rotor *= ofQuaternion(diff.x * 200, ofVec3f(0, 1, 0));
	if (button == 0) {
		scale += diff.y;
		if (scale.x < 0.0001) {
			scale.set(0.0001, 0.0001, 0.0001);
		}
	}
	else if (button == 1) {

		ofMatrix4x4 mr(model_rotor);
		ofMatrix4x4 mri = mr.getInverse();
		ofMatrix4x4 t;
		t.makeTranslationMatrix(ofVec3f(1, 0, 0));
		t = mr * t * mri;
		ofVec3f side = t.getTranslation();
		ofVec3f front = ofQuaternion(90, ofVec3f(0, 0, 1)) * side;
		translate +=
			-diff.x * 10 * side +
			diff.y * 10 * front;

	}
	else if (button == 2) {

		ofVec3f up = model_rotor * ofVec3f(0, 0, 1);
		ofVec3f right = ofVec3f(1, 0, 0);
		model_rotor *= ofQuaternion(diff.x * 200, up);
		model_rotor *= ofQuaternion(diff.y * 200, right);
		matrix_rotor.makeRotationMatrix(model_rotor);

	}
	prev_mouse = mp;

}

void App::mousePressed(int x, int y, int button) {

	ofKinectApp::mousePressed(x, y, button);
	prev_mouse.set(x, y);

}

void App::mouseReleased(int x, int y, int button) {

	ofKinectApp::mouseReleased(x,y,button);

}

void App::windowResized(int w, int h) {
	
	ofKinectApp::windowResized(w, h);

}
