# filtering kinect skeletons

source: https://msdn.microsoft.com/en-us/library/nuiskeleton.nui_transform_smooth_parameters.aspx

filter config is using the formating of other configuration files

* hash char is used for comments
* empty lines are ignored
* valid entry format: [name of the value][1 space][float value]

## default values

fSmoothing 0.5
fCorrection 0.5
fPrediction 0.5
fJitterRadius 0.5
fMaxDeviationRadius 0.5

## smoothed with some latency - by default in streamer

fSmoothing 0.5
fCorrection 0.1
fPrediction 0.5
fJitterRadius 0.1
fMaxDeviationRadius 0.1

## very smooth, but with a lot of latency

fSmoothing 0.7
fCorrection 0.3
fPrediction 1.0
fJitterRadius 1.0
fMaxDeviationRadius 1.0