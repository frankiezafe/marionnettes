#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	
	std::string ip = "192.168.3.68";
	int port = 23000;
	int sknum = 6;
	for( int appid = 0; appid < 3; ++appid ) {
	for( int i = 0; i < sknum; ++i ) {
		ofxOscSender* s = new ofxOscSender();
		s->setup( ip, port + appid * 10 + i );
		senders.push_back( s );
	}
	}
	sent_messages = 0;

}

//--------------------------------------------------------------
void ofApp::update(){

	// at 30 fps
	if ( ofGetFrameNum() % 2 == 0 ) {
		return;
	}
	
	ofxOscMessage msg;
	msg.setAddress( "/tester" );
	for( int i = 0; i < 20; ++i ) {
		msg.addStringArg( "some name" );
		msg.addFloatArg( ofRandomuf() );
		msg.addFloatArg( ofRandomuf() );
		msg.addFloatArg( ofRandomuf() );
		msg.addFloatArg( ofRandomuf() );
	}
	for( int i = 0; i < senders.size(); ++i ) {
		senders[i]->sendMessage(msg, false);
		++sent_messages;
	}
	cout << "sent messages: " << sent_messages << endl;
	
}

//--------------------------------------------------------------
void ofApp::draw(){

	ofBackground( 0,0,0 );
	ofDrawBitmapString( ofToString( ofGetFrameRate() ), 10, 25 );
	
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}