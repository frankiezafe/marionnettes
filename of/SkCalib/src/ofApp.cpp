#include "ofApp.h"

ofApp::ofApp() {

    matrix_rotor.makeRotationMatrix(model_rotor);
    scale.set(0.4, 0.4, 0.4);

    ofDirectory data(ofToDataPath(""));
    const vector<ofFile>& fs = data.getFiles();
    for (size_t i = 0; i < fs.size(); ++i) {
        std::cout << fs[i].path() << std::endl;
        files.push_back(ofToDataPath(fs[i].path(), true));
    }

    link_calibrated = false;

    file2load = 0;
    process();

}

void ofApp::process() {

    vec3pairs.clear();
    rotated.clear();
    calibrated.clear();

    std::string p = files[ file2load ];

    raw_reader.read_open(p);
    size_t fnum = raw_reader.frame_num();
    float tdiv = 0.5f / fnum;
    translate.set(0, 0, 0);
    for (size_t i = 0; i < fnum; ++i) {
        raw_reader.read_frame(i, raw_content);
        vec3pairs.push_back(
                std::pair< ofVec3f, ofVec3f >(
                ofVec3f(
                raw_content.vec3s[0][0],
                raw_content.vec3s[0][1],
                raw_content.vec3s[0][2]
                ),
                ofVec3f(
                raw_content.vec3s[1][0],
                raw_content.vec3s[1][1],
                raw_content.vec3s[1][2]
                )
                )
                );
        translate.x -= raw_content.vec3s[0][0] * tdiv;
        translate.x -= raw_content.vec3s[1][0] * tdiv;
        translate.y -= raw_content.vec3s[0][1] * tdiv;
        translate.y -= raw_content.vec3s[1][1] * tdiv;
        translate.z -= raw_content.vec3s[0][2] * tdiv;
        translate.z -= raw_content.vec3s[1][2] * tdiv;
        raw_content.clear();
    }
    raw_reader.read_close();

    // processing vec3pairs
    size_t vnum = vec3pairs.size();
    size_t vvalid = 0;
    Vec3PairListIter it;
    Vec3PairListIter itprev;
    Vec3PairListIter itend;
    Vec3PairList directions;

    float* weight = new float[vnum];
    float weight_total = 0;

    ofVec3f zero(0, 0, 0);

    // verification of data + directions process
    it = vec3pairs.begin();
    itprev = vec3pairs.begin();
    itend = vec3pairs.end();
    ++it;
    size_t i = 1;
    for (; it != itend; ++it, ++i) {
        bool valid = true;
        ofVec3f delta_first = it->first - itprev->first;
        if (delta_first.lengthSquared() < 1.e-5) {
            std::cout << "duplicated first position at " << i << std::endl;
            valid = false;
        }
        ofVec3f delta_second = it->second - itprev->second;
        if (delta_second.lengthSquared() < 1.e-5) {
            std::cout << "duplicated second position at " << i << std::endl;
            valid = false;
        }
        if (valid) {
            ++vvalid;
            weight[ i - 1 ] = it->first.distance(it->second) * (
                    delta_first.length() + delta_second.length()
                    );
            weight_total += weight[ i - 1 ];
            directions.push_back(
                    std::pair<ofVec3f, ofVec3f>(
                    delta_first.normalize(),
                    delta_second.normalize()
                    )
                    );
        } else {
            weight[ i - 1 ] = 0;
            directions.push_back(
                    std::pair<ofVec3f, ofVec3f>(zero, zero)
                    );
        }
        rotated_dir.push_back(zero);
    }

    ofQuaternion calibration_q;
    calibration_matrix.makeIdentityMatrix();
    ofMatrix4x4 calibration_mati;
    ofMatrix4x4 work_mat;
    float angl_div = 1.f / vvalid;

    // decomposition of the orientation in 3 axis:
    // Z first (~yaw) -> angle perpendicular to ground
    // X (~tilt) -> up/down angle
    // Y (~roll) -> if kinect transversal axis is not parallel to ground, the
    // less likely

    // ************ Z ************

    float zangl = 0;
    it = directions.begin();
    itend = directions.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        if (it->first == zero) {
            continue;
        }
        // projection of the direction on XY plane
        ofVec2f first(it->first.x, it->first.y);
        ofVec2f second(it->second.x, it->second.y);
        zangl += second.angle(first) * weight[i] / weight_total;
    }

    std::cout << "average Z angle " << zangl << std::endl;

    calibration_q = ofQuaternion(zangl, ofVec3f(0, 0, 1));
    calibration_matrix.makeRotationMatrix(calibration_q);
    calibration_mati = calibration_matrix.getInverse();

    // applying the Z correction on points & rendering new directions
    it = vec3pairs.begin();
    itprev = vec3pairs.begin();
    itend = vec3pairs.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        work_mat.makeTranslationMatrix(it->second);
        work_mat = work_mat * calibration_matrix;
        rotated.push_back(work_mat.getTranslation());
        if (i != 0 && directions[i - 1].first != zero) {
            work_mat.makeTranslationMatrix(directions[i - 1].second);
            work_mat = calibration_mati * work_mat * calibration_matrix;
            rotated_dir[i - 1] = work_mat.getTranslation();
        }
    }

    // ************ X ************

    float xangl = 0;
    it = directions.begin();
    itend = directions.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        if (it->first == zero) {
            continue;
        }
        // projection of the direction on YZ plane
        ofVec2f first(it->first.y, it->first.z);
        ofVec2f second(rotated_dir[i].y, rotated_dir[i].z);
        xangl += second.angle(first) * weight[i] / weight_total;
    }

    std::cout << "average X angle " << xangl << std::endl;

    ofVec3f right(1, 0, 0);
    right = calibration_q * right;
    calibration_q *= ofQuaternion(xangl, right);
    calibration_matrix.makeRotationMatrix(calibration_q);
    calibration_mati = calibration_matrix.getInverse();

    // applying the X correction on points & rendering new directions
    it = vec3pairs.begin();
    itprev = vec3pairs.begin();
    itend = vec3pairs.end();
    i = 0;
    for (; it != itend; ++it, ++i) {
        work_mat.makeTranslationMatrix(it->second);
        work_mat = work_mat * calibration_matrix;
        rotated[i] = work_mat.getTranslation();
//        if (i != 0 && directions[i - 1].first != zero) {
//            directions[i - 1].second = rotated[i] - rotated[i - 1];
//            directions[i - 1].second.normalize();
//        }
        if (i != 0 && directions[i - 1].first != zero) {
            work_mat.makeTranslationMatrix(directions[i - 1].second);
            work_mat = calibration_mati * work_mat * calibration_matrix;
            rotated_dir[i - 1] = work_mat.getTranslation();
        }
    }

    // ************ Y ************

//    float yangl = 0;
//    it = directions.begin();
//    itend = directions.end();
//    i = 0;
//    for (; it != itend; ++it, ++i) {
//        if (it->first == zero) {
//            continue;
//        }
//        // projection of the direction on XZ plane
//        ofVec2f first(it->first.x, it->first.z);
//        ofVec2f second(rotated_dir[i].x, rotated_dir[i].z);
//        yangl += second.angle(first) * weight[i] / weight_total;
//    }
//
//    std::cout << "average Y angle " << yangl << std::endl;
//    
//    ofVec3f front(0, 1, 0);
//    front = calibration_q * front;
//    calibration_q *= ofQuaternion(yangl, front);
//    calibration_matrix.makeRotationMatrix(calibration_q);
//    calibration_mati = calibration_matrix.getInverse();
//
//    // applying the Y correction on points & rendering new directions
//    it = vec3pairs.begin();
//    itprev = vec3pairs.begin();
//    itend = vec3pairs.end();
//    i = 0;
//    for (; it != itend; ++it, ++i) {
//        work_mat.makeTranslationMatrix(it->second);
//        work_mat = calibration_mati * work_mat * calibration_matrix;
//        rotated[i] = work_mat.getTranslation();
//    }

    // back to positions
    it = vec3pairs.begin();
    itend = vec3pairs.end();

    // rendering the average difference between rotated position and originals
    float lerp = 1.f / vnum;
    ofVec3f median_offset(0, 0, 0);
    for (; it != itend; ++it, ++i) {
        work_mat.makeTranslationMatrix(it->second);
        work_mat = calibration_mati * work_mat * calibration_matrix;
        ofVec3f prtd = work_mat.getTranslation();
        median_offset += (it->first - prtd) * lerp;
    }

    work_mat.makeTranslationMatrix(median_offset);
    calibration_matrix *= work_mat;
    calibration_mati = calibration_matrix.getInverse();

    // rendering the calibrated points
    it = vec3pairs.begin();
    for (; it != itend; ++it, ++i) {
        work_mat.makeTranslationMatrix(it->second);
        work_mat = work_mat * calibration_matrix;
        calibrated.push_back(work_mat.getTranslation());
    }

    delete[] weight;
    
    std::stringstream ss;
    ss << "yaw (z): " << zangl << std::endl;
    ss << "tilt(x): " << xangl << std::endl;
    ss << "offset x: " << median_offset.x << std::endl;
    ss << "offset y: " << median_offset.y << std::endl;
    ss << "offset z: " << median_offset.z << std::endl;
    
    calibration_log = ss.str();

    return;

    //    std::cout << "valid positions " << vvalid << " / " << vnum << std::endl;
    //    // rendering orientations
    //    it = directions.begin();
    //    itend = directions.end();
    //    ofQuaternion median_q;
    //    ofVec3f ref( 0,1,0 );
    //    ofVec4f qvs;
    //    for (; it != itend; ++it, ++i) {
    //        if ( it->first == zero ) {
    //            continue;
    //        }
    //        ofQuaternion d;
    //        d.makeRotate( it->second, it->first );
    //        median_q.slerp( angl_div * 4, median_q, d );
    //    }
    //    ofMatrix4x4 calibration_mat;
    //    calibration_mat.makeRotationMatrix( median_q );
    //    ofMatrix4x4 calibration_mati = calibration_mat.getInverse();
    //    
    //    // back to positions
    //    it = vec3pairs.begin();
    //    itend = vec3pairs.end();
    //    ofMatrix4x4 work_mat;
    //    // rendering the average difference between rotated position and originals
    //    float lerp = 1.f / vnum;
    //    ofVec3f median_offset(0,0,0);
    //    for (; it != itend; ++it, ++i) {
    //        work_mat.makeTranslationMatrix( it->second );
    //        work_mat = calibration_mati * work_mat * calibration_mat;
    //        ofVec3f prtd = work_mat.getTranslation();
    //        median_offset += ( it->first - prtd ) * lerp;
    //        rotated.push_back( prtd );
    //    }
    //    
    //    std::cout << "median offset: " << 
    //            median_offset.x << ", " <<
    //            median_offset.y << ", " <<
    //            median_offset.z << std::endl;
    //    
    //    work_mat.makeTranslationMatrix( median_offset );
    //    calibration_mat *= work_mat;
    //    calibration_mati = calibration_mat.getInverse();
    //    it = vec3pairs.begin();
    //    for (; it != itend; ++it, ++i) {
    //        work_mat.makeTranslationMatrix( it->second );
    //        work_mat = work_mat * calibration_mat;
    //        calibrated.push_back( work_mat.getTranslation() );
    //    }

}

void ofApp::setup() {
}

void ofApp::update() {

}

void ofApp::axis(float size) const {

    glLineWidth(3);
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(size, 0, 0);
    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, size, 0);
    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, size);
    glEnd();
    glLineWidth(1);

}

void ofApp::grid(bool x, bool y, bool z) const {
    glColor4f(0.35, 0.35, 0.35, 1);
    glLineWidth(0.5);
    glBegin(GL_LINES);
    for (float u = -10; u <= 10; u += 0.1) {
        //        if (u == int(u)) continue;
        for (float v = -10; v <= 10; v += 0.1) {
            //            if (v == int(v)) continue;
            if (x) {
                //			glColor4f(1, 0, 0, 0.5);
                glVertex3f(0, v, -u);
                glVertex3f(0, v, u);
                glVertex3f(0, -u, v);
                glVertex3f(0, u, v);
            }
            if (y) {
                //			glColor4f(0, 1, 0, 0.5);
                glVertex3f(v, 0, -u);
                glVertex3f(v, 0, u);
                glVertex3f(-u, 0, v);
                glVertex3f(u, 0, v);
            }
            if (z) {
                //			glColor4f(0, 0, 1, 0.5);
                glVertex3f(v, -u, 0);
                glVertex3f(v, u, 0);
                glVertex3f(-u, v, 0);
                glVertex3f(u, v, 0);
            }
        }
    }
    glEnd();
    glColor4f(0.65, 0.65, 0.65, 1);
    glLineWidth(1);
    glBegin(GL_LINES);
    for (float u = -10; u <= 10; u += 1) {
        for (float v = -10; v <= 10; v += 1) {
            if (x) {
                //			glColor4f(1, 0, 0, 0.5);
                glVertex3f(0, v, -u);
                glVertex3f(0, v, u);
                glVertex3f(0, -u, v);
                glVertex3f(0, u, v);
            }
            if (y) {
                //			glColor4f(0, 1, 0, 0.5);
                glVertex3f(v, 0, -u);
                glVertex3f(v, 0, u);
                glVertex3f(-u, 0, v);
                glVertex3f(u, 0, v);
            }
            if (z) {
                //			glColor4f(0, 0, 1, 0.5);
                glVertex3f(v, -u, 0);
                glVertex3f(v, u, 0);
                glVertex3f(-u, v, 0);
                glVertex3f(u, v, 0);
            }
        }
    }
    glEnd();
}

void ofApp::draw() {

    ofSetupScreenPerspective(ofGetWidth(), ofGetHeight(), 30, 0, 10000);

    ofBackground(20, 20, 20);
    glEnable(GL_DEPTH_TEST);

    ofPushMatrix();
    ofTranslate(ofGetWidth()*0.5, ofGetHeight()*0.5, 0);
    //	ofScale(-1, 1, 1);
    ofVec3f sc = scale * ofGetHeight() * ofVec3f(-1, 1, 1);
    ofScale(sc);
    ofMultMatrix(matrix_rotor);
    ofTranslate(translate);
    grid(false, false, true);
    axis(0.5);

    ofPushMatrix();
    ofMultMatrix(calibration_matrix);
    axis(0.5);
    ofPopMatrix();

    Vec3PairListIter itp;
    Vec3PairListIter itprev;
    Vec3PairListIter itpe = vec3pairs.end();
    std::vector< ofVec3f >::iterator itr;
    std::vector< ofVec3f >::iterator itrprev;
    std::vector< ofVec3f >::iterator itre;

    glPointSize(1);
    glLineWidth(0.5);
    //    glBegin(GL_LINES);
    //    itp = vec3pairs.begin();
    //    for (; itp != itpe; ++itp) {
    //        ofSetColor(255, 255, 255, 100);
    //        glVertex3f(itp->first.x, itp->first.y, itp->first.z);
    //        ofSetColor(255, 190, 0, 100);
    //        glVertex3f(itp->second.x, itp->second.y, itp->second.z);
    //    }
    //    glEnd();

    ofSetColor(255, 255, 255);
    glPointSize(2);
    glBegin(GL_POINTS);
    itp = vec3pairs.begin();
    for (; itp != itpe; ++itp) {
        glVertex3f(itp->first.x, itp->first.y, itp->first.z);
    }
    glEnd();
    ofSetColor(255, 255, 255, 180);
    glLineWidth(1.5);
    glBegin(GL_LINES);
    itp = vec3pairs.begin();
    itprev = vec3pairs.begin();
    for (++itp; itp != itpe; ++itp, ++itprev) {
        glVertex3f(itprev->first.x, itprev->first.y, itprev->first.z);
        glVertex3f(itp->first.x, itp->first.y, itp->first.z);
    }
    glEnd();

    // ground projection
    ofSetColor(255, 255, 255, 90);
    glBegin(GL_LINES);
    itp = vec3pairs.begin();
    itprev = vec3pairs.begin();
    for (++itp; itp != itpe; ++itp, ++itprev) {
        glVertex3f(itprev->first.x, itprev->first.y, 0);
        glVertex3f(itp->first.x, itp->first.y, 0);
    }
    glEnd();
    // back projection
    glBegin(GL_LINES);
    itp = vec3pairs.begin();
    itprev = vec3pairs.begin();
    for (++itp; itp != itpe; ++itp, ++itprev) {
        glVertex3f(itprev->first.x, 4.5, itprev->first.z);
        glVertex3f(itp->first.x, 4.5, itp->first.z);
    }
    glEnd();
    // side projection
    glBegin(GL_LINES);
    itp = vec3pairs.begin();
    itprev = vec3pairs.begin();
    for (++itp; itp != itpe; ++itp, ++itprev) {
        glVertex3f(-1.5, itprev->first.y, itprev->first.z);
        glVertex3f(-1.5, itp->first.y, itp->first.z);
    }
    glEnd();

    ofSetColor(255, 190, 0, 180);
    glBegin(GL_LINES);
    itp = vec3pairs.begin();
    itprev = vec3pairs.begin();
    for (++itp; itp != itpe; ++itp, ++itprev) {
        glVertex3f(itprev->second.x, itprev->second.y, itprev->second.z);
        glVertex3f(itp->second.x, itp->second.y, itp->second.z);
    }
    glEnd();

    // ground projection
    ofSetColor(255, 190, 0, 90);
    glBegin(GL_LINES);
    itp = vec3pairs.begin();
    itprev = vec3pairs.begin();
    for (++itp; itp != itpe; ++itp, ++itprev) {
        glVertex3f(itprev->second.x, itprev->second.y, 0);
        glVertex3f(itp->second.x, itp->second.y, 0);
    }
    glEnd();
    // back projection
    glBegin(GL_LINES);
    itp = vec3pairs.begin();
    itprev = vec3pairs.begin();
    for (++itp; itp != itpe; ++itp, ++itprev) {
        glVertex3f(itprev->second.x, 4.5, itprev->second.z);
        glVertex3f(itp->second.x, 4.5, itp->second.z);
    }
    glEnd();
    // side projection
    glBegin(GL_LINES);
    itp = vec3pairs.begin();
    itprev = vec3pairs.begin();
    for (++itp; itp != itpe; ++itp, ++itprev) {
        glVertex3f(-1.5, itprev->second.y, itprev->second.z);
        glVertex3f(-1.5, itp->second.y, itp->second.z);
    }

    ofSetColor(255, 190, 0);
    glBegin(GL_POINTS);
    itp = vec3pairs.begin();
    for (; itp != itpe; ++itp) {
        glVertex3f(itp->second.x, itp->second.y, itp->second.z);
    }
    glEnd();

    //    ofSetColor(0, 255, 255);
    //    glBegin(GL_POINTS);
    //    itr = rotated.begin();
    //    itre = rotated.end();
    //    for (; itr != itre; ++itr) {
    //        glVertex3f((*itr).x, (*itr).y, (*itr).z);
    //    }
    //    glEnd();
    //    ofSetColor(0, 255, 255, 180);
    //    glBegin(GL_LINES);
    //    itr = rotated.begin();
    //    itrprev = rotated.begin();
    //    itre = rotated.end();
    //    for (++itr; itr != itre; ++itr, ++itrprev) {
    //        glVertex3f((*itrprev).x, (*itrprev).y, (*itrprev).z);
    //        glVertex3f((*itr).x, (*itr).y, (*itr).z);
    //    }
    //    glEnd();

    ofSetColor(0, 255, 255);
    glBegin(GL_POINTS);
    itr = calibrated.begin();
    itre = calibrated.end();
    for (; itr != itre; ++itr) {
        glVertex3f((*itr).x, (*itr).y, (*itr).z);
    }
    glEnd();
    ofSetColor(0, 255, 255, 180);
    glBegin(GL_LINES);
    itr = calibrated.begin();
    itrprev = calibrated.begin();
    itre = calibrated.end();
    for (++itr; itr != itre; ++itr, ++itrprev) {
        glVertex3f((*itrprev).x, (*itrprev).y, (*itrprev).z);
        glVertex3f((*itr).x, (*itr).y, (*itr).z);
    }
    glEnd();

    // ground projection
    ofSetColor(0, 255, 255, 90);
    glBegin(GL_LINES);
    itr = calibrated.begin();
    itrprev = calibrated.begin();
    itre = calibrated.end();
    for (++itr; itr != itre; ++itr, ++itrprev) {
        glVertex3f((*itrprev).x, (*itrprev).y, 0);
        glVertex3f((*itr).x, (*itr).y, 0);
    }
    glEnd();
    // back projection
    glBegin(GL_LINES);
    itr = calibrated.begin();
    itrprev = calibrated.begin();
    itre = calibrated.end();
    for (++itr; itr != itre; ++itr, ++itrprev) {
        glVertex3f((*itrprev).x, 4.5, (*itrprev).z);
        glVertex3f((*itr).x, 4.5, (*itr).z);
    }
    glEnd();
    // side projection
    glBegin(GL_LINES);
    itr = calibrated.begin();
    itrprev = calibrated.begin();
    itre = calibrated.end();
    for (++itr; itr != itre; ++itr, ++itrprev) {
        glVertex3f(-1.5, (*itrprev).y, (*itrprev).z);
        glVertex3f(-1.5, (*itr).y, (*itr).z);
    }

    if (!link_calibrated) {

        glLineWidth(0.5);
        glBegin(GL_LINES);
        itr = calibrated.begin();
        itp = vec3pairs.begin();
        for (; itr != itre; ++itr, ++itp) {
            ofSetColor(0, 255, 255, 180);
            glVertex3f((*itr).x, (*itr).y, (*itr).z);
            ofSetColor(255, 255, 255, 180);
            glVertex3f(itp->first.x, itp->first.y, itp->first.z);
        }
        glEnd();

        glBegin(GL_QUADS);
        itr = calibrated.begin();
        itrprev = calibrated.begin();
        itp = vec3pairs.begin();
        itprev = vec3pairs.begin();
        for (++itr, ++itp; itr != itre; ++itr, ++itp, ++itrprev, ++itprev) {
            ofSetColor(0, 255, 255, 100);
            glVertex3f((*itrprev).x, (*itrprev).y, (*itrprev).z);
            glVertex3f((*itr).x, (*itr).y, (*itr).z);
            ofSetColor(255, 255, 255, 100);
            glVertex3f(itp->first.x, itp->first.y, itp->first.z);
            glVertex3f(itprev->first.x, itprev->first.y, itprev->first.z);
        }
        glEnd();

    } else {

        glBegin(GL_LINES);
        itr = calibrated.begin();
        itp = vec3pairs.begin();
        for (; itr != itre; ++itr, ++itp) {
            ofSetColor(0, 255, 255, 180);
            glVertex3f((*itr).x, (*itr).y, (*itr).z);
            ofSetColor(255, 190, 0, 180);
            glVertex3f(itp->second.x, itp->second.y, itp->second.z);
        }
        glEnd();

    }


    ofPopMatrix();
    
    ofSetColor( 255,255,255 );
    ofDrawBitmapString( calibration_log, 30, 50 );

}

void ofApp::keyPressed(int key) {

    if (key == 'n') {
        file2load++;
        if (file2load >= files.size()) {
            file2load = 0;
        }
        process();
    } else if (key == 'l') {

        link_calibrated = !link_calibrated;

    }
}

void ofApp::keyReleased(int key) {

}

void ofApp::mouseMoved(int x, int y) {

}

void ofApp::mouseDragged(int x, int y, int button) {

    ofVec2f mp(x, y);
    ofVec2f diff = (mp - prev_mouse) / ofGetWidth();
    //    model_rotor *= ofQuaternion(-diff.y * 200, ofVec3f(1, 0, 0));
    //    model_rotor *= ofQuaternion(diff.x * 200, ofVec3f(0, 1, 0));
    if (button == 0) {
        scale += diff.y;
        if (scale.x < 0.0001) {
            scale.set(0.0001, 0.0001, 0.0001);
        }
    } else if (button == 1) {

        ofMatrix4x4 mr(model_rotor);
        ofMatrix4x4 mri = mr.getInverse();
        ofMatrix4x4 t;
        t.makeTranslationMatrix(ofVec3f(1, 0, 0));
        t = mr * t * mri;
        ofVec3f side = t.getTranslation();
        ofVec3f front = ofQuaternion(90, ofVec3f(0, 0, 1)) * side;
        translate +=
                -diff.x * 10 * side +
                diff.y * 10 * front;

    } else if (button == 2) {

        ofVec3f up = model_rotor * ofVec3f(0, 0, 1);
        ofVec3f right = ofVec3f(1, 0, 0);
        model_rotor *= ofQuaternion(diff.x * 200, up);
        model_rotor *= ofQuaternion(diff.y * 200, right);
        matrix_rotor.makeRotationMatrix(model_rotor);

    }
    prev_mouse = mp;

}

void ofApp::mousePressed(int x, int y, int button) {
    prev_mouse.set(x, y);
}

void ofApp::mouseReleased(int x, int y, int button) {

}

void ofApp::mouseEntered(int x, int y) {

}

void ofApp::mouseExited(int x, int y) {

}

void ofApp::windowResized(int w, int h) {

}

void ofApp::gotMessage(ofMessage msg) {

}

void ofApp::dragEvent(ofDragInfo dragInfo) {

}