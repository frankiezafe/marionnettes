#pragma once

#include "ofMain.h"
#include "raw.h"

typedef std::vector< std::pair< ofVec3f, ofVec3f > > Vec3PairList;
typedef std::vector< std::pair< ofVec3f, ofVec3f > >::iterator Vec3PairListIter;

class ofApp : public ofBaseApp {
public:
    
    ofApp();
    
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

private:
    
    void axis(float size) const;
    void grid(bool x, bool y, bool z) const;
    
    ofVec2f prev_mouse;
    ofQuaternion model_rotor;
    ofMatrix4x4 matrix_rotor;
    ofVec3f translate;
    ofVec3f scale;
    
    frame_content raw_content;
    raw raw_reader;
    
    Vec3PairList vec3pairs;
    std::vector< ofVec3f > rotated;
    std::vector< ofVec3f > rotated_dir;
    std::vector< ofVec3f > calibrated;
    ofMatrix4x4 calibration_matrix;
    std::string calibration_log;
    
    std::vector< std::string > files;
    size_t file2load;
    
    void process();
    
    bool link_calibrated;
    
};
