#pragma once

#include "ofKinectBase.h"

#define OSC_MATRIX_REQUEST "/m?"
#define OSC_MATRIX_UPLOAD "/m!"
#define OSC_MATRIX_UPDATE "/m>"

#define OSC_FILTER_REQUEST "/f?"
#define OSC_FILTER_UPLOAD "/f!"
#define OSC_FILTER_UPDATE "/f>"

#define OSC_SKELETON_FRAME "/skf"
#define OSC_SKELETON_POSITIONS "/skp"
#define KINECT_CONTROL_PORT 33333

#define AVATAR_MODEL "avatar.model"

/* Access to internal static values and arrays. No safeguard => segmentation fault if you
   request an undefined key.
 */
class SKstatic {
public:

    static const uint8_t& body_bones_count();
    static const uint8_t& arm_bone_count();
    static const uint8_t& leg_bone_count();
    static const uint8_t& body_bones(const uint8_t& i);
    static const uint8_t& arm_left_bones(const uint8_t& i);
    static const uint8_t& arm_right_bones(const uint8_t& i);
    static const uint8_t& leg_left_bones(const uint8_t& i);
    static const uint8_t& leg_right_bones(const uint8_t& i);

    static const uint8_t& bones_parents_count() {
        return NUI_SKELETON_POSITION_COUNT;
    }
    static const uint8_t& bones_parents(const uint8_t& i);

    static const uint8_t& bone_key_count() {
        return NUI_SKELETON_POSITION_COUNT + 1;
    }
    
    static const std::string& bone_key(const uint8_t& i);

};

/* Class containing all basic parameters and methods for skeleton
manipulations.  Used for kinect skeletons processing.
 */
class SkeletonBase {
public:

    /* Alignment matrices for positions ( absolute_positions[] )
     */
    ofMatrix4x4 rot_mat;
    ofMatrix4x4 rot_mati;

    /* Alignment matrices for orientations ( absolute_rotations[] )
     */
    ofMatrix4x4 pos_mat;
    ofMatrix4x4 pos_mati;

    ofVec3f origin;

    /* 3D position of bones in world space, relative to hip_center and 
       after alignement (pos_mat) if loaded via object's method.
     */
    ofVec3f absolute_positions[NUI_SKELETON_POSITION_COUNT];

    /* 3D position of bones in parent space, render on basis of absolute_positions
    automatically processed if values are loaded via object's method.
     */
    ofVec3f relative_positions[NUI_SKELETON_POSITION_COUNT];

    /* World orientation of bones, after alignement (rot_mat) if loaded via
    object's method.
     */
    ofQuaternion absolute_rotations[NUI_SKELETON_POSITION_COUNT];

    /*Standard constructor*/
    SkeletonBase();

    /* Check if rot_mat and pos_mat are different from identity.
       If yes, it sets pos_mat_enabled and/or rot_mat_enabled to true.
     */
    virtual void check_alignment();

    /* Set origin of the skeleton and align it (pos_mat) 
     */
    virtual void set(const ofVec3f& p);

    /* Set each absolute_positions and absolute_rotations
    manually, used at kinect sekeleton retrieval.
    @remark
    no need to render the skeleton in this case.
     */
    virtual void set(uint8_t i, const ofVec3f& p, const ofQuaternion& q);

    /* Simple OpenGL calls to draw skeleton with octahedrons.
     */
    virtual void draw() const;

    /* Simple OpenGL calls to draw skeleton as lines.
     */
    virtual void lines() const;

    /* Get parent id for the specified bone ID
    @param
    one of the NUI_SKELETON_POSITION
    @return
    NUI_SKELETON_POSITION_COUNT if specified bone ID isinvalid or has
    no parent.
     */
    uint8_t parent(uint8_t id) const;

    /* Draw helper, draws a 3d axisin the current model matrix.
     */
    static void axis(float size);

    /* Draw helper, render an octahedron for the specified bone.
    Requires a parent.
     */
    virtual void octa(uint8_t i, float thickness) const;

protected:

    /* Draw helper, calls glVertex3f for the specified absolute_positions
     */
    virtual void vertex(uint8_t i) const;

    /* Draw helper, calls glVertex3f for the specified absolute_positions
    and the specified absolute_positions + relative_positions
     */
    virtual void line(uint8_t i, const ofVec3f& offset) const;

    bool pos_mat_enabled;

    bool rot_mat_enabled;


};

/* Class specialised in external skeleton loading. Extends SkeletonBase.
Filename is defined at the top of this file (AVATAR_MODEL).
 */
class SkeletonModel : public SkeletonBase {
public:

    /* Matrix used to align kinect skeletons */
    ofMatrix4x4 origin_mat;
	ofMatrix4x4 origin_mati;

    /* World matrices per bone */
    ofMatrix4x4 absolute_matrices[NUI_SKELETON_POSITION_COUNT];

    /* Inverse of world matrices per bone */
    ofMatrix4x4 absolute_matricesi[NUI_SKELETON_POSITION_COUNT];

    /* Inverse of matrices difference between bones and their parent,
    used for output processing in SkeletonDelta
     */
    ofMatrix4x4 delta_matricesi[NUI_SKELETON_POSITION_COUNT];

    /* Loads the file 'data/avatar.model' from disk.
    @return
    false is loading failed
    @important
    make sure this method returns true if you want the behaviour to
    be correct
     */
    bool loadModel();

};

/* Class specialised in comparaison between SkeletonBase and SkeletonModel.
Extends SkeletonBase.
 */
class SkeletonDelta : public SkeletonBase {
public:

    /* This matrix contains the world position and orientation 
       of the kinect. It is applied on the output_rotations and 
       output_positions only
     */
    ofMatrix4x4 calibration_mat;
    ofMatrix4x4 calibration_mati;

    /* Used to store inverse of absolute matrices, and therefore
    optimise computation of relative orientations (parent space).
    Computed by diff() method.
     */
    ofMatrix4x4 absolute_matrices[NUI_SKELETON_POSITION_COUNT];
    ofMatrix4x4 absolute_matricesi[NUI_SKELETON_POSITION_COUNT];

    /* Contains relative rotations for internal displayand debugging.
    Computed by diff() method.
     */
    ofQuaternion relative_rotations[NUI_SKELETON_POSITION_COUNT];

    /* Contains local delta orientations ready to be sent to game engine.
    Computed by diff() method.
     */
    ofQuaternion output_rotations[NUI_SKELETON_POSITION_COUNT];

    /* Contains absolute positions after calibration.
     */
    ofVec3f output_positions[NUI_SKELETON_POSITION_COUNT];

    /* Renders and stores the difference between 2 SkeletonModel.
    @param
    SkeletonBase extract from kinect (typically)
    @param
    SkeletonModel, containing model and all matrices
     */
    void diff(const SkeletonBase& newsk, const SkeletonModel& model);

    /* Draw helper, shows relative orientations computed on each bone.
     */
    void relatives();

    /* Flag set to true if skeleton has been updated at last kinect frame.
     */
    bool updated;

    /* Standard constructor, set updated to false.
     */
    SkeletonDelta();

};

/* Class linking the output of SkeletonDelta to its OSC representation.
The name of each MappingKey is used to identify the bone in the message.
Pairs contains the references to SkeletonDelta bones' orientations to be sent
for this name.
 */
class MappingKey {
public:

    /* Standard constructor, set count to 0.
     */
    MappingKey() :
    count(0) {
    }

    /* Fast constructor, sets name and number of values.
     */
    MappingKey(std::string n, size_t num = 0);

    ~MappingKey() {
    }

    /* Name getter.
    @return
    a const reference to the name of this instance
     */
    inline const std::string& name() const {
        return _name;
    }

    /* Size getter.
    @return
    a const reference to the number of values of this instance
     */
    inline const size_t& size() const {
        return count;
    }

    /* Value getter
    @return
    a pair representing the ID of the bone to use and its waight
     */
    inline std::pair< uint8_t, float >& operator[](size_t i) {
        return pairs[i];
    }

    /* Add a value to the instance.
    @param
    position in the values list
    @param
    ID of the bone to use, see NUI_SKELETON_POSITION_INDEX
    @param
    weight of the bone in the output, 1 by default
    @remarks
    only accessible if the instance is declared using the fast
    constructor, MappingKey(std::string n, size_t num = 0)
     */
    void add(
            size_t indice,
            uint8_t bone_id,
            float weight = 1.0);

    /* Util to get all the values of the instance as a string,
    used with std::cout.
     */
    std::string print() const;

private:

    /* Name of the instance, only set via the fast
    constructor, MappingKey(std::string n, size_t num = 0)
     */
    std::string _name;

    /* Number of values in the instance, only set via the fast
    constructor, MappingKey(std::string n, size_t num = 0)
     */
    size_t count;

    /* Values of the instance, list of pairs representing:
    first: ID of the bone to use, see NUI_SKELETON_POSITION_INDEX
    second: weight of the bone in the output
     */
    vector< std::pair< uint8_t, float > > pairs;

};