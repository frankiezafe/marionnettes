/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   raw.h
 * Author: frankiezafe
 *
 * Created on November 17, 2017, 5:56 PM
 */

#ifndef RAW_H
#define RAW_H

#include <chrono>
#include <sys/time.h>
#include <iostream>
#include <ostream>
#include <fstream>
#include <iosfwd>
#include <stdio.h>
#include <cstring>
#include <array>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define STRING_MAX_SIZE 255

template<typename T>
union data {
    char c[sizeof(T)];
    T val;
};

enum frame_flag {
    FRAME_UNDEFINED =   0,
    FRAME_FIRST =       1,
    FRAME_INBETWEEN =   2,
    FRAME_LAST =        3
};

struct frame_info {
    uint32_t UID;
    uint64_t timestamp;
    uint32_t duration;
    uint32_t time;
    uint32_t bytes;
    std::streamoff previousg;
    std::streamoff currentg;
    std::streamoff nextg;
    frame_flag flag;
};


class frame_configuration {
public:
    uint16_t object_num;
    uint16_t player_num;
    uint16_t vec2_num;
    uint16_t vec3_num;
    frame_configuration():
    object_num(0),
    player_num(0),
    vec2_num(0),
    vec3_num(0){}
};

class frame_content {
public:
    std::vector< std::array<float, 2> > vec2s;
    std::vector< std::array<float, 3> > vec3s;
    void clear() {
        vec2s.clear();
        vec3s.clear();
    }
};

// value of leading char + num of bits after
// 1: timestamp [64] + previous frame bit offset [64] + next frame bit offset [64]
// 2: object name [str, 255 bytes] + RTS [float * 9]
// 4: player [str] + RT (pov) [float * 6] + RT (action) [float * 6]

class raw {
public:
    
    static uint64_t now();

    raw();
    virtual ~raw();

    bool write_open(std::string filepath);
    bool write_start_frame( frame_configuration* fc = 0 );
    bool write_vec2( float x, float y );
    bool write_vec3( float x, float y, float z );
    void write_flush();
    void write_close();
    
    /* Disable automatic flush at each write.
     * Autoflush is enabled by default.
     * @warning
     *      call write_flush regularly if you disable this
     */
    void write_autoflush( bool enabled );
    
    bool read_open(std::string filepath);
    void read_close();
    
    bool read_frame_info( size_t i, frame_info& dst );
    
    bool read_frame( size_t i, frame_content& dst );
    
    inline size_t frame_num() const {
        return read_frame_num;
    }

private:
    
    uint32_t write_previous_index;
    uint32_t write_current_index;
    uint32_t write_frame_bytes;
    uint32_t write_written_bytes;
    uint32_t write_next_index;
    std::string write_path;
    std::ofstream* write_file;
    bool write_autoflush_enabled;
    
    void operator << ( const char& uc );
    void operator << ( const std::string& s );
    void operator << ( const float& f );
    void operator << ( const uint32_t& ui32 );
    void operator << ( const uint64_t& ui64 );
    
    void frame_consistency();

    uint32_t read_file_size;
    data< uint64_t > read_frame_timestamp;
    data< std::streamoff > read_previous_index;
    data< std::streamoff > read_next_index;
    std::string read_path;
    std::ifstream* read_file;
    
    bool retrieve_frame_info();
    size_t read_frame_num;
    frame_info* read_frame_infos;

};

#endif /* RAW_H */

