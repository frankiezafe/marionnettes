/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   raw.cpp
 * Author: frankiezafe
 * 
 * Created on November 17, 2017, 5:56 PM
 */

#include <vector>

#include "raw.h"

using namespace std;

//////////////////////////////////////
////////////// STATICS ///////////////
//////////////////////////////////////

static char UNDEFINED_STAMP(0);
static char FRAME_STAMP(1);
static char OBJECT_STAMP(2);
static char PLAYER_STAMP(3);
static char VEC2_STAMP(4);
static char VEC3_STAMP(5);

static uint16_t FRAME_STAMP_SIZE = 1 + sizeof (uint64_t) + sizeof (uint32_t) * 2;
static uint16_t OBJECT_SIZE = (1 + STRING_MAX_SIZE + sizeof (float) * 9);
static uint16_t PLAYER_SIZE = (1 + STRING_MAX_SIZE + sizeof (float) * 12);
static uint16_t VEC2_SIZE = (1 + sizeof (float) * 2);
static uint16_t VEC3_SIZE = (1 + sizeof (float) * 3);

uint64_t raw::now() {
    return (uint64_t) chrono::duration_cast< chrono::microseconds >(
            chrono::system_clock::now().time_since_epoch()
            ).count();
}

raw::raw() :
write_file(0),
write_autoflush_enabled(true),
read_file(0),
read_frame_num(0),
read_frame_infos(0) {
}

raw::~raw() {

    write_close();
    read_close();

}

//////////////////////////////////////
//////////////// WRITE ///////////////
//////////////////////////////////////

bool raw::write_open(std::string filepath) {

    write_close();
    write_path = filepath;
    write_file = new std::ofstream();
    write_file->open(write_path, ios::binary | ios::out);
    if (!write_file->is_open()) {
        delete write_file;
        write_file = 0;
        write_path.clear();
        return false;
    }
    write_previous_index = 0;
    write_current_index = 0;
    write_frame_bytes = 0;
    write_written_bytes = 0;
    write_next_index = 0;
    return true;

}

void raw::frame_consistency() {

    while (write_written_bytes != write_frame_bytes) {
        (*this) << UNDEFINED_STAMP;
        ++write_written_bytes;
    }
    write_file->flush();

}

bool raw::write_start_frame(frame_configuration* fcontent) {

    if (!write_file) {
        return false;
    }

    //    std::cout << std::endl << "start_frame" << std::endl;

    frame_consistency();

    write_frame_bytes = FRAME_STAMP_SIZE;
    if (fcontent) {
        write_frame_bytes += OBJECT_SIZE * fcontent->object_num;
        write_frame_bytes += PLAYER_SIZE * fcontent->player_num;
        write_frame_bytes += VEC2_SIZE * fcontent->vec2_num;
        write_frame_bytes += VEC3_SIZE * fcontent->vec3_num;
    }

    uint64_t timestamp = now();

    (*this) << FRAME_STAMP;

    // timestamp
    (*this) << timestamp;

    // previous frame
    (*this) << write_previous_index;

    write_previous_index = write_current_index;
    write_current_index = write_next_index;
    write_next_index += write_frame_bytes;

    // next frame
    (*this) << write_next_index;

    // good to go
    if ( write_autoflush_enabled ) { 
        write_file->flush();
    }

    write_written_bytes = FRAME_STAMP_SIZE;

    // debug info
    //    std::cout <<
    //            "timestamp: " << timestamp <<
    //            ", previous: " << write_previous_index <<
    //            ", current: " << write_current_index <<
    //            ", next: " << write_next_index <<
    //            ", frame bytes: " << write_frame_bytes <<
    //            ", written bytes: " << write_written_bytes <<
    //            std::endl;
    //    struct stat filestatus;
    //    stat(write_path.c_str(), &filestatus);
    //    cout << filestatus.st_size << " bytes\n";

    return true;

}

bool raw::write_vec2(float x, float y) {

    if (!write_file) {
        return false;
    }
    if (write_frame_bytes - write_written_bytes < VEC2_SIZE) {
        std::cout << "not enough space left in the frame!" << std::endl;
        return false;
    }

    (*this) << VEC2_STAMP;
    (*this) << x;
    (*this) << y;

    write_written_bytes += VEC2_SIZE;

    // good to go
    if ( write_autoflush_enabled ) { 
        write_file->flush();
    }

}

bool raw::write_vec3(float x, float y, float z) {

    if (!write_file) {
        return false;
    }
    if (write_frame_bytes - write_written_bytes < VEC3_SIZE) {
        std::cout << "not enough space left in the frame!" << std::endl;
        return false;
    }

    (*this) << VEC3_STAMP;
    (*this) << x;
    (*this) << y;
    (*this) << z;

    write_written_bytes += VEC3_SIZE;

    // good to go
    if ( write_autoflush_enabled ) { 
        write_file->flush();
    }

}

void raw::operator<<(const char& uc) {
    write_file->write((char*) &uc, sizeof (char));
}

void raw::operator<<(const std::string& s) {
    write_file->write(s.c_str(), sizeof (char) * s.size());
}

void raw::operator<<(const float& f) {
    write_file->write((char*) &f, sizeof (float));
}

void raw::operator<<(const uint32_t& ui32) {
    write_file->write((char*) &ui32, sizeof (uint32_t));
}

void raw::operator<<(const uint64_t& ui64) {
    write_file->write((char*) &ui64, sizeof (uint64_t));
}

void raw::write_flush() {
    if (!write_file) {
        return;
    }
    write_file->flush();
}

void raw::write_close() {

    if (write_file) {
        frame_consistency();
        write_file->flush();
        write_file->close();
        delete write_file;
        write_file = 0;
        write_path.clear();
    }

}

void raw::write_autoflush(bool enabled) {

    write_autoflush_enabled = enabled;

}

//////////////////////////////////////
//////////////// READ ////////////////
//////////////////////////////////////

bool raw::read_open(std::string filepath) {

    read_close();
    read_path = filepath;
    read_file = new std::ifstream();
    read_file->open(read_path, ios::binary | ios::in);
    if (!read_file->is_open()) {
        delete read_file;
        read_file = 0;
        read_path.clear();
        return false;
    }

    read_file->seekg(0, read_file->end);
    read_file_size = read_file->tellg();
    read_file->seekg(0, read_file->beg);
    //    cout << read_file_size << " bytes\n";

    // initialising stream offsets
    std::size_t streamoff_size(sizeof (streamoff));
    for (size_t i = 0; i < streamoff_size; ++i) {
        read_previous_index.c[i] = 0;
        read_next_index.c[i] = 0;
    }

    if (!retrieve_frame_info()) {
        std::cout << read_path << " is corrupted" << std::endl;
        read_close();
        return false;
    }

    return true;

}

bool raw::retrieve_frame_info() {

    //    std::cout << "frame retrieval: " << read_path << std::endl;

    read_file->seekg(0, ios::beg);

    char stamp[FRAME_STAMP_SIZE];

    std::vector< frame_info > frames_tmp;

    uint64_t timegap = 0;
    uint64_t previous_timestamp = 0;

    while (read_next_index.val < read_file_size) {

        std::streampos currentpos = read_file->tellg();

        // get char at read_current_index
        read_file->read((char*) &stamp, FRAME_STAMP_SIZE);
        if (stamp[0] != FRAME_STAMP) {
            std::cout << int(stamp[0]) << " is not a FRAME_STAMP" << std::endl;
            return false;
        }

        // getting timestamp and frame indices
        std::size_t offset = 1;

        memcpy(read_frame_timestamp.c, stamp + offset, sizeof (uint64_t));
        offset += sizeof (uint64_t);

        memcpy(read_previous_index.c, stamp + offset, sizeof (uint32_t));
        offset += sizeof (uint32_t);

        memcpy(read_next_index.c, stamp + offset, sizeof (uint32_t));
        offset += sizeof (uint32_t);

        // moving the reading head upfront
        read_file->seekg(read_next_index.val, ios::beg);

        timegap = read_frame_timestamp.val - previous_timestamp;

        frame_info f;
        f.UID = frames_tmp.size();
        f.timestamp = read_frame_timestamp.val;
        f.previousg = read_previous_index.val;
        f.currentg = currentpos;
        f.nextg = read_next_index.val;
        f.flag = FRAME_INBETWEEN;
        f.duration = 0;
        f.bytes = f.nextg - currentpos;
        if (f.UID == 0) {
            f.previousg = -1;
            f.flag = FRAME_FIRST;
        } else {
            frames_tmp[ frames_tmp.size() - 1 ].duration = timegap;
            f.time = frames_tmp[ frames_tmp.size() - 1 ].time + timegap;
            if (f.nextg >= read_file_size) {
                f.nextg = -1;
                f.flag = FRAME_LAST;
                f.bytes = read_file_size - currentpos;
            }
        }
        frames_tmp.push_back(f);

        previous_timestamp = read_frame_timestamp.val;

    }

    if (frames_tmp.empty()) {
        return false;
    }

    read_frame_num = frames_tmp.size();
    read_frame_infos = new frame_info[ read_frame_num ];
    std::copy(frames_tmp.begin(), frames_tmp.end(), read_frame_infos);

    //    for (size_t i = 0; i < read_frame_num; ++i) {
    //        frame_info& f = read_frame_infos[i];
    //        std::cout <<
    //                "frame UID: " << f.UID << std::endl <<
    //                "\ttimestamp: " << f.timestamp << std::endl <<
    //                "\tduration: " << f.duration << std::endl <<
    //                "\ttime: " << f.time << std::endl <<
    //                "\tbytes: " << f.bytes << std::endl <<
    //                "\tprevious: " << f.previousg << std::endl <<
    //                "\tcurrent: " << f.currentg << std::endl <<
    //                "\tnext: " << f.nextg << std::endl <<
    //                "\tflag: " << int( f.flag) << std::endl;
    //    }

    return true;

}

bool raw::read_frame_info(size_t i, frame_info& dst) {

    if (!read_file || i >= read_frame_num) {
        return false;
    }
    memcpy(&(dst), &read_frame_infos[i], sizeof (struct frame_info));
    return true;

}

bool raw::read_frame(size_t i, frame_content& dst) {

    if (!read_file || i >= read_frame_num) {
        return false;
    }

    // uncompressing frame
    frame_info& fi = read_frame_infos[i];
    //    std::cout <<
    //            "frame UID: " << fi.UID << std::endl <<
    //            "\ttimestamp: " << fi.timestamp << std::endl <<
    //            "\tduration: " << fi.duration << std::endl <<
    //            "\ttime: " << fi.time << std::endl <<
    //            "\tprevious: " << fi.previousg << std::endl <<
    //            "\tcurrent: " << fi.currentg << std::endl <<
    //            "\tnext: " << fi.nextg << std::endl <<
    //            "\tflag: " << int( fi.flag) << std::endl;

    read_file->seekg(fi.currentg + FRAME_STAMP_SIZE, ios::beg);

    uint32_t buffer_size = fi.bytes - FRAME_STAMP_SIZE;
    uint32_t current = 0;

    char buffer[buffer_size];
    read_file->read((char*) &buffer, buffer_size);

    data< float > data_float;

    while (current != buffer_size) {

        if (buffer[current] == OBJECT_STAMP) {

            std::cout << int(buffer[current]) << " is an object" << std::endl;
            current += OBJECT_SIZE;

        } else if (buffer[current] == PLAYER_STAMP) {

            std::cout << int(buffer[current]) << " is a player" << std::endl;
            current += PLAYER_SIZE;

        } else if (buffer[current] == VEC2_STAMP) {

            //std::cout << int(buffer[current]) << " is a vec2" << std::endl;

            std::array<float, 2> vec2;

            memcpy(&data_float, &buffer[current + 1], sizeof (float));
            vec2[0] = data_float.val;

            memcpy(&data_float, &buffer[current + 5], sizeof (float));
            vec2[1] = data_float.val;

            dst.vec2s.push_back(vec2);

            current += VEC2_SIZE;

        } else if (buffer[current] == VEC3_STAMP) {

            //std::cout << int(buffer[current]) << " is a vec3" << std::endl;

            std::array<float, 3> vec3;
            for( uint8_t i = 0; i <  3; ++i ) {
                memcpy(&data_float, &buffer[current + 1 + i*sizeof (float)], sizeof (float));
                vec3[i] = data_float.val;
            }
            dst.vec3s.push_back(vec3);
            current += VEC3_SIZE;

        } else {

            std::cout << read_path << " is corrupted" << std::endl;
            return false;

        }

    }

    return true;

}

void raw::read_close() {

    if (read_file) {
        read_file->close();
        delete read_file;
        read_file = 0;
        read_path.clear();
    }
    if (read_frame_infos) {
        delete[] read_frame_infos;
        read_frame_infos = 0;
        read_frame_num = 0;
    }

}