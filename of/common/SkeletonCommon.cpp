#include "SkeletonCommon.h"

//////////////////////////////
// ********* STATICS *********
//////////////////////////////

static uint8_t _body_bones_count = 4;
static uint8_t _arm_bone_count = 5;
static uint8_t _leg_bone_count = 5;
static uint8_t _body_bones[4] = {
	NUI_SKELETON_POSITION_HIP_CENTER,
	NUI_SKELETON_POSITION_SPINE,
	NUI_SKELETON_POSITION_SHOULDER_CENTER,
	NUI_SKELETON_POSITION_HEAD
};
static uint8_t _arm_left_bones[5] = {
	NUI_SKELETON_POSITION_SHOULDER_CENTER,
	NUI_SKELETON_POSITION_SHOULDER_LEFT,
	NUI_SKELETON_POSITION_ELBOW_LEFT,
	NUI_SKELETON_POSITION_WRIST_LEFT,
	NUI_SKELETON_POSITION_HAND_LEFT
};
static uint8_t _arm_right_bones[5] = {
	NUI_SKELETON_POSITION_SHOULDER_CENTER,
	NUI_SKELETON_POSITION_SHOULDER_RIGHT,
	NUI_SKELETON_POSITION_ELBOW_RIGHT,
	NUI_SKELETON_POSITION_WRIST_RIGHT,
	NUI_SKELETON_POSITION_HAND_RIGHT
};
static uint8_t _leg_left_bones[5] = {
	NUI_SKELETON_POSITION_HIP_CENTER,
	NUI_SKELETON_POSITION_HIP_LEFT,
	NUI_SKELETON_POSITION_KNEE_LEFT,
	NUI_SKELETON_POSITION_ANKLE_LEFT,
	NUI_SKELETON_POSITION_FOOT_LEFT
};
static uint8_t _leg_right_bones[5] = {
	NUI_SKELETON_POSITION_HIP_CENTER,
	NUI_SKELETON_POSITION_HIP_RIGHT,
	NUI_SKELETON_POSITION_KNEE_RIGHT,
	NUI_SKELETON_POSITION_ANKLE_RIGHT,
	NUI_SKELETON_POSITION_FOOT_RIGHT
};
// shortcuts to bone's parent, if NUI_SKELETON_POSITION_COUNT > no parent
// just use "bones_parents[id]" to get parent
static uint8_t _bones_parents[NUI_SKELETON_POSITION_COUNT] = {
	NUI_SKELETON_POSITION_COUNT,				// NUI_SKELETON_POSITION_HIP_CENTER
	NUI_SKELETON_POSITION_HIP_CENTER,			// NUI_SKELETON_POSITION_SPINE
	NUI_SKELETON_POSITION_SPINE,				// NUI_SKELETON_POSITION_SHOULDER_CENTER
	NUI_SKELETON_POSITION_SHOULDER_CENTER,		// NUI_SKELETON_POSITION_HEAD
	NUI_SKELETON_POSITION_SHOULDER_CENTER,		// NUI_SKELETON_POSITION_SHOULDER_LEFT
	NUI_SKELETON_POSITION_SHOULDER_LEFT,		// NUI_SKELETON_POSITION_ELBOW_LEFT
	NUI_SKELETON_POSITION_ELBOW_LEFT,			// NUI_SKELETON_POSITION_WRIST_LEFT
	NUI_SKELETON_POSITION_WRIST_LEFT,			// NUI_SKELETON_POSITION_HAND_LEFT
	NUI_SKELETON_POSITION_SHOULDER_CENTER,		// NUI_SKELETON_POSITION_SHOULDER_RIGHT
	NUI_SKELETON_POSITION_SHOULDER_RIGHT,		// NUI_SKELETON_POSITION_ELBOW_RIGHT
	NUI_SKELETON_POSITION_ELBOW_RIGHT,			// NUI_SKELETON_POSITION_WRIST_RIGHT
	NUI_SKELETON_POSITION_WRIST_RIGHT,			// NUI_SKELETON_POSITION_HAND_RIGHT
	NUI_SKELETON_POSITION_HIP_CENTER,			// NUI_SKELETON_POSITION_HIP_LEFT
	NUI_SKELETON_POSITION_HIP_LEFT,				// NUI_SKELETON_POSITION_KNEE_LEFT
	NUI_SKELETON_POSITION_KNEE_LEFT,			// NUI_SKELETON_POSITION_ANKLE_LEFT
	NUI_SKELETON_POSITION_ANKLE_LEFT,			// NUI_SKELETON_POSITION_FOOT_LEFT
	NUI_SKELETON_POSITION_HIP_CENTER,			// NUI_SKELETON_POSITION_HIP_RIGHT
	NUI_SKELETON_POSITION_HIP_RIGHT,			// NUI_SKELETON_POSITION_KNEE_RIGHT
	NUI_SKELETON_POSITION_KNEE_RIGHT,			// NUI_SKELETON_POSITION_ANKLE_RIGHT
	NUI_SKELETON_POSITION_ANKLE_RIGHT,			// NUI_SKELETON_POSITION_FOOT_RIGHT
};
// recognized keys for model and mapping
static std::string _bone_key[NUI_SKELETON_POSITION_COUNT + 1] = {
	"HIP_CENTER",
	"SPINE",
	"SHOULDER_CENTER",
	"HEAD",
	"SHOULDER_LEFT",
	"ELBOW_LEFT",
	"WRIST_LEFT",
	"HAND_LEFT",
	"SHOULDER_RIGHT",
	"ELBOW_RIGHT",
	"WRIST_RIGHT",
	"HAND_RIGHT",
	"HIP_LEFT",
	"KNEE_LEFT",
	"ANKLE_LEFT",
	"FOOT_LEFT",
	"HIP_RIGHT",
	"KNEE_RIGHT",
	"ANKLE_RIGHT",
	"FOOT_RIGHT",
	"UNDEFINED"
};

const uint8_t& SKstatic::body_bones_count() {
	return _body_bones_count;
}
const uint8_t& SKstatic::arm_bone_count() {
	return _arm_bone_count;
}
const uint8_t& SKstatic::leg_bone_count() {
	return _leg_bone_count;
}
const uint8_t& SKstatic::body_bones(const uint8_t& i) {
	return _body_bones[i];
}
const uint8_t& SKstatic::arm_left_bones(const uint8_t& i) {
	return _arm_left_bones[i];
}
const uint8_t& SKstatic::arm_right_bones(const uint8_t& i) {
	return _arm_right_bones[i];
}
const uint8_t& SKstatic::leg_left_bones(const uint8_t& i) {
	return _leg_left_bones[i];
}
const uint8_t& SKstatic::leg_right_bones(const uint8_t& i) {
	return _leg_right_bones[i];
}
const uint8_t& SKstatic::bones_parents(const uint8_t& i) {
	return _bones_parents[i];
}
const std::string& SKstatic::bone_key(const uint8_t& i) {
	return _bone_key[i];
}

////////////////////////////////////
// ********* SKELETON BASE *********
////////////////////////////////////

SkeletonBase::SkeletonBase():
pos_mat_enabled(false),
rot_mat_enabled(false)
{}

void SkeletonBase::check_alignment() {
	pos_mat_enabled = !pos_mat.isIdentity();
	rot_mat_enabled = !rot_mat.isIdentity();
}

void SkeletonBase::set(const ofVec3f& p) {
	if (!pos_mat_enabled) {
		origin = p;
		return;
	}
	ofMatrix4x4 m;
	m.makeTranslationMatrix(p);
	m = pos_mati * m * pos_mat;
	origin = m.getTranslation();
}

void SkeletonBase::set(uint8_t i, const ofVec3f& p, const ofQuaternion& q) {
	if (i > NUI_SKELETON_POSITION_COUNT) {
		return;
	}
	if (!pos_mat_enabled) {
		absolute_positions[i] = p;
	} else {
		ofMatrix4x4 m;
		m.makeTranslationMatrix(p);
		m = pos_mati * m * pos_mat;
		absolute_positions[i] = m.getTranslation();
	}
	if (!rot_mat_enabled) {
		absolute_rotations[i] = q;
	}
	else {
		ofMatrix4x4 m;
		m.makeRotationMatrix(q);
		m = rot_mati * m * rot_mat;
		absolute_rotations[i] = m.getRotate();
	}
	uint8_t parentid = parent(i);
	if (parentid != NUI_SKELETON_POSITION_COUNT) {
		relative_positions[i] = absolute_positions[i] - absolute_positions[parentid];
	}
	else {
		relative_positions[i] = absolute_positions[i];
	}
}

void SkeletonBase::vertex(uint8_t i) const {
	glVertex3f(
		absolute_positions[i].x,
		absolute_positions[i].y,
		absolute_positions[i].z
	);
}

void SkeletonBase::line(uint8_t i, const ofVec3f& offset) const {
	uint8_t parent = _bones_parents[i];
	if (parent == NUI_SKELETON_POSITION_COUNT) {
		return;
	}
	glVertex3f(
		offset.x + absolute_positions[parent].x,
		offset.y + absolute_positions[parent].y,
		offset.z + absolute_positions[parent].z
	);
	glVertex3f(
		offset.x + absolute_positions[parent].x + relative_positions[i].x,
		offset.y + absolute_positions[parent].y + relative_positions[i].y,
		offset.z + absolute_positions[parent].z + relative_positions[i].z
	);
}

void SkeletonBase::octa(uint8_t i, float thickness) const {

	uint8_t parentid = parent(i);
	if (parentid == NUI_SKELETON_POSITION_COUNT) {
		return;
	}

	glPushMatrix();

	glTranslatef(
		absolute_positions[i].x,
		absolute_positions[i].y,
		absolute_positions[i].z
	);

	ofMatrix4x4 m(absolute_rotations[i]);
	glMultMatrixf(m.getPtr());

	ofVec3f start(0, -relative_positions[i].length(), 0);
	ofVec3f mid = start * 0.7;

	glBegin(GL_TRIANGLES);

	glColor3f(0.6, 0.6, 0.6);
	glVertex3f(start.x, start.y, start.z);
	glVertex3f(mid.x + thickness, mid.y, mid.z - thickness);
	glVertex3f(mid.x + thickness, mid.y, mid.z + thickness);

	glVertex3f(start.x, start.y, start.z);
	glVertex3f(mid.x - thickness, mid.y, mid.z + thickness);
	glVertex3f(mid.x - thickness, mid.y, mid.z - thickness);

	glVertex3f(mid.x + thickness, mid.y, mid.z - thickness);
	glVertex3f(mid.x + thickness, mid.y, mid.z + thickness);
	glVertex3f(0, 0, 0);

	glVertex3f(mid.x - thickness, mid.y, mid.z - thickness);
	glVertex3f(mid.x - thickness, mid.y, mid.z + thickness);
	glVertex3f(0, 0, 0);

	glColor3f(0.4, 0.4, 0.4);
	glVertex3f(start.x, start.y, start.z);
	glVertex3f(mid.x + thickness, mid.y, mid.z - thickness);
	glVertex3f(mid.x - thickness, mid.y, mid.z - thickness);

	glVertex3f(mid.x - thickness, mid.y, mid.z - thickness);
	glVertex3f(mid.x + thickness, mid.y, mid.z - thickness);
	glVertex3f(0, 0, 0);

	glColor3f(0.8, 1, 1);
	glVertex3f(start.x, start.y, start.z);
	glVertex3f(mid.x + thickness, mid.y, mid.z + thickness);
	glVertex3f(mid.x - thickness, mid.y, mid.z + thickness);

	glVertex3f(mid.x - thickness, mid.y, mid.z + thickness);
	glVertex3f(mid.x + thickness, mid.y, mid.z + thickness);
	glVertex3f(0, 0, 0);

	glEnd();
	glPopMatrix();

}

void SkeletonBase::axis(float size) {
	glLineWidth(2);
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(size, 0, 0);
	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, size, 0);
	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, size);
	glEnd();
}

void SkeletonBase::draw() const {

	ofVec3f offset(0, -0.01, 0);
	for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {
		octa(i, 0.01f);
	}
	// absolute orientations
	for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {
		glPushMatrix();
		glTranslatef(
			absolute_positions[i].x,
			absolute_positions[i].y,
			absolute_positions[i].z
		);
		ofMatrix4x4 m(absolute_rotations[i]);
		glMultMatrixf(m.getPtr());
		axis(0.03f);
		glPopMatrix();
	}

	glColor3f(1, 1, 1);

}

void SkeletonBase::lines() const {
	glLineWidth(2);
	glColor3f(1, 1, 0);
	// CENTER: YELLOW
	glBegin(GL_LINE_STRIP);
	vertex(NUI_SKELETON_POSITION_HIP_CENTER);
	vertex(NUI_SKELETON_POSITION_SPINE);
	vertex(NUI_SKELETON_POSITION_SHOULDER_CENTER);
	vertex(NUI_SKELETON_POSITION_HEAD);
	glEnd();
	// LEFT: MAGENTA
	glColor3f(1, 0, 1);
	glBegin(GL_LINE_STRIP);
	vertex(NUI_SKELETON_POSITION_SHOULDER_CENTER);
	vertex(NUI_SKELETON_POSITION_SHOULDER_LEFT);
	vertex(NUI_SKELETON_POSITION_ELBOW_LEFT);
	vertex(NUI_SKELETON_POSITION_WRIST_LEFT);
	vertex(NUI_SKELETON_POSITION_HAND_LEFT);
	glEnd();
	// RIGHT: CYAN
	glBegin(GL_LINE_STRIP);
	vertex(NUI_SKELETON_POSITION_HIP_CENTER);
	vertex(NUI_SKELETON_POSITION_HIP_LEFT);
	vertex(NUI_SKELETON_POSITION_KNEE_LEFT);
	vertex(NUI_SKELETON_POSITION_ANKLE_LEFT);
	vertex(NUI_SKELETON_POSITION_FOOT_LEFT);
	glEnd();
	glColor3f(0, 1, 1);
	glBegin(GL_LINE_STRIP);
	vertex(NUI_SKELETON_POSITION_SHOULDER_CENTER);
	vertex(NUI_SKELETON_POSITION_SHOULDER_RIGHT);
	vertex(NUI_SKELETON_POSITION_ELBOW_RIGHT);
	vertex(NUI_SKELETON_POSITION_WRIST_RIGHT);
	vertex(NUI_SKELETON_POSITION_HAND_RIGHT);
	glEnd();
	glBegin(GL_LINE_STRIP);
	vertex(NUI_SKELETON_POSITION_HIP_CENTER);
	vertex(NUI_SKELETON_POSITION_HIP_RIGHT);
	vertex(NUI_SKELETON_POSITION_KNEE_RIGHT);
	vertex(NUI_SKELETON_POSITION_ANKLE_RIGHT);
	vertex(NUI_SKELETON_POSITION_FOOT_RIGHT);
	glEnd();
	glPointSize(4);
	glColor3f(1, 1, 1);
	glBegin(GL_POINTS);
	for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {
		vertex(i);
	}
	glEnd();
	glPointSize(1);
	glLineWidth(1);
}

uint8_t SkeletonBase::parent(uint8_t id) const {
	if (id >= NUI_SKELETON_POSITION_COUNT) {
		return NUI_SKELETON_POSITION_COUNT;
	}
	return _bones_parents[id];
}

/////////////////////////////////////
// ********* SKELETON MODEL *********
/////////////////////////////////////

bool SkeletonModel::loadModel() {

	ofFile fmodel(ofToDataPath(AVATAR_MODEL));
	if (fmodel.exists()) {
		ofBuffer buffer(fmodel);
		uint8_t found = 0;
		ofVec3f tmp[NUI_SKELETON_POSITION_COUNT];
		ofMatrix4x4 glob_matrices[NUI_SKELETON_POSITION_COUNT];
		ofMatrix4x4 m;
		for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
			if ((*it)[0] == '#') {
				continue;
			}
			string line = *it;
			if (line.empty()) {
				continue;
			}
			std::vector<std::string> words = ofSplitString(line, " ");
			// name, 3 positions & 16 global matrix values
			if (words.size() < 20) {
				continue;
			}
			uint8_t id = NUI_SKELETON_POSITION_COUNT;
			for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {
				if (words[0].compare(_bone_key[i]) == 0) {
					id = i;
				}
			}
			if (id == NUI_SKELETON_POSITION_COUNT) {
				continue;
			}
			m.makeTranslationMatrix(
				atof(words[1].c_str()),
				atof(words[2].c_str()),
				atof(words[3].c_str())
			);
			m = pos_mati * m * pos_mat;
			tmp[id] = m.getTranslation();
			// loading global matrix
			for (uint8_t i = 4, row = 0; i < 20; i += 4, ++row) {
				glob_matrices[id]._mat[row].set(
					atof(words[i + 0].c_str()),
					atof(words[i + 1].c_str()),
					atof(words[i + 2].c_str()),
					atof(words[i + 3].c_str())
				);
			}
			glob_matrices[id] = rot_mati * glob_matrices[id] * rot_mat;
			// good to go
			++found;
		}
		if (found == NUI_SKELETON_POSITION_COUNT) {

			std::cout << "SkeletonModel successfully loaded" << std::endl;

			for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {
				absolute_positions[i] = tmp[i];
				uint8_t parentid = parent(i);
				if (parentid != NUI_SKELETON_POSITION_COUNT) {
					relative_positions[i] = absolute_positions[i] - absolute_positions[parentid];
				}
				else {
					relative_positions[i] = absolute_positions[i];
				}
				absolute_rotations[i] = glob_matrices[i].getRotate();
				absolute_matrices[i] = glob_matrices[i];
				absolute_matricesi[i] = absolute_matrices[i].getInverse();
				if (parentid != NUI_SKELETON_POSITION_COUNT) {
					delta_matricesi[i] = (
						absolute_matrices[i] * absolute_matricesi[parentid]
						).getInverse();
				}
				else {
					delta_matricesi[i].makeIdentityMatrix();
				}
			}
			//don't ask me why
			origin_mat = absolute_matricesi[NUI_SKELETON_POSITION_HIP_CENTER] *
				absolute_matricesi[NUI_SKELETON_POSITION_SPINE];
			origin_mati = origin_mat.getInverse();
		}
		else {
			std::cout << "SkeletonModel loading failed! " <<
				"only found " << int(found) <<
				" out of " << int(NUI_SKELETON_POSITION_COUNT) <<
				" bones" << std::endl;
			return false;
		}
	}
	else {
		std::cout << "SkeletonModel failed to load " <<
			AVATAR_MODEL << std::endl;
		return false;
	}
	return true;
}

/////////////////////////////////////
// ********* SKELETON DELTA *********
/////////////////////////////////////

SkeletonDelta::SkeletonDelta() :
	SkeletonBase(),
	updated(false) {
}

void SkeletonDelta::relatives() {

	glLineWidth(2);

	for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {

		glPushMatrix();
		glTranslatef(
			absolute_positions[i].x,
			absolute_positions[i].y,
			absolute_positions[i].z
		);

		glPushMatrix();
		glMultMatrixf(ofMatrix4x4(relative_rotations[i]).getPtr());
		axis(0.03f);
		glPopMatrix();

		glColor3f(1, 1, 1);

		glTranslatef(
			-relative_positions[i].x,
			-relative_positions[i].y,
			-relative_positions[i].z
		);

		glMultMatrixf(ofMatrix4x4(relative_rotations[i]).getPtr());

		glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(
			relative_positions[i].x,
			relative_positions[i].y,
			relative_positions[i].z
		);
		glEnd();

		glPopMatrix();

	}

	glLineWidth(1);

}

void SkeletonDelta::diff(const SkeletonBase& newsk, const SkeletonModel& model) {

	updated = true;

	ofMatrix4x4 m;
	m.makeTranslationMatrix(newsk.origin);
	m = model.origin_mati * m * model.origin_mat;
	ofVec3f root_pos = m.getTranslation();
	m.makeIdentityMatrix();

	for (uint8_t i = 0; i < NUI_SKELETON_POSITION_COUNT; ++i) {

		uint8_t parent = _bones_parents[i];

		m.makeRotationMatrix(newsk.absolute_rotations[i]);
		m = m * model.origin_mat;
		m.setTranslation(0, 0, 0);

		absolute_matrices[i] = m;
		absolute_matricesi[i] = m.getInverse();

		// ***** internal visualisation *****
		// GOOD > new absolute orientation
		absolute_rotations[i] = m.getRotate();

		// GOOD > new relative orientation, applied on the relative position of the model
		// the rotation brings the model bone in the kinect orientation
		m = model.absolute_matricesi[i] * m;
		relative_rotations[i] = m.getRotate();

		// rendering absolute positions based on the relative position,
		// to be certain that hierarchy is correct
		relative_positions[i] = relative_rotations[i] * model.relative_positions[i];
		absolute_positions[i] = relative_positions[i];
		if (parent != NUI_SKELETON_POSITION_COUNT) {
			absolute_positions[i] += absolute_positions[parent];
		}

		// ***** output generation *****
		if (parent != NUI_SKELETON_POSITION_COUNT) {
			m = absolute_matrices[i] *
				absolute_matricesi[parent] *
				model.delta_matricesi[i];
		}
		else {
			m = absolute_matrices[i] * model.absolute_matricesi[i];
		}

		// applying rotation alignment
		if (rot_mat_enabled) {
			m = rot_mati * m * rot_mat;
		}
		// and setting the output rotation
		m = calibration_mati * m * calibration_mat;
		output_rotations[i] = m.getRotate();

		m.makeIdentityMatrix();
		m.makeTranslationMatrix(root_pos + absolute_positions[i] );
		m = calibration_mati * m * calibration_mat;
		output_positions[i] = calibration_mat.getTranslation() + m.getTranslation();

	}

}

//////////////////////////////////
// ********* MAPPING KEY *********
//////////////////////////////////

MappingKey::MappingKey(std::string n, size_t num) :
	count(num) {
	_name = n;
	if (count == 0) return;
	pairs.resize(num);
	for (uint16_t i = 0; i < num; ++i) {
		pairs[i] = std::pair< uint8_t, float >(
			NUI_SKELETON_POSITION_COUNT, 0.f);
	}
}

void MappingKey::add(
	size_t indice,
	uint8_t bone_id,
	float weight) {
	if (indice >= pairs.size()) {
		return;
	}
	pairs[indice] = std::pair<uint8_t, float >(bone_id, weight);
}

std::string MappingKey::print() const {
	std::stringstream ss;
	ss << "mapping key: " << name() << std::endl;
	for (uint16_t i = 0; i < size(); ++i) {
		ss << "\t";
		ss << _bone_key[pairs[i].first] << ", weight: " << pairs[i].second;
	}
	return ss.str();
}
