import bge
import bge.logic as gl
from scripts.K2A.ThreadOsc import ThreadOscInit
from scripts.K2A.AvatarManager import AvatarManager
from scripts.K2A.Avatar import Avatar
from scripts.K2A.OSC import decodeOSC

ready = True

'''
DO NOT FORGET TO HIT SPACEBAR BEFORE ESC!
if not, the threads will not be killed, preventing a restart & forcing you to hard quit blender...
'''

# loading presets
try:

	pl = bge.presets["loaded"]

except:

	bge.osc_sender = 0

	bge.presets = {}
	# ('local_IP', local_port, buffer_size, appid, kinect_num, skeleton_num)
	#bge.presets["osc_configuration"] = {}
	bge.presets["osc_configuration"] = { 0: ('192.168.3.68', 23000, 1024, 3, 1, 6), 1 : ('192.168.3.68', 23000, 1024, 2, 1, 6), 2 : ('192.168.3.68', 23000, 1024, 1, 1, 6) }
	# list of bones sent by the streamers
	bge.presets["kbones"] = [ "HIP_CENTER", "SPINE", "SHOULDER_CENTER", "HEAD", "SHOULDER_LEFT", "ELBOW_LEFT", "WRIST_LEFT", "HAND_LEFT", "SHOULDER_RIGHT", "ELBOW_RIGHT", "WRIST_RIGHT", "HAND_RIGHT", "HIP_LEFT", "KNEE_LEFT", "ANKLE_LEFT", "FOOT_LEFT", "HIP_RIGHT", "KNEE_RIGHT", "ANKLE_RIGHT", "FOOT_RIGHT" ]
	# declare avatars here, see K2A.Avatar for full configuration
	bge.presets["avatars"] = []
	#bge.presets["avatars"].append( {"avatar":"stdman", "no_slerp":["HIP_CENTER"], "position_scale":5} )
	#bge.presets["avatars"].append( {"avatar":"stdmani", "no_slerp":["HIP_CENTER"]} )
	#bge.presets["avatars"].append( {"avatar":"veronica"} )
	#bge.presets["avatars"].append( {"avatar":"deer", "move":False} )
	#bge.presets["avatars"].append( {"avatar":"cheeta"} )
	bge.presets["poly_avatars"] = [] # avatar that accepts more than 1 skeleton, based on bone extension: [bone].001, [bone].002, etc
	bge.presets["poly_avatars"].append( {"avatar":"ico"} )

	bge.presets["loaded"] = True
	print("presets loaded")
	
	#starting thread osc
	bge.threadosc = None
	#ThreadOscInit()
	bge.avatar_mgr = AvatarManager()

try:
	bge.avatar_mgr.exists()
except:
	ready = False
	print( "BGE not ready..." )
	pass

if ready == True:

	#print( "ready to run" )
	#bge.avatar_mgr.print()
	bge.avatar_mgr.update()
