import bge
import bge.logic as gl
import socket
from scripts.K2A.ThreadOsc import ThreadOscInit
from scripts.K2A.AvatarManager import AvatarManager
from scripts.K2A.Avatar import Avatar
from scripts.K2A.OSC import decodeOSC

'''
~ main.cpp :
1. check that all objects are instanciated, if not, game never starts
2. once everyhting is ok, calls updates methods of each objects. it is obvious that threads doesn't requires updates...
NB: all objects are added to BGE in their own class...
'''

# bge.logic.setExitKey( bge.events.F12KEY )
# dependency in objects is really important to take into account
# when testing their existance!
# be very carefull about the redundant dependencies!!!
# these objects are ~ to singletons and register themselves in BGE root object

ready = True

# loading presets
try:

	pl = bge.presets["loaded"]

except:

	bge.presets = {}
	#  ('local_IP', local_port, buffer_size, appid, kinect_num, skeleton_num)
	bge.presets["thread"] = { 0: ('192.168.3.68', 23000, 1024, 2, 2, 6), 1 : ('192.168.3.68', 23000, 1024, 1, 1, 6), 2 : ('192.168.3.68', 23000, 1024, 0, 1, 6) }
	bge.presets["avatars"] = [ {"avatar":"stdman"}, {"avatar":"stdman.001"} ]
	bge.presets["loaded"] = True
	print("presets loaded")
	bge.threadosc = None

	#starting thread osc
	ThreadOscInit()
	bge.avatar_mgr = AvatarManager()

try:
	bge.avatar_mgr.exists()
except:
	ready = False
	print( "BGE not ready..." )
	pass

if ready == True:

	#print( "ready to run" )
	#bge.avatar_mgr.print()
	bge.avatar_mgr.update()
