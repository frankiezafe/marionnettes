import bge
import bge.logic as gl
from scripts.K2A.ThreadOsc import ThreadOscInit, OSCClient as osc_sender, OSCMessage as osc_msg
from scripts.K2A.AvatarManager import AvatarManager
from scripts.K2A.Avatar import Avatar
from scripts.K2A.OSC import decodeOSC

ready = True

'''
DO NOT FORGET TO HIT SPACEBAR BEFORE ESC!
if not, the threads will not be killed, preventing a restart & forcing you to hard quit blender...
'''

# loading presets
try:

	pl = bge.presets["loaded"]

except:

	bge.osc_sender = osc_sender()
	bge.osc_sender.connect(('192.168.3.69', 35000))
	oscmsg = osc_msg()
	oscmsg.setAddress("/startup")
	oscmsg.append('HELLO')
	bge.osc_sender.send(oscmsg)

	bge.presets = {}
	# ('local_IP', local_port, buffer_size, appid, kinect_num, skeleton_num)
	# bge.presets["thread"] = { 0: ('192.168.3.68', 23000, 1024, 3, 1, 6), 1 : ('192.168.3.68', 23000, 1024, 2, 1, 6), 2 : ('192.168.3.68', 23000, 1024, 1, 1, 6) }
	# bge.presets["thread"] = { 0: ('192.168.3.91', 23000, 1024, 2, 2, 6), 1 : ('192.168.3.91', 23000, 1024, 1, 1, 6), 2 : ('192.168.3.91', 23000, 1024, 0, 1, 6) }
	# bge.presets["osc_configuration"] = { 0: ('192.168.3.91', 23000, 1024, 2, 2, 6), 1 : ('192.168.3.91', 23000, 1024, 1, 1, 6), 2 : ('192.168.3.91', 23000, 1024, 0, 1, 6) }
	bge.presets["osc_configuration"] = { 0: ('192.168.3.68', 23000, 1024, 3, 1, 6), 1 : ('192.168.3.68', 23000, 1024, 2, 1, 6), 2 : ('192.168.3.68', 23000, 1024, 1, 1, 6) }
	# list of bones sent by the streamers
	bge.presets["kbones"] = [ "HIP_CENTER", "SPINE", "SHOULDER_CENTER", "HEAD", "SHOULDER_LEFT", "ELBOW_LEFT", "WRIST_LEFT", "HAND_LEFT", "SHOULDER_RIGHT", "ELBOW_RIGHT", "WRIST_RIGHT", "HAND_RIGHT", "HIP_LEFT", "KNEE_LEFT", "ANKLE_LEFT", "FOOT_LEFT", "HIP_RIGHT", "KNEE_RIGHT", "ANKLE_RIGHT", "FOOT_RIGHT" ]
	# declare avatars here, see K2A.Avatar for full configuration
	bge.presets["avatars"] = []
	scdef = 0.4
	scpos = 1.5
	bge.presets["avatars"].append( {"root":"blob-root", "avatar":"blob", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.001", "avatar":"blob.001", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.002", "avatar":"blob.002", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.003", "avatar":"blob.003", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.004", "avatar":"blob.004", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.005", "avatar":"blob.005", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.006", "avatar":"blob.006", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.007", "avatar":"blob.007", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.008", "avatar":"blob.008", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["avatars"].append( {"root":"blob-root.009", "avatar":"blob.009", "no_slerp":["HIP_CENTER"], "happy_blob":True, "scale":True, "position_scale":scpos, "move":True, "scale_default": scdef, "scale_speed": 0.8 } )
	bge.presets["poly_avatars"] = [] # no poly avatars
	bge.presets["loaded"] = True
	print("presets loaded")
	
	#starting thread osc
	bge.threadosc = None
	# ThreadOscInit()
	bge.avatar_mgr = AvatarManager()
	# 0bge.avatar_mgr.one2all = True

try:
	bge.avatar_mgr.exists()
except:
	ready = False
	print( "BGE not ready..." )
	pass

if ready == True:

	#print( "ready to run" )
	#bge.avatar_mgr.print()
	bge.avatar_mgr.update()