import bge
import math
import mathutils
import random
import time

class SubSkeleton:

	def __init__(self):
		self.linked = False
		self.idle = True
		self.last_update = 0
		# maps of w,x,y,z quaternion values, by bone
		self.default_quaternion = {}
		self.default_pose = {}
		self.current_pose = {}
		self.received_pose = {}
		self.pose_speed = 0.01
		self.kbone_relation = {}
		self.default_scale = {}
		self.current_scale = {}
		self.target_scale = {}
		self.scale_speed = 0.05
		self.influence = 0
		self.influence_target = 0.75
		self.influence_speed = 0.002
		for kb in bge.presets["kbones"]:
			self.kbone_relation[kb] = None

	def register( self, bone, kbone ):
		q = mathutils.Quaternion()
		q.identity();
		#q = mathutils.Quaternion((0.0, 1.0, 0.0), math.radians(20.0))
		self.default_quaternion[bone] = self.list2quat(bone.rotation_quaternion)
		self.default_pose[bone] = bone.rotation_quaternion
		self.current_pose[bone] = bone.rotation_quaternion
		self.received_pose[bone] = bone.rotation_quaternion
		self.default_scale[bone] = [ bone.scale.x, bone.scale.y, bone.scale.z ]
		self.current_scale[bone] = [ bone.scale.x, bone.scale.y, bone.scale.z ]
		self.kbone_relation[kbone] = bone
		self.stop_receiving()
		#bone.inherit_scale = False

	def listmult( self, list, factorx, factory, factorz ):
		return [ list[0] * factorx, list[1] * factory, list[2] * factorz ]

	def accumulate( self, list1, list2, factor ):
		out = []
		l = len( list1 )
		for i in range(0,l):
			out.append( list1[i] + (list2[i]-list1[i]) * factor )
		return out

	def copy( self, list ):
		out = []
		l = len( list )
		for i in range(0,l):
			out.append( list[i] )
		return out

	def dist( self, list1, list2 ):
		out = 0
		l = len( list1 )
		for i in range(0,l):
			out += abs(list2[i]-list1[i])
		return out

	def list2quat( self, list ):
		return mathutils.Quaternion((list[0], list[1], list[2], list[3]))

	def quat2list( self, quat ):
		return (quat.w, quat.x, quat.y, quat.z)

	def start_receiving( self ):
		self.influence = 0
		for bone in self.default_scale:
			self.target_scale[bone] = self.listmult( self.default_scale[bone], random.uniform( 0.5, 0.6 ), random.uniform( 0.5, 0.6 ), random.uniform( 0.5, 0.6 ) )

	def stop_receiving( self ):
		for bone in self.default_scale:
			self.target_scale[bone] = self.listmult( self.default_scale[bone], random.uniform( 0.1, 0.9 ), 1, random.uniform( 0.1, 0.9 ) )

	def rotate( self, kbone, values ):
		if kbone == "SPINE":
			return
		if self.kbone_relation[kbone] is not None:
			if self.influence == 1:
				self.current_pose[ self.kbone_relation[kbone] ] = values
			else:
				self.current_pose[ self.kbone_relation[kbone] ] = self.accumulate( self.current_pose[ self.kbone_relation[kbone] ], values, self.sined( self.influence ) )

	def sined(self, f):
		return ( 1 + math.sin(-math.pi * 0.5 + math.pi * f) ) * 0.5

	def frame( self, data ):
		#self.target_scale = [1,1,1]
		if self.influence < self.influence_target: 
			self.influence += self.influence_speed
			if self.influence > self.influence_target:
				self.influence = self.influence_target
		self.last_update = int(round(time.time() * 1000))
		# unpacking quaternions
		for d in range( 8, len( data ) ):
			dn = d - 8
			if dn % 5 == 0:
				bname = str( data[ d ] )
				l = [ data[ d+1 ], data[ d+2 ], data[ d+3 ], data[ d+4 ] ]
				self.rotate( bname, l )
				d += 5

	def update( self ):
		for b in self.current_pose:
			if self.linked == False and self.dist( self.current_pose[b], self.default_pose[b] ) > 1e-6:
				self.current_pose[b] = self.accumulate( self.current_pose[b], self.default_pose[b], self.pose_speed )
				if self.dist( self.current_pose[b], self.default_pose[b] ) < 1e-6:
					self.current_pose[b] = self.copy( self.default_pose[b] )
			b.rotation_quaternion = self.current_pose[b]
			if self.dist( self.target_scale[b], self.current_scale[b] ) > 1e-6:
				self.current_scale[b] = self.accumulate( self.current_scale[b], self.target_scale[b], self.scale_speed )
				if self.dist( self.target_scale[b], self.current_scale[b] ) < 1e-6:
					self.current_scale[b] = self.copy( self.target_scale[b] )
			b.scale = self.current_scale[b]

class PolyAvatar():

	def __init__(self):

		self.root = 0
		self.armature = 0
		# relation between avatar bone's names and received ones, see load for creation & applyQuaternion for usage
		# positions
		self.position_scale = 0.5
		self.default_position = mathutils.Vector(( 0.0, 0.0, 0.0 ))
		self.current_position = mathutils.Vector(( 0.0, 0.0, 0.0 ))
		self.received_position = mathutils.Vector(( 0.0, 0.0, 0.0 ))
		self.received_position_count = 0
		# boolean indicating if bone has to be slerped, true by default
		self.move = True
		self.last_frame = 0
		self.frame_received = False
		self.subs = {}
		# used for mapping
		self.sub_hash = {}
		self.avalaible_subs = []

	def load( self, conf ):

		if "avatar" not in conf:
			print( "Avatar configuration nust at least have a key 'avatar'!" )
			return False
		
		avatar_name = conf["avatar"]
		root_name = conf["avatar"]
		if "root" in conf:
			root_name = conf["root"]
		
		try:
			self.root = bge.logic.getCurrentScene().objects[ root_name ]
			self.armature = bge.logic.getCurrentScene().objects[ avatar_name ]
		except:
			print( "no object(s) called '" + root_name + "' (root) or '" + avatar_name + "' (avatar) in current scene" )
			return False
			
		if "move" in conf:
			self.move = conf["move"]

		if self.move == True:
			v = self.root.position
			self.default_position = mathutils.Vector((v.x, v.y, v.z))
			print( "default_position:", self.default_position )

		if "position_scale" in conf:
			self.position_scale = conf["position_scale"]

		for c in self.armature.channels:
			cn = c.name
			for kb in bge.presets["kbones"]:
				if cn[:len(kb)] == kb:
					key = cn[len(kb):]
					if len( key ) == 0:
						key = "default"
					if key not in self.subs:
						self.subs[key] = SubSkeleton()
						self.avalaible_subs.append( self.subs[key] )
						print( "SubSkeleton creation for '" + key + "'" )
					self.subs[key].register( c, kb )
					c.rotation_mode = bge.logic.ROT_MODE_QUAT

		print( "Poly avatar loaded with", len(self.armature.channels), "bones and", len( self.subs ), "sub-skeleton(s)" )
		for s in self.subs:
			sksub = self.subs[s]
			print( "\tsub skeleton '" + s + "' with " + str( len(sksub.default_quaternion) ) + " bones registered" )
			for ks in sksub.kbone_relation:
				if sksub.kbone_relation[ks] is None:
					print( "\t\tno map for " + ks )
					continue
				print( "\t\t" + sksub.kbone_relation[ks].name + " <> " + ks )
		return True

	def frame( self, data ):

		appid = int( data[ 2 ] )
		kinectid = int( data[ 3 ] )
		skeletonid = int( data[ 4 ] )
		key = appid * 100 + kinectid * 10 + skeletonid
		if key not in self.sub_hash:
			if len( self.avalaible_subs ) == 0:
				print( "Poly avatar is at full capacity, refusing kinect", key )
				return
			else:
				self.sub_hash[key] = self.avalaible_subs[0]
				self.sub_hash[key].linked = True
				self.avalaible_subs.remove( self.sub_hash[key] )
				print( "linking sub '" + str( self.sub_hash[key] ) + "' to kinect '" + str( key ) + "'" )

		self.frame_received = True
		self.last_frame = int(round(time.time() * 1000))
		self.sub_hash[key].frame( data )

		#print( "frame!", self.last_frame )
		if ( self.move == True ):
			x = data[ 5 ] * self.position_scale
			y = data[ 6 ] * self.position_scale
			z = data[ 7 ] * self.position_scale * 0.01
			self.received_position = mathutils.Vector(( x,y,z ))
			self.current_position = self.current_position + self.received_position
			self.received_position_count += 1

	def update(self):
		
		now = int(round(time.time() * 1000))

		# trashing old relations
		tmp = {}
		for khash in self.sub_hash:
			if now - self.sub_hash[khash].last_update > 300:
				self.sub_hash[khash].stop_receiving()
				self.sub_hash[khash].linked = False
				self.avalaible_subs.append( self.sub_hash[khash] )
				print( "dropping relation", khash, self.sub_hash[khash] )
			else:
				tmp[khash] = self.sub_hash[khash]
		self.sub_hash = tmp

		for s in self.subs:
			self.subs[s].update()

		self.armature.update()
		if self.move == True and self.received_position_count > 0:
			self.current_position = self.current_position * ( 1.0 / self.received_position_count )
			v = [ self.root.position.x, self.root.position.y, self.root.position.z ]
			vt = [ self.current_position.x, self.current_position.y, self.current_position.z ]
			for s in self.subs:
				v = self.subs[s].accumulate( v, vt, 0.01 )
				break
			self.root.position = v
		'''
		elif self.move == True:
			v = [ self.root.position.x, self.root.position.y, self.root.position.z ]
			vt = [ self.default_position.x, self.default_position.y, self.default_position.z ]
			for s in self.subs:
				v = self.subs[s].accumulate( v, vt, 0.03 )
				break
			self.root.position = v
		'''


		# reseting frame received to 0
		self.frame_received = False
		self.current_position = mathutils.Vector((self.default_position.x, self.default_position.y, self.default_position.z))
		self.received_position_count = 0