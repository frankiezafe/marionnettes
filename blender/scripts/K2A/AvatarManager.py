import bge
import time
import random
from .Avatar import Avatar, HappyBlobPose
from .PolyAvatar import PolyAvatar
from .OSCRapidSocket import OSCRapidSocket
from .ThreadOsc import decodeOSC, OSCMessage as osc_msg

class AvatarManager():
	
	def __init__(self):

		self.one2all = False
		self.delay_asleep = 1000
		self.avatars = []
		self.available_avatars = []
		self.mapped_avatars = {}
		self.poly_avatars = []
		# when using OSCRapidSocket
		self.osc_sockets = []
		# ThreadOsc will read these 2 params
		self.rcvdata = []	   # array to hold data
		self.rcvmutex = False   # mutex, ThreadOsc will wait for it to be False before writting the data

		self.loadAvatars()

		# OSC management
		if bge.threadosc is not None:
			bge.threadosc.addListener( self )
			return
		else:
			print( "No thread osc declared" )

		for i in range( len( bge.presets["osc_configuration"] ) ):
			cfg = bge.presets["osc_configuration"][i]
			for kinect in range( 0, cfg[4] ):
				for skeleton in range( 0, cfg[5] ):
					port = int( cfg[1] ) + int( cfg[3] ) * 100 + kinect * 10 + skeleton + 1
					ors = OSCRapidSocket()
					ors.configure( cfg[0], port, cfg[2] )
					self.osc_sockets.append( ors )

	def loadAvatars(self):
		for avconf in bge.presets["avatars"]:
			self.loadAvatar(avconf)
		print( self.avatars )
		#try:
		for polyavconf in bge.presets["poly_avatars"]:
			self.loadPolyAvatar(polyavconf)
		print( self.avatars )
		#except:
		#	print( "no presets[\"poly_avatars\"]" )

	def loadPolyAvatar(self, conf ):
		a = PolyAvatar()
		success = a.load( conf )
		if success == True:
			self.registerPolyAvatar( a )
		else:
			print( "Error while loading poly avatar", conf )

	def loadAvatar(self, conf ):
		a = Avatar()
		success = a.load( conf )
		if success == True:
			self.registerAvatar( a )
			#self.awakeAvatar( a )
		else:
			print( "Error while loading avatar", conf )

	def registerPolyAvatar(self, avatar):
		print( "registering poly avatar", avatar.armature.name )
		self.poly_avatars.append( avatar )

	def registerAvatar(self, avatar):
		print( "registering avatar", avatar.armature.name )
		self.avatars.append( avatar )
		self.available_avatars.append( avatar )

	def awakeAvatar(self, avatar):
		print( "awakening avatar", avatar.armature.name )
		self.available_avatars.remove( avatar )
		avatar.wakeup()

	def asleepAvatar(self, avatar):
		print( "asleeping avatar", avatar.armature.name )
		avatar.sleep()
		self.available_avatars.append( avatar )
		for k in self.mapped_avatars:
			if self.mapped_avatars[k] == avatar:
				del self.mapped_avatars[k]
				return

	def exists(self):
		return True

	def sendSkeletonFrame( self, data ):

		appid = int( data[ 2 ] )
		kinectid = int( data[ 3 ] )
		skeletonid = int( data[ 4 ] )
		key = appid * 100 + kinectid * 10 + skeletonid

		if self.one2all == True:

			for av in self.avatars:
				if av.idle == True:
					self.awakeAvatar(av)
				av.UID = key
				av.frame( data )
		else:

			if len( self.avatars ):
				oktogo = True
				if key not in self.mapped_avatars:
					if len( self.available_avatars ) == 0:
						oktogo = False
					else:
						i = random.randint(0, len( self.available_avatars )-1)
						a = self.available_avatars[i]
						self.awakeAvatar(a)
						self.mapped_avatars[key] = a
						self.mapped_avatars[key].UID = key
						print( "linking", key ,"to avatar", self.mapped_avatars[key].armature.name )
				if oktogo == True:
					self.mapped_avatars[key].frame( data )

		# poly avatars manage data themselves
		for polya in self.poly_avatars:
			polya.frame( data )

	# previous version, require ThreadOsc and its OSCworkers to be correctly instanciated
	def oscEvents( self ):
		self.rcvmutex = True
		tmp = list( self.rcvdata )
		self.rcvdata = []
		self.rcvmutex = False
		if len( tmp ) == 0:
			return
		for data in tmp:
			if data[ 0 ] == "/skf" :
				self.sendSkeletonFrame( data )
			else:
				print( data[ 0 ] )

	def rapidOSC( self ):
		for socket in self.osc_sockets:
			socket.update()
			while socket.data_received == True:
				#print( "received from", socket, socket.data[ 0 ] )
				if socket.data[ 0 ] == "/skf" :
					self.sendSkeletonFrame( socket.data )
				socket.update()

	def keyboardEvents( self ):
		kbe = bge.logic.keyboard.events
		if kbe[ bge.events.SPACEKEY ] == bge.logic.KX_INPUT_JUST_ACTIVATED:
			if bge.threadosc is not None:
				print( "Network shutdown" )
				bge.threadosc.stop()
				bge.threadosc.killall()
				del bge.threadosc
				bge.threadosc = None

	def update( self ):

		now = int(round(time.time() * 1000))
		
		# events management
		self.keyboardEvents()
		if bge.threadosc is not None:
			self.oscEvents()
		else:
			self.rapidOSC()

		# update of all avatars
		happy_blobs = []
		for a in self.avatars:
			if a.happy_blob == True:
				happy_blobs.append( a )
			a.update()
			if a.idle == False and a.awake_percent == 1 and now - a.last_frame > self.delay_asleep:
				self.asleepAvatar( a )
			elif a.is_asleep:
				#print( "avatar went to sleep!" )
				a.idle = True

		if len( happy_blobs ) > 0:
			keepon = False
			actives = 0
			for hb in happy_blobs:
				if hb.idle == False:
					actives += 1
				if hb.happy_blob_changed == True:
					keepon = True
			if keepon == True:
				try:
					oscmsg = osc_msg()
					oscmsg.setAddress("/happy_blobs")
					oscmsg.append( actives )			
					for hb in happy_blobs:
						oscmsg.append( hb.UID )
						if hb.happy_blob_changed == True:
							oscmsg.append( 1 )
							if hb.happy_blob_pose[ "ELBOW_LEFT" ][0] == HappyBlobPose.RELAX and hb.happy_blob_pose[ "ELBOW_RIGHT" ][0] == HappyBlobPose.RELAX:
								oscmsg.append( "arms_relax" )
							else:
								oscmsg.append( "arms_up" )
							if hb.happy_blob_pose[ "KNEE_LEFT" ][0] == HappyBlobPose.RELAX and hb.happy_blob_pose[ "KNEE_RIGHT" ][0] == HappyBlobPose.RELAX:
								oscmsg.append( "legs_relax" )
							else:
								oscmsg.append( "legs_up" )
							if hb.happy_blob_pose[ "SHOULDER_CENTER" ][0] == HappyBlobPose.RELAX:
								oscmsg.append( "body_bent" )
							else:
								oscmsg.append( "body_relax" )
						else:
							oscmsg.append( 0 )
							oscmsg.append( "arms_relax" )
							oscmsg.append( "legs_relax" )
							oscmsg.append( "body_relax" )

					bge.osc_sender.send(oscmsg)
				except:
					print( "Receiver is not connected!!!!!!!!!!!!!!!!!!!!!!!!" )

		for pa in self.poly_avatars:
			pa.update()

	def print(self):
		print( "avatar manager", self.avatars )