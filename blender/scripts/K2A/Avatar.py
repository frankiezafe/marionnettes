import bge
import math
import mathutils
import time
import random
from enum import Enum

from .ThreadOsc import OSCMessage as osc_msg

class HappyBlobPose( Enum ):
	RELAX = 1
	ARM_UP = 2
	BENT = 3
	LEG_UP = 4

class Avatar():

	def __init__(self):

		self.UID = 0

		self.root = 0
		self.armature = 0
		# relation between avatar bone's names and received ones, see load for creation & applyQuaternion for usage
		self.kbone_relation = {}
		# positions
		self.position_scale = 1
		self.default_position = [ 0.0, 0.0, 0.0 ]
		self.current_position = [ 0.0, 0.0, 0.0 ]
		self.received_position = [ 0.0, 0.0, 0.0 ]
		# maps of w,x,y,z quaternion values, by bone
		self.default_quaternion = {}
		self.current_pose = {}
		self.received_pose = {}
		# boolean indicating if bone has to be slerped, true by default
		self.pose_slerp = {}
		# scale
		self.scale = False
		self.scale_speed = 0.1
		self.scale_default = 0.2
		self.random_scale_multiplier = [ 1,1, 0.9,1.2, 1,1 ]
		self.default_scale = {}
		self.current_scale = {}
		self.target_scale = {}

		self.is_asleep = False
		self.idle = True

		self.move_speed = 0.2
		self.move = True
		self.awake_percent = 0
		self.wakeup_delay = 2000
		self.wakeup_time = 0
		self.asleep_delay = 2000
		self.asleep_time = 0
		self.last_frame = 0
		self.frame_received = False

		# customisation
		self.happy_blob = False
		self.happy_blob_changed = False
		self.happy_blob_pose = {}

	def load( self, conf ):

		if "avatar" not in conf:
			print( "Avatar configuration nust at least have a key 'avatar'!" )
			return False
		
		avatar_name = conf["avatar"]
		root_name = conf["avatar"]
		if "root" in conf:
			root_name = conf["root"]
		
		try:
			self.root = bge.logic.getCurrentScene().objects[ root_name ]
			self.armature = bge.logic.getCurrentScene().objects[ avatar_name ]
		except:
			print( "no object(s) called '" + root_name + "' (root) or '" + avatar_name + "' (avatar) in current scene" )
			return False
			
		if "move" in conf:
			print( "enabling move on", avatar_name )
			self.move = conf["move"]

		if "scale" in conf:
			self.scale = conf["scale"]
			print( "scale on", avatar_name, self.scale )

		if "scale_speed" in conf:
			self.scale_speed = conf["scale_speed"]
			print( "scale_speed on", avatar_name, self.scale_speed )

		if "happy_blob" in conf:
			print( "enabling happy_blob on", avatar_name )
			self.happy_blob = conf["happy_blob"]
			if self.happy_blob == True:
				#												CURRENT 			PREVIOUS
				self.happy_blob_pose[ "ELBOW_LEFT" ] = [ HappyBlobPose.RELAX, HappyBlobPose.RELAX ]
				self.happy_blob_pose[ "ELBOW_RIGHT" ] = [ HappyBlobPose.RELAX, HappyBlobPose.RELAX ]
				self.happy_blob_pose[ "SHOULDER_CENTER" ] = [ HappyBlobPose.RELAX, HappyBlobPose.RELAX ]
				self.happy_blob_pose[ "KNEE_RIGHT" ] = [ HappyBlobPose.RELAX, HappyBlobPose.RELAX ]
				self.happy_blob_pose[ "KNEE_LEFT" ] = [ HappyBlobPose.RELAX, HappyBlobPose.RELAX ]

		if "scale_default" in conf:
			self.scale_default = conf["scale_default"]

		if self.move == True:
			self.default_position = [ self.root.position.x, self.root.position.y, self.root.position.z ]
			self.current_position = [ self.root.position.x, self.root.position.y, self.root.position.z ]
			self.received_position = [ self.root.position.x, self.root.position.y, self.root.position.z ]
			print( "default_position:", self.default_position )

		if "wakeup_delay" in conf:
			self.wakeup_delay = conf["wakeup_delay"]

		if "asleep_delay" in conf:
			self.asleep_delay = conf["asleep_delay"]

		if "position_scale" in conf:
			self.position_scale = conf["position_scale"]

		for kb in bge.presets["kbones"]:
			self.kbone_relation[kb] = []

		q = mathutils.Quaternion()
		q.identity();

		for c in self.armature.channels:
			c.rotation_mode = bge.logic.ROT_MODE_QUAT
			cn = c.name
			self.default_quaternion[cn] = self.list2quat(c.rotation_quaternion)
			self.current_pose[cn] = self.quat2list(q)
			self.received_pose[cn] = self.quat2list(q)
			if self.scale == True:
				self.default_scale[cn] = [ c.scale.x, c.scale.y, c.scale.z ]
				self.current_scale[cn] = [ c.scale.x, c.scale.y, c.scale.z ]
				self.target_scale[cn] = [ c.scale.x * self.scale_default, c.scale.y * self.scale_default, c.scale.z * self.scale_default ]
			self.pose_slerp[cn] = True
			if "no_slerp" in conf:
				for b in conf["no_slerp"]:
					if cn == b:
						self.pose_slerp[cn] = False
			for kb in bge.presets["kbones"]:
				if cn[:len(kb)] == kb:
					self.kbone_relation[kb].append( cn )

		print( "Avatar loaded!" )

		return True

	def listmult( self, list, factorx, factory, factorz ):
		return [ list[0] * factorx, list[1] * factory, list[2] * factorz ]

	def add( self, list1, list2, factor ):
		out = []
		l = len( list1 )
		for i in range(0,l):
			out.append( list1[i] + list2[i] * factor )
		return out

	def accumulate( self, list1, list2, factor ):
		out = []
		l = len( list1 )
		for i in range(0,l):
			out.append( list1[i] + (list2[i]-list1[i]) * factor )
		return out

	def copy( self, list ):
		out = []
		l = len( list )
		for i in range(0,l):
			out.append( list[i] )
		return out

	def dist( self, list1, list2 ):
		out = 0
		l = len( list1 )
		for i in range(0,l):
			out += abs(list2[i]-list1[i])
		return out

	def awake(self):
		self.idle = False
		self.is_asleep = False

	def wakeup( self ):
		self.awake()
		self.wakeup_time = int(round(time.time() * 1000)) + self.wakeup_delay
		self.asleep_time = 0
		if self.scale == True:
			for c in self.armature.channels:
				cn = c.name
				self.target_scale[cn] = self.listmult( self.default_scale[cn], random.uniform( self.random_scale_multiplier[0], self.random_scale_multiplier[1] ), random.uniform( self.random_scale_multiplier[2], self.random_scale_multiplier[3] ), random.uniform( self.random_scale_multiplier[4], self.random_scale_multiplier[5] ) )

	def sleep( self ):
		self.awake()
		self.idle = False
		self.wakeup_time = 0
		self.asleep_time = int(round(time.time() * 1000)) + self.asleep_delay
		if self.scale == True:
			for c in self.armature.channels:
				cn = c.name
				self.target_scale[cn] = self.listmult( self.default_scale[cn], self.scale_default, self.scale_default, self.scale_default )
				#print( "back to", cn, self.target_scale )
		else:
			a = 0
			#print( "no scaling on", self.armature.name )

	def default_slerp( self, percent ):
		for c in self.armature.channels:
			cn = c.name
			if self.pose_slerp[cn] == False or percent <= 0:
				self.current_pose[cn] = self.received_pose[cn]
			else:
				self.current_pose[cn] = self.quat2list( self.list2quat( self.received_pose[cn] ).slerp( self.default_quaternion[cn], percent ) )
		#if self.move == True and percent > 0:
		#	self.current_position = self.received_position.lerp( self.default_position, percent )

	def rotate( self, kbone, values ):
		if values[0] != values[0]:
			print( "????", values[0] )
			return
		for b in self.kbone_relation[kbone]:
			self.received_pose[b] = values
			if self.awake_percent == 1:
				self.current_pose[b] = values

	def frame( self, data ):
		self.idle = False
		self.last_frame = int(round(time.time() * 1000))
		self.frame_received = True
		#print( "frame!", self.last_frame )
		if ( self.move == True ):
			self.received_position = ( -data[ 5 ] * self.position_scale, data[ 6 ] * self.position_scale * 3, data[ 7 ] * self.position_scale * 0.1 )
			#self.received_position = self.add( self.received_position, self.default_position, 1 )
			self.current_position = self.accumulate( self.current_position, self.received_position, self.move_speed )
		# unpacking quaternions
		for d in range( 8, len( data ) ):
			dn = d - 8
			if dn % 5 == 0:
				bname = str( data[ d ] )
				l = [ data[ d+1 ], data[ d+2 ], data[ d+3 ], data[ d+4 ] ]
				self.rotate( bname, l )
				d += 5

	def apply_scale(self):
		for c in self.armature.channels:
			cn = c.name
			if self.dist( self.target_scale[cn], self.current_scale[cn] ) > 1e-6:
				self.current_scale[cn] = self.accumulate( self.current_scale[cn], self.target_scale[cn], self.scale_speed )
				if self.dist( self.target_scale[cn], self.current_scale[cn] ) < 1e-6:
					self.current_scale[cn] = self.copy( self.target_scale[cn] )
				c.scale = self.current_scale[cn]

	def happy_scale( self, bname ):
		q = self.list2quat( self.received_pose[bname] )
		eulers = q.to_euler( 'ZYX' )
		zangl = abs(eulers.z)
		#active = False
		if zangl < 0.4:
			self.armature.channels[bname].scale = [1,1,1]
			if bname == "ELBOW_LEFT" or bname == "ELBOW_RIGHT":
				self.happy_blob_pose[ bname ][0] = HappyBlobPose.RELAX
		else:
			self.armature.channels[bname].scale = [ 1, 1 + math.sin(abs(eulers.z) - 0.4) * 3, 1 ]
			if bname == "ELBOW_LEFT" or bname == "ELBOW_RIGHT":
				self.happy_blob_pose[ bname ][0] = HappyBlobPose.ARM_UP
			#active = True

	def happy_bend( self, bname ):
		q = self.list2quat( self.received_pose[bname] )
		eulers = q.to_euler( 'XYZ' )
		xangl = abs(eulers.x)
		active = False
		if xangl < 0.4:
			active = True
		if bname == "SHOULDER_CENTER":
			if active == True:
				self.happy_blob_pose[ bname ][0] = HappyBlobPose.BENT
			else:
				self.happy_blob_pose[ bname ][0] = HappyBlobPose.RELAX
		elif bname == "KNEE_RIGHT" or bname == "KNEE_LEFT":
			if active == True:
				self.happy_blob_pose[ bname ][0] = HappyBlobPose.LEG_UP
			else:
				self.happy_blob_pose[ bname ][0] = HappyBlobPose.RELAX

	def apply_happy_blob(self):
		if "ELBOW_LEFT" in self.kbone_relation:
			self.happy_scale( self.kbone_relation["ELBOW_LEFT"][0] )
		if "ELBOW_RIGHT" in self.kbone_relation:
			self.happy_scale( self.kbone_relation["ELBOW_RIGHT"][0] )
		if "SHOULDER_CENTER" in self.kbone_relation:
			self.happy_bend( self.kbone_relation["SHOULDER_CENTER"][0] )
		if "KNEE_RIGHT" in self.kbone_relation:
			self.happy_bend( self.kbone_relation["KNEE_RIGHT"][0] )
		if "KNEE_LEFT" in self.kbone_relation:
			self.happy_bend( self.kbone_relation["KNEE_LEFT"][0] )
		self.happy_blob_changed = False
		for k in self.happy_blob_pose:
			if self.happy_blob_pose[k][1] != self.happy_blob_pose[k][0]:
				self.happy_blob_changed = True
				self.happy_blob_pose[k][1] = self.happy_blob_pose[k][0]

	def update(self):

		if self.idle == True:
			if self.scale == True:
				self.apply_scale()
				self.armature.update()
			#print( "avatar is idled" )
			return
		
		now = int(round(time.time() * 1000))

		#print( "avatar update", now )

		if self.wakeup_time != 0:

			self.awake_percent = 1 - (self.wakeup_time - now) / self.wakeup_delay
			if self.awake_percent < 0:
				self.awake_percent = 0
			elif self.awake_percent >= 1:
				# stopping awakening
				self.awake_percent = 1
				self.wakeup_time = 0
			# slerping from default to current
			self.default_slerp( 1 - self.awake_percent )

		elif self.asleep_time != 0:
			
			self.awake_percent = (self.asleep_time - now) / self.asleep_delay
			if self.awake_percent < 0:
				# stopping asleep
				self.awake_percent = 0
				self.asleep_time = 0
				# go to asleep!
				self.is_asleep = True
			elif self.awake_percent >= 1:
				self.awake_percent = 1
			# slerping from default to current
			self.default_slerp( 1 - self.awake_percent )

		for c in self.armature.channels:
			cn = c.name
			c.rotation_quaternion = self.current_pose[c.name]
	
		if self.scale == True:
			self.apply_scale()
	
		if self.happy_blob == True:
			self.apply_happy_blob()

		self.armature.update()

		if self.move == True:
			self.root.position = self.current_position
			#print( self.root.position )

		# reseting frame received to 0
		self.frame_received = False

	def list2quat( self, list ):
		return mathutils.Quaternion((list[0], list[1], list[2], list[3]))

	def quat2list( self, quat ):
		return (quat.w, quat.x, quat.y, quat.z)

	def print(self):
		print( "avatar", self.root, self.armature )