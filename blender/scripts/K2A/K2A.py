import sys
import math
import random
import bge
import bge.logic as gl
import mathutils
import socket
from .OSC import decodeOSC

K2A_buffersize = 1024
K2A_root = 0
K2A_OSC_PRINT = False

def applyPosition( avatar_name, values ):
	try:
		avatar = K2A_root["avatars"][ avatar_name ]
		obj = avatar["root"]
		obj.worldPosition = values
	except:
		print( "Unexpected error:", sys.exc_info()[0] )
		pass

def applyQuaternion( avatar_name, bone, values ):
	global root
	blist = []
	startat = 0
	avatar = 0
	try:
		avatar = K2A_root["avatars"][ avatar_name ]
	except:
		return
	while avatar["bonelist"].find( ":"+bone, startat ) != -1:
		startat = avatar["bonelist"].find( ":"+bone, startat )
		dot = avatar["bonelist"].find( ":", startat+1 )
		name = avatar["bonelist"][startat+1:dot]
		startat += 1
		blist.append( name )
	for n in blist:
		try:
			bone = avatar["obj"].channels[ n ]
			bone.rotation_quaternion = [values[0], values[1], values[2],values[3]]
		except:
			print( "Unexpected error:", sys.exc_info()[0] )
			print( 
				"applyQuaternion crashed: '" + 
				avatar["obj"].name + "' '" + n + "'", 
				values )
			pass

def initAvatar( root_name, avatar_name ):
	
	global K2A_root
	
	try:
		root_obj = bge.logic.getCurrentScene().objects[ root_name ]
		obj = bge.logic.getCurrentScene().objects[ avatar_name ]
	except:
		print( "no object(s) called '" + root_name + "' or '" + avatar_name + "' in current scene" )
		return
	bonelist = ""
	K2A_root["avatars"][ avatar_name ] = {}
	K2A_root["avatars"][ avatar_name ]["root"] = root_obj
	K2A_root["avatars"][ avatar_name ]["obj"] = obj
	K2A_root["avatars"][ avatar_name ]["bonelist"] = ""
	for c in obj.channels:
		c.rotation_mode = bge.logic.ROT_MODE_QUAT
		cn = c.name
		bonelist = bonelist + ":"+cn+":"
	K2A_root["avatars"][ avatar_name ]["bonelist"] = bonelist
	print( "*************> ", avatar_name, bonelist )

def updateAvatars():
	
	global K2A_root
	
	for n in K2A_root["avatars"]:
		K2A_root["avatars"][n]["obj"].update()

def k2a_isvalid():
	if K2A_root != 0:
		return True
	return False

def k2a_init( root_name, osc_configuration, avatars ):

	global K2A_root
	global K2A_buffersize
	
	if k2a_isvalid() == True:
		return
	
	root = bge.logic.getCurrentScene().objects[ root_name ]
	
	if "K2A_root" in root:
		return
	
	root["K2A"] = {}
	K2A_root = root["K2A"]
	K2A_root["osc"] = osc_configuration
	K2A_root["initialised"] = False
	K2A_root["avatars"] = {}
	print( "*************K2A - osc initialisation starts*************" )
	# ------------ set list for get_osc ---------------- #
	gl.connected = []
	gl.socket = []
	buffer_size = 1024
	for i in range( len( K2A_root["osc"] ) ):
		gl.connected.append(False)
		gl.socket.append(0)
		# ------------ set some var ---------------- #
		gl.tempo = 0
		ip = osc_configuration[i][0]
		port = osc_configuration[i][1]
		buffer_size = osc_configuration[i][2]
		# ------------ connecting ---------------- #
		if not gl.connected[i] :
			gl.socket[i] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			try:
				gl.socket[i].bind((ip, port))
				gl.socket[i].setblocking(0)
				gl.socket[i].settimeout(0.01)
				gl.socket[i].setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, K2A_buffersize)
				print('Plug : IP = {} Port = {} Buffer Size = {} Instance = {}'.format(ip, port, K2A_buffersize, i))
				gl.connected[i] = True
			except:
				print('No connected to IP = {} Port = {}'.format(ip, port))
				pass
	print( "*************K2A - osc initialisation stops*************" )
	print( "*************K2A - avatars initialisation starts*************" )
	for av in avatars:
		initAvatar( av, av )
	print( "*************K2A - avatars initialisation stops*************" )

def k2a_update():
	
	global K2A_root
	global K2A_buffersize
	
	if k2a_isvalid() == False:
		return
	
	# ------------ gather messages ---------------- #
	for i in range( len( K2A_root["osc"] ) ):
		if gl.connected[i] :
			try:
				raw_data = gl.socket[i].recv( K2A_buffersize )
				data = decodeOSC(raw_data)
				if K2A_OSC_PRINT == True:
					print( data )
				if data[0] == "/skpos" :
					kinectid = data[ 2 ]
					skid = data[ 3 ]
					for av in K2A_root["avatars"]:
						applyPosition( av, [ 
							float( data[4] ) * 0.001, 
							float( data[5] ) * 0.001, 
							float( data[6] ) * 0.001
							] )
				elif data[0] == "/skf" :
					#print( "skrot", data )
					if len( data ) < 8:
						continue
					appid = data[ 2 ]
					kinectid = data[ 3 ]
					skid = data[ 4 ]
					#worldx = data[ 5 ]
					#worldy = data[ 6 ]
					#worldz = data[ 7 ]
					for av in K2A_root["avatars"]:
						applyPosition( av, [ data[ 5 ], data[ 6 ], data[ 7 ] ] )
						break
						
					for d in range( 8, len( data ) ):
						dn = d - 8
						if dn % 5 == 0:
							bname = str( data[ d ] )
							try:
								l = [ data[ d+1 ], data[ d+2 ], data[ d+3 ], data[ d+4 ] ]
								for av in K2A_root["avatars"]:
									applyQuaternion( av, bname, l )
								d += 5
							except:
								print( "OSC format error, no bone named '" + bname + "'!" )
			except IOError as io_error:
				pass
			except socket.error:
				pass
	# ------------ updating armature ---------------- #
	updateAvatars()