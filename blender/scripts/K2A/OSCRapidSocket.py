import socket
import traceback
from .OSC import decodeOSC

class OSCRapidSocket():
	
	def configure(self, ip, port, bufsize):
		self.data = None
		self.connected = False
		self.data_received = False
		self.ip = ip
		self.port = port
		self.buffersize = bufsize
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		print('Trying to connect to IP = {} Port = {}'.format( self.ip, self.port ))
		try:
			self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, self.buffersize )
			self.socket.bind(( self.ip, self.port ))
			self.socket.setblocking(0)
			self.socket.settimeout(0)
			print('Plug : IP = {} Port = {} Buffer Size = {}'.format( self.ip, self.port, self.buffersize))
			self.connected = True
		except socket.error:
			print('Not connected to IP = {} Port = {}, socket error'.format( self.ip, self.port ))
			traceback.print_exc()
		except:
			print('Not connected to IP = {} Port = {}, other error'.format( self.ip, self.port ))

	def update(self):

		if self.connected == True:

			if self.data is not None:
				del self.data
			
			self.data = None
			self.data_received = False
			
			try:
				raw_data = self.socket.recv( self.buffersize )
			except:
				self.data = None
				return

			if len( raw_data ) == 0:
				self.data = None
				return

			else:
				self.data_received = True

			self.data = decodeOSC( raw_data )