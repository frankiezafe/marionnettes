# Socket without errors throwing

This library is similar to official socket wrapper, but it returns an empty message instead of generating an error.

Version for linux 64bits and blender 2.78c.

Unzip _socket.cpython-35m-x86_64-linux-gnu.so.zip in current folder.

Deploy it in: **blender-2.78c-linux-glibc219-x86_64/2.78/python/lib/python3.5/lib-dynload**

 $ cd [installation path]/blender-2.78c-linux-glibc219-x86_64/2.78/python/lib/python3.5/lib-dynload
 
 $ cp _socket.cpython-35m-x86_64-linux-gnu.so _socket.cpython-35m-x86_64-linux-gnu.so.BU01
 
 $ cp [this folder path]/_socket.cpython-35m-x86_64-linux-gnu.so _socket.cpython-35m-x86_64-linux-gnu.so
 
