/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of polymorph package
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2016 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ___________________________________( ^3^)_____________________________________
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 */

/* 
 * File:   CustomApp.cpp
 * Author: frankiezafe
 * 
 * Created on November 19, 2016, 6:07 PM
 */

#include "CustomApp.h"

using namespace std;
using namespace Ogre;
using namespace polymorph;

CustomApp::CustomApp() :
PolymorphApplication(),
resetAnim(false),
mouse_button(0),
bones_current(0),
bones_num(0),
bones(0) {
    polyMgr.verbose(true);
    polyMgr.parse();
}

void CustomApp::setupResources(void) {

    PolymorphApplication::setupResources(polyMgr.getProject());

}

void CustomApp::createScene(void) {

    // keep default behavior
    PolymorphApplication::createScene();

    trayMgr->hideAll();

    polyMgr.setSceneManager(sceneMgr);
    polyMgr.nextScene();

    sophiekhan = polyMgr.node("sophiekhan");

    // enabling skeleton debugging
    sophiekhan->debug(PDM_SKELETON, true);

    // getting all bone's name
    size_t bnum;
    std::string* bnames;
    sophiekhan->getBoneNames(bnum, bnames);
    cout << "Getting bone's names" << endl;
    for (size_t i = 0; i < bnum; ++i) {
        cout << "\tbone[" << i << "] name: '"
                << bnames[ i ] << "'" << endl;
    }
    if (bnames) {
        delete[] bnames;
    }

    // better method: storing PBone pointer (faster)
    sophiekhan->getBones(bones_num, bones);
    // storing default bone orientations and positions
    bquats_init = new Quaternion[ bones_num ];
    bquats_current = new Quaternion[ bones_num ];
    bpos_init = new Vector3[ bones_num ];
    bpos_current = new Vector3[ bones_num ];
    // all bones ponters are now stored in 'bones'
    for (size_t i = 0; i < bones_num; ++i) {
        sophiekhan->getBoneOrientation(bones[ i ], bquats_init[ i ]);
        bquats_current[ i ] = bquats_init[ i ];
        sophiekhan->getBonePosition(bones[ i ], bpos_init[ i ]);
        bpos_current[ i ] = bpos_init[ i ];
    }
    cout << "Getting bone pointers" << endl;
    for (size_t i = 0; i < bones_num; ++i) {
        sophiekhan->getBoneOrientation(bones[ i ], bquats_init[ i ]);
        cout << "\tbone[" << i << "] name: '"
                << bones[ i ]->getName() << "'" << endl;
    }

    bone_hightlight.sphere(sceneMgr, "bone highlighter");
    bone_hightlight.material("General", "sphere-mat");
    bone_hightlight.scale(10);

    
    // stdman
    stdman_bonespace = PBone::TS_LOCAL;
    stdman.load(sceneMgr, "Marionnettes", "stdman-mesh.mesh");
    stdman.scale(50);
//    stdman.load(sceneMgr, "Marionnettes", "deer-mesh.mesh");
//    stdman.scale(1);
    
    std::cout << "has skeleton? " << stdman.hasSkeleton() << std::endl;
    stdman.debug(PDM_SKELETON, true);
    size_t std_bnum;
    std::string* std_bnames;
    stdman.getBoneNames(std_bnum, std_bnames);
    cout << "Getting stdman bone's names" << endl;
    for (size_t i = 0; i < std_bnum; ++i) {
        cout << "\tbone[" << i << "] name: '" << std_bnames[ i ] << "'" << endl;
        stdman.getBoneOrientation(
                std_bnames[ i ],
                stdman_base[ std_bnames[ i ] ],
                stdman_bonespace
                );
    }
    if (std_bnames) {
        delete[] std_bnames;
    }

    POscReceiverData rcvd;
    rcvd.autostart = true;
    rcvd.max_queue_size = 50;
    rcvd.port = 23000;
    rcvr = new polymorph::POscReceiver(rcvd);

}

void CustomApp::draw() {

    // back t default animation
    if (resetAnim) {
        float pc = resetTimer / RESET_ANIMATION_DURATION;
        if (pc < 0) {
            pc = 0;
            resetAnim = false;
        }
        pc = 1 - pc;
        for (int i = 0; i < bones_num; i++) {
            if (pc < 1) {
                bpos_current[ i ] = PUtil::slerp(
                        bpos_current[ i ], bpos_init[ i ],
                        pc);
                bquats_current[ i ] = Quaternion::Slerp(
                        pc,
                        bquats_current[ i ],
                        bquats_init[ i ]);
            } else {
                bpos_current[ i ] = bpos_init[ i ];
                bquats_current[ i ] = bquats_init[ i ];
            }
            sophiekhan->orientBone(bones[ i ], bquats_current[ i ]);
            sophiekhan->moveBone(bones[ i ], bpos_current[ i ]);
        }
        resetTimer -= draw_event.timeSinceLastFrame;
    }

    Quaternion ry(Ogre::Radian(-mouse_norm_rel.x * Math::PI), Vector3(0, 1, 0));
    Quaternion rx(Ogre::Radian(mouse_norm_rel.y * Math::HALF_PI), Vector3(1, 0, 0));

    // getting world position of a bone
    Ogre::Vector3 bp;
    if (sophiekhan->getBonePosition(bones[ bones_current ], bp, PBone::TS_WORLD)) {
        bone_hightlight.move(bp);
    } else {
        cout << "failed to get bone!!!" << endl;
    }


    switch (mouse_button) {
        case 1: // left
            sophiekhan->yaw(Ogre::Radian(mouse_norm_rel.x * 3));
            break;
        case 2: // right
        {
            Quaternion& q = bquats_current[ bones_current ];
            q = q * rx * ry;
            sophiekhan->orientBone(bones[ bones_current ], q);
//            stdman.orientBone( "ḦIP_CENTER", q );
            stdman.orientation( q );
        }
            break;
        case 3: // middle
        {
            Ogre::Vector3 p(-mouse_norm_rel.x, 0, -mouse_norm_rel.y);
            sophiekhan->translateBone(bones[ bones_current ], p);
            sophiekhan->getBonePosition(bones[ bones_current ], bpos_current[ bones_current ]);
        }
            break;
        default:
            break;
    }

    mouse_norm_rel = Vector2(0, 0);

}

void CustomApp::after_draw() {

    if (rcvr->hasPending()) {
        PMessageData* msg = rcvr->getNext();
        while (msg) {
            if (strcmp(msg->addr, "/skf") == 0) {
                
                uint8_t appid = msg->asInt(0);
                uint8_t kinectid = msg->asInt(1);
                uint8_t skeletonid = msg->asInt(2);

//                std::cout << "received skeleton frame: " <<
//                        int( appid) << ":" <<
//                        int( kinectid) << ":" <<
//                        int( appid) <<
//                        std::endl;

                for (uint16_t i = 6; i < msg->typetags_size; i += 5) {
                    string name = msg->asString(i);
                    PBone* b = stdman.bone(name);
                    if (b) {
                        Quaternion q(
                                msg->asFloat(i + 1),
                                msg->asFloat(i + 2),
                                msg->asFloat(i + 3),
                                msg->asFloat(i + 4)
                                );
                        q = stdman_base[ name ] * q;
                        stdman.orientBone(name, q, stdman_bonespace);
                    }
                }

            }
            msg = rcvr->getNext();
        }
    }

}

bool CustomApp::keyPressed(const OIS::KeyEvent &arg) {

    // keep default behavior
    PolymorphApplication::keyPressed(arg);
    switch (arg.key) {
        case OIS::KC_ESCAPE:
            if (rcvr->runs()) {
                std::cout << "stopping OSC receiver" << std::endl;
                rcvr->stop();
            }
            break;
        case OIS::KC_SPACE:
            // toggle skeleton debugging
        {
            if (sophiekhan->isDebugMode(PDM_SKELETON)) {
                sophiekhan->debug(PDM_SKELETON, false);
                bone_hightlight.visible(false);
            } else {
                sophiekhan->debug(PDM_SKELETON, true);
                bone_hightlight.visible(true);
            }
        }
            break;
        case OIS::KC_R:
            if (!resetAnim) {
                resetAnim = true;
                resetTimer = RESET_ANIMATION_DURATION;
            }
        case OIS::KC_UP:
        case OIS::KC_RIGHT:
            if (resetAnim) return true;
            bones_current++;
            if (bones_current >= bones_num) {
                bones_current = 0;
            }
            cout << bones[ bones_current ]->getName() << endl;
            break;
        case OIS::KC_DOWN:
        case OIS::KC_LEFT:
            if (resetAnim) return true;
            if (bones_current == 0) {
                bones_current = bones_num - 1;
            } else {
                bones_current--;
            }
            cout << bones[ bones_current ]->getName() << endl;
            break;
        default:
            break;
    }

}

bool CustomApp::mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id) {

    // keep default behavior
    PolymorphApplication::mousePressed(arg, id);
    mouse_button = ((uint) id) + 1;

}

bool CustomApp::mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id) {

    // keep default behavior
    PolymorphApplication::mouseReleased(arg, id);
    mouse_button = 0;

}

bool CustomApp::mouseMoved(const OIS::MouseEvent &arg) {

    // keep default behavior
    PolymorphApplication::mouseMoved(arg);

    mouse_norm = Ogre::Vector2(
            Ogre::Real(arg.state.X.abs) / window->getWidth(),
            Ogre::Real(arg.state.Y.abs) / window->getHeight()
            );
    mouse_norm_rel = Ogre::Vector2(
            Ogre::Real(arg.state.X.rel) / window->getWidth(),
            Ogre::Real(arg.state.Y.rel) / window->getHeight()
            );

}