#if __VERSION__ < 330

#version 120

#define M_PI 3.1415926535897932384626433832795

uniform sampler2D tex;
uniform vec2 center;
uniform float radius;
uniform float ratio;
uniform vec4 center_color;
uniform vec4 border_color;

void main()
{
    vec2 texcoord = vec2( gl_TexCoord[0] );
    vec4 col = texture2D( tex, texcoord, 0.0 );
    texcoord.y = 0.5 + ((texcoord.y-0.5) / ratio);
    float d = distance( texcoord, center );
    if ( d > radius ) {
        gl_FragColor = border_color;
    } else {
        // sinus fall off
        d = ( sin( ( ( d / radius ) - 0.5 ) * M_PI ) + 1.0 ) * 0.5;
        gl_FragColor = mix( center_color, border_color, d );
    }
}

#else

#version 330

#define M_PI 3.1415926535897932384626433832795

uniform sampler2D tex;
uniform vec2 center;
uniform float radius;
uniform float ratio;
uniform vec4 center_color;
uniform vec4 border_color;

in vec2 uv0;
out vec4 fragColour;

void main()
{
    uv0.y = 0.5 + ((uv0.y-0.5) / ratio);
    float d = distance( uv0, center );
    if ( d > radius ) {
        fragColour = border_color;
    } else {
        // sinus fall off
        d = ( sin( ( ( d / radius ) - 0.5 ) * M_PI ) + 1.0 ) * 0.5;
        fragColour = mix( center_color, border_color, d );
    }
    #vec4 col = texture2D( tex, uv0 );
}

#endif
