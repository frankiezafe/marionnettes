#if __VERSION__ < 330

#version 120

uniform sampler2D tex;
uniform float alpha_mult;

void main()
{
	vec2 texcoord = vec2( gl_TexCoord[0] );
	vec4 col = texture2D( tex, texcoord, 0.0 );
	col.a *= alpha_mult;
	gl_FragColor = col;
}

#else

#version 330

uniform sampler2D tex;
uniform float alpha_mult;

in vec2 uv0;
out vec4 fragColour;

void main()
{
    vec4 col = texture2D( tex, uv0 );
	col.a *= alpha_mult;
	fragColour = col;
}

#endif
