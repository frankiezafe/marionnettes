float4 alpha_main( 
	uniform sampler2D tex : register(s0), 
	float2 uv : TEXCOORD0
	uniform float alpha_mult : float ) : COLOR0
{
	return tex2D( tex, uv ).xyzw;
}