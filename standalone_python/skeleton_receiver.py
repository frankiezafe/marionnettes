'''

== generalities ==

This script is a receiver of messages emitted by 
KinectStreamers. They have to know your IP to send you
the messages...

Once launched, threads are started to receive all messages:

* /skp: skeleton joints position messages, no rotations
* /skf: skeleton frame, position of the skeleton, joints names and their rotations expressed as a quaternion (w,x,y,z order)

Each message begins with 3 numbers:

* application ID: one per computer
* kinect ID: 0 or 1, as each computer might have 2 kinect
* skeleton ID: 0 to 5, (maximum 6 skeletons per kinect)

== before starting ==

checks the "global" parameters and adapt your IP

it is strongly recommended to work with wired connection,
udp messages are easily lost on wifi connection

LAUNCH THIS WITH PYTHON 3!

& python3 skeleton_receiver.py

And here we go!

'''

##################### IMPORTS #####################

import time
import random
import ThreadOsc

##################### GLOBALS #####################

MY_IP = '192.168.3.68'
MAIN_PORT = 23000
MSG_BYTES = 1024

##################### PARSER #####################

class MsgParser():

	def __init__(self):
		self.rcvmutex = False
		self.rcvdata = []
		self.experts = {}
		self.expert_name = [ "a0","a1","a2","a3","a4","a5","a6","a7","a8","a9" ]

	def uniqueID( self, msg ):
		appid = int( msg[ 2 ] )
		kinectid = int( msg[ 3 ] )
		skeletonid = int( msg[ 4 ] )
		return appid * 100 + kinectid * 10 + skeletonid

	def parse_skeleton_position(self, msg):
		UID = self.uniqueID( msg )
		print( "*********** positions of", UID )
		position = []
		for d in range( 5, len( msg ) ):
			dn = d - 5
			if dn % 3 == 0:
				position.append( [ msg[ d ], msg[ d+1 ], msg[ d+2 ] ] )
				d += 3
		print( UID, "positions", position )

	def parse_skeleton_frame(self, msg):
		UID = self.uniqueID( msg )
		if UID not in self.experts:
			i = random.randint(0, len( self.expert_name )-1)
			self.experts[UID] = self.expert_name[ i ]
			self.expert_name.remove( self.experts[UID] )
			print( UID, "mapped to", self.experts[UID] )
		print( "*********** frame of", UID )
		position = [ msg[ 5 ], msg[ 6 ], msg[ 7 ] ]
		#print( UID, "located at", position )
		rotations = {}
		# unpacking quaternions
		for d in range( 8, len( msg ) ):
			dn = d - 8
			if dn % 5 == 0:
				rotations[  msg[ d ] ] = [ msg[ d+1 ], msg[ d+2 ], msg[ d+3 ], msg[ d+4 ] ]
				d += 5
		#print( UID, "rotations", rotations )

	def parse(self):

		# nothing here...
		if len( self.rcvdata ) == 0:
			return

		self.rcvmutex = True
		tmp = self.rcvdata
		self.rcvdata = []
		self.rcvmutex = False
		
		for msg in tmp:
			if msg[ 0 ] == '/skp':
				continue
				# its a position message, let's parse it
				# self.parse_skeleton_position( msg )
			elif msg[ 0 ] == '/skf':
				# its a mapped rotations message, let's parse it
				self.parse_skeleton_frame( msg )
			else:
				print( "heu???", msg )


##################### MAIN #####################

parser = MsgParser()

rcv = ThreadOsc.ThreadOsc()
rcv.osc_simple = { 0: (MY_IP, MAIN_PORT, MSG_BYTES) }
rcv.osc_skeleton = { 0: (MY_IP, MAIN_PORT, MSG_BYTES, 3, 1, 6), 1 : (MY_IP, MAIN_PORT, MSG_BYTES, 2, 1, 6), 2 : (MY_IP, MAIN_PORT, MSG_BYTES, 1, 1, 6) }
rcv.configure()
rcv.addListener( parser )
rcv.start()

while True:
	nothingtodo = True
	parser.parse()
	time.sleep( 0.015 ) # sleeping 15 millis

print( rcv )