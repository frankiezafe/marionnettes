= About communication with text =

When running the performing-the-lexcicon.blend, messages are broadcatsed to detect poses:

* when arms go up,
* when legs go up,
* when body is bending forward.

== OSC messages ==

Form of the messages, for arms:

* address: **/rotationZ**
* typetag: **isfi**
* argument[0]: ID
* argument[1]: bone name, **ELBOW_LEFT** or **ELBOW_RIGHT**
* argument[2]: radian
* argument[3]: flag, 0 when above 0.4 radian, 1 if above

Form of the messages, for legs & body:

* address: **/rotationX**
* typetag: **isfi**
* argument[0]: ID
* argument[1]: bone name, **SHOULDER_CENTER**, **KNEE_LEFT** or **KNEE_RIGHT**
* argument[2]: radian
* argument[3]: flag, 0 when above 0.4 radian, 1 if above

The python script ../blender/scripts/performing-the-lexcicon_mono.py contains the IP address to the text generator.

== Displaying console of another machine ==

* open a terminal on the display machine
* connect through SSH: ssh [user]@[ip]
* adapt the window size
* $ screen

On the host machine, type:
* $ screen -rx

Configuration of the terminal @beursschouwburg:

* font: Rubik Mono One Regular
* size: 14
* background fully transparent
