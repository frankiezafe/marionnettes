import bpy

############## CONFIGURATION ##############

export_path = bpy.path.abspath("//") + "avatar.model"

############## EXECUTION ##############

arma = bpy.data.armatures["model-arma"]

file = open( export_path,"w")
for b in arma.bones:
    file.write( 
        b.name + " " + 
        str( b.tail.x ) + " " + 
        str( b.tail.y ) + " " + 
        str( b.tail.z ) + "\n" )