# AVATAR EXPORTER

This file contains the armature (~skeleton) to use as
a reference in the streamer application.

## HOW TO USE

* Modify position of 'model-arma' bones as you pleased.
* In 'avatar-export.py', adapt 'export_path'.
* Once done, run the script by hitting 'ALT+P' or 
  'Run Script' button.
* Copy/paste the 'avatar.model' in the 'data' folder
  of the streamer application.
* If you want a perfect match, the easiest is to import
  'model-arma' in your blender file and use it for your
  avatar. Nevertheless, you use mapping functionnality
  of the streamer application to make a relation with any
  bone.

## REQUIREMENT

Make sure that:

* your file is called 'avatar.model';
* all your bones are connected;
* you have all the kinect bones with the right names in
  the armature:
* * HIP_CENTER
* * SPINE
* * SHOULDER_CENTER
* * HEAD
* * SHOULDER_LEFT
* * ELBOW_LEFT
* * WRIST_LEFT
* * HAND_LEFT
* * SHOULDER_RIGHT
* * ELBOW_RIGHT
* * WRIST_RIGHT
* * HAND_RIGHT
* * HIP_LEFT
* * KNEE_LEFT
* * ANKLE_LEFT
* * FOOT_LEFT
* * HIP_RIGHT
* * KNEE_RIGHT
* * ANKLE_RIGHT
* * FOOT_RIGHT