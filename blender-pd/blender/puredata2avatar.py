#http://www.blender.org/documentation/blender_python_api_2_67_1/bge.types.KX_GameObject.html -> playAction !!!

import sys
import math
import random
import bge
import bge.logic as gl
import mathutils

import socket
from OSC.OSC import decodeOSC

# bicipite_R, avambraccio_R, mano_R

scene = bge.logic.getCurrentScene()
avatar_root = scene.objects["av"]
avatar = scene.objects["stdman"]
avatar001 = scene.objects["stdman.001"]
tube = scene.objects["tube"]
avatar_mesh = scene.objects["stdman-mesh"]
root = scene.objects["root"]

buffer_size = 1024
config = { 0: ('127.0.0.1', 23000, buffer_size ) }

init = False
try:
    if ( root["configured"] != True ):
        init = True
        print( "*************KEYS*************" )
        print( avatar_mesh )
        print( avatar_mesh.meshes )
        print( avatar_mesh.getPropertyNames() )
    root["configured"] = True
except:
    root["configured"] = False

if ( init == True ):
    
    print( "*************INIT START*************" )
    
    # ------------ globals ---------------- #
    root["rest"] = {}
    root["eulers"] = {}
    root["scales"] = {}
    root["pos"] = {}
    root["need_update"] = {}
    
    # ------------ collecting all matrices ---------------- #
    for c in avatar.channels:
        cn = c.name
        root["rest"][ cn ] = mathutils.Matrix( c.pose_matrix )
        root["eulers"][ cn ] = [ 0,0,0 ]
        root["scales"][ cn ] = [ 1,1,1 ]
        root["pos"][cn] = c.location
        root["need_update"][ cn ] = False
        print( cn )
        #print( "* eurler:", root["eulers"][ cn ] )
    
    # ------------ set list for get_osc ---------------- #
    gl.connected = []
    gl.socket = []
    for i in range( len( config ) ):
        gl.connected.append(False)
        gl.socket.append(0)
    
        # ------------ set some var ---------------- #
        gl.tempo = 0
        ip = config[i][0]
        port = config[i][1]
        buffer_size = config[i][2]
    
        # ------------ connecting ---------------- #
        if not gl.connected[i] :
            gl.socket[i] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            try:
                gl.socket[i].bind((ip, port))
                gl.socket[i].setblocking(0)
                gl.socket[i].settimeout(0.01)
                gl.socket[i].setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, buffer_size)
                print('Plug : IP = {} Port = {} Buffer Size = {} Instance = {}'.format(ip, port, buffer_size, i))
                gl.connected[i] = True
            except:
                print('No connected to IP = {} Port = {}'.format(ip, port))
                pass
        
    print( "*************INIT STOP*************" )
else:
    
    rest = root["rest"]
    eulers = root["eulers"]
    scales = root["scales"]
    
    # ------------ gather messages ---------------- #
    for i in range( len( config ) ):
        if gl.connected[i] :
            try:
                raw_data = gl.socket[i].recv( buffer_size )
                data = decodeOSC(raw_data)
                if data[0] == "/avatar/bone/rot" :
                    for d in range( len( data ) ):
                        if d < 2:
                            continue
                        dn = d - 2
                        if dn % 4 == 0:
                            bname = str( data[ d ] )
                            try:
                                agls = eulers[ bname ]
                                agls[ 0 ] = float( data[ d+1 ] )
                                agls[ 1 ] = float( data[ d+2 ] )
                                agls[ 2 ] = float( data[ d+3 ] )
                                root["need_update"][ bname ] = True
                                print( bname )
                                d += 4
                            except:
                                print( "OSC format error, no bone named '", bname, "'!" )
                elif data[0] == "/avatar/bone/scale" :
                    bname = str( data[ 2 ] )
                    scales[ bname ][ 0 ] = data[ 3 ]
                    scales[ bname ][ 1 ] = data[ 4 ]
                    scales[ bname ][ 2 ] = data[ 5 ]
                elif data[0] == "/avatar/bone/pos" :
                    bname = str( data[ 2 ] )
                    root["pos"][ bname ][ 0 ] = data[ 3 ]
                    root["pos"][ bname ][ 1 ] = data[ 4 ]
                    root["pos"][ bname ][ 2 ] = data[ 5 ]
                elif data[0] == "/avatar/pos" :
                    avatar_root.position[ 0 ] = data[ 2 ]
                    avatar_root.position[ 1 ] = data[ 3 ]
                    avatar_root.position[ 2 ] = data[ 4 ]
                elif data[0] == "/avatar/reset" :
                    eulers = root["eulers"]
                    for cn in rest:
                        eulers[ cn ][ 0 ] = 0
                        eulers[ cn ][ 1 ] = 0
                        eulers[ cn ][ 2 ] = 0
                elif data[0] == "/avatar/print" :
                    print( eulers )
            except socket.error:
                #print( "OSC socket error" )
                pass
    
    # ------------ updating armature ---------------- #

    for cn in rest:
        
        channel = avatar.channels[ cn ]
        mat = mathutils.Matrix( rest[ cn ] )
        agls = eulers[ cn ]
        channel.joint_rotation = (0,0,0)
        channel.rotation_mode = bge.logic.ROT_MODE_XYZ
        channel.rotation_euler = agls
        channel.scale = scales[ cn ]
        channel.location = root["pos"][cn]
        
        channel = avatar001.channels[ cn ]
        channel.joint_rotation = (0,0,0)
        channel.rotation_mode = bge.logic.ROT_MODE_XYZ
        channel.rotation_euler = agls
        channel.scale = scales[ cn ]
        channel.location = root["pos"][cn]
        
        try:
            channel = tube.channels[ cn ]
            channel.joint_rotation = (0,0,0)
            channel.rotation_mode = bge.logic.ROT_MODE_XYZ
            channel.rotation_euler = agls
            channel.scale = scales[ cn ]
        except:
            error = 1
            
    
    #avatar.channels["root"].location = (0,2,0)    
    
    avatar.update()
    avatar001.update()
    tube.update()
