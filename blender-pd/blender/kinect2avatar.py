#http://www.blender.org/documentation/blender_python_api_2_67_1/bge.types.KX_GameObject.html -> playAction !!!

import sys
import math
import random
import bge
import bge.logic as gl
import mathutils

import socket
from OSC.OSC import decodeOSC

def applyPosition( avatar_name, values ):
    try:
        obj = root["avatars"][ avatar_name ]["root"]
        obj.worldPosition = values
    except:
        print( "Unexpected error:", sys.exc_info()[0] )
        pass
    

def applyQuaternion( avatar_name, bone, values ):
    global root
    blist = []
    startat = 0
    while root["avatars"][ avatar_name ]["bonelist"].find( ":"+bone, startat ) != -1:
        startat = root["avatars"][ avatar_name ]["bonelist"].find( ":"+bone, startat )
        dot = root["avatars"][ avatar_name ]["bonelist"].find( ":", startat+1 )
        name = root["avatars"][ avatar_name ]["bonelist"][startat+1:dot]
        startat += 1
        blist.append( name )
    for n in blist:
        try:
            bone = root["avatars"][ avatar_name ]["obj"].channels[ n ]
            bone.joint_rotation = [0,0,0]
            bone.rotation_quaternion = [values[0], values[1], values[2],values[3]]
            #bone = root["avatars"][ avatar_name ]["bones"][ bone ]
            #q = mathutils.Quaternion((values[3],values[0], values[1], values[2])).to_matrix()
            #q = q * bone["mati"]
            #q = bone["mat"] * q
            #q = q.to_quaternion() 
            #q.normalize();
            #bone["obj"].joint_rotation = [0,0,0]
            #bone["obj"].rotation_quaternion = [q.w, q.x, q.y, q.z]
        except:
            print( "Unexpected error:", sys.exc_info()[0] )
            print( 
                "applyQuaternion crashed: '" + 
                avatar_name + "' '" + n + "'", 
                values )
            pass

def initAvatar( root_name, avatar_name ):
    global root
    try:
        root_obj = bge.logic.getCurrentScene().objects[ root_name ]
        obj = bge.logic.getCurrentScene().objects[ avatar_name ]
    except:
        print( "no object called '" + name + "' in current scene" )
        return
    bonelist = ""
    root["avatars"][ avatar_name ] = {}
    root["avatars"][ avatar_name ]["root"] = root_obj
    root["avatars"][ avatar_name ]["obj"] = obj
    root["avatars"][ avatar_name ]["bonelist"] = ""
    root["avatars"][ avatar_name ]["bones"] = {}
    #obj.applyRotation(mathutils.Vector((math.radians(90),0,0)))
    for c in obj.channels:
        c.rotation_mode = bge.logic.ROT_MODE_QUAT
        cn = c.name
        bonelist = bonelist + ":"+cn+":"
        #m = mathutils.Matrix( c.pose_matrix ).to_3x3()
        #root["avatars"][ avatar_name ]["bones"][ cn ] = {}
        #root["avatars"][ avatar_name ]["bones"][ cn ]["obj"] = c
        #root["avatars"][ avatar_name ]["bones"][ cn ]["mat"] = m
        #root["avatars"][ avatar_name ]["bones"][ cn ]["mati"] = m.inverted()
    root["avatars"][ avatar_name ]["bonelist"] = bonelist
    print( "*************> ", avatar_name, bonelist )
    for n in root["avatars"][ avatar_name ]["bones"]:
        print( '\t', n )
    
def updateAvatars():
    global root
    for n in root["avatars"]:
        root["avatars"][n]["obj"].update()

scene = bge.logic.getCurrentScene()
root = scene.objects["root"]

buffer_size = 1024
config = { 
    0: ('127.0.0.1', 23000, buffer_size ),
    1: ('192.168.3.68', 23000, buffer_size )}

init = False
try:
    if ( root["configured"] != True ):
        root["avatars"] = {}
        init = True
    root["configured"] = True
except:
    root["configured"] = False

if ( init == True ):
    print( "*************INIT START*************" )
    initAvatar( "av", "stdman" )
    initAvatar( "tube", "tube" )
    initAvatar( "cheeta", "cheeta" )
    initAvatar( "deer", "deer" )
    #initAvatar( "dragon", "dragon" )
    # ------------ set list for get_osc ---------------- #
    gl.connected = []
    gl.socket = []
    for i in range( len( config ) ):
        gl.connected.append(False)
        gl.socket.append(0)
        # ------------ set some var ---------------- #
        gl.tempo = 0
        ip = config[i][0]
        port = config[i][1]
        buffer_size = config[i][2]
        # ------------ connecting ---------------- #
        if not gl.connected[i] :
            gl.socket[i] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            try:
                gl.socket[i].bind((ip, port))
                gl.socket[i].setblocking(0)
                gl.socket[i].settimeout(0.01)
                gl.socket[i].setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, buffer_size)
                print('Plug : IP = {} Port = {} Buffer Size = {} Instance = {}'.format(ip, port, buffer_size, i))
                gl.connected[i] = True
            except:
                print('No connected to IP = {} Port = {}'.format(ip, port))
                pass
    print( "*************INIT STOP*************" )
else:
    # ------------ gather messages ---------------- #
    for i in range( len( config ) ):
        if gl.connected[i] :
            try:
                raw_data = gl.socket[i].recv( buffer_size )
                data = decodeOSC(raw_data)
                if data[0] == "/skpos" :
                    kinectid = data[ 2 ]
                    skid = data[ 3 ]
                    applyPosition(
                        "stdman", 
                        [ 
                            float( data[4] ) * 0.001, 
                            float( data[5] ) * 0.001, 
                            float( data[6] ) * 0.001
                            ] )
                elif data[0] == "/skrot" :
                    #print( "skrot", data )
                    for d in range( len( data ) ):
                        if d < 5:
                            continue
                        kinectid = data[ 2 ]
                        skid = data[ 3 ]
                        dn = d - 4
                        if dn % 5 == 0:
                            bname = str( data[ d ] )
                            try:
                                l = [ data[ d+1 ], data[ d+2 ], data[ d+3 ], data[ d+4 ] ]
                                applyQuaternion( "stdman", bname, l )
                                applyQuaternion( "tube", bname, l )
                                applyQuaternion( "cheeta", bname, l )
                                applyQuaternion( "deer", bname, l )
                                #applyQuaternion( "dragon", bname, l )
                                d += 5
                            except:
                                print( "OSC format error, no bone named '", bname, "'!" )
                else:
                    print( "???", data )
            except socket.error:
                pass
    # ------------ updating armature ---------------- #
    updateAvatars()